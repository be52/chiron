package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

object Main {
  def main(accelUnit: AccelUnit): Unit = {
    accelUnit.io <> DontCare
    val x580_nrow = accelUnit.io.argIns(api.NROW_arg)
    val x581_ncol = accelUnit.io.argIns(api.NCOL_arg)
    val x582_ncons = accelUnit.io.argIns(api.NCONS_arg)
    val x583_ncoefs = accelUnit.io.argIns(api.NCOEFS_arg)
    val x600 = accelUnit.io.memStreams.loads(0).cmd // StreamOut
    ModuleParams.addParams("x600_p", (x600.bits.addrWidth, x600.bits.sizeWidth))  
    val x602 = accelUnit.io.memStreams.loads(0).data // StreamIn
    ModuleParams.addParams("x602_p", (x602.bits.v, x602.bits.w))  
    // scoped in dram is  
    val x592_tab_dram = Wire(new FixedPoint(true, 64, 0))
    x592_tab_dram.r := accelUnit.io.argIns(api.TAB_DRAM_ptr)
    val x1022 = accelUnit.io.memStreams.stores(0).cmd // StreamOut
    ModuleParams.addParams("x1022_p", (x1022.bits.addrWidth, x1022.bits.sizeWidth))  
    val x1023 = accelUnit.io.memStreams.stores(0).data // StreamOut
    ModuleParams.addParams("x1023_p", (x1023.bits.v, x1023.bits.w))  
    val x1024  = accelUnit.io.memStreams.stores(0).wresp // StreamIn
    // scoped in dram is  
    val x596_tab_dram_out = Wire(new FixedPoint(true, 64, 0))
    x596_tab_dram_out.r := accelUnit.io.argIns(api.TAB_DRAM_OUT_ptr)
    val retime_counter = Module(new SingleCounter(1, Some(0), Some(accelUnit.max_latency), Some(1), false)); retime_counter.io <> DontCare // Counter for masking out the noise that comes out of ShiftRegister in the first few cycles of the app
    retime_counter.io.setup.saturate := true.B; retime_counter.io.input.reset := accelUnit.reset.toBool; retime_counter.io.input.enable := true.B;
    val rr = getRetimed(retime_counter.io.output.done, 1, true.B) // break up critical path by delaying this 
    val breakpoints = Wire(Vec(accelUnit.io_numArgOuts_breakpts max 1, Bool())); breakpoints.zipWithIndex.foreach{case(b,i) => b.suggestName(s"breakpoint" + i)}; breakpoints := DontCare
    val done_latch = Module(new SRFF())
    val RootController = new RootController_kernel(List(x596_tab_dram_out,x592_tab_dram), List(x1024), List(x582_ncons,x580_nrow,x581_ncol), List(x1022,x600), List(x1023), List(x602) , None, List(), -1, 3, 1, List(1), List(32), breakpoints, rr)
    RootController.baseEn := accelUnit.io.enable && rr && ~done_latch.io.output
    RootController.resetMe := getRetimed(accelUnit.accelReset, 1)
    RootController.mask := true.B
    RootController.sm.io.parentAck := accelUnit.io.done
    RootController.sm.io.enable := RootController.baseEn & !accelUnit.io.done & true.B
    done_latch.io.input.reset := RootController.resetMe
    done_latch.io.input.asyn_reset := RootController.resetMe
    accelUnit.io.done := done_latch.io.output
    RootController.sm.io.ctrDone := risingEdge(RootController.sm.io.ctrInc)
    RootController.backpressure := true.B | RootController.sm.io.doneLatch
    RootController.forwardpressure := true.B | RootController.sm.io.doneLatch
    RootController.sm.io.enableOut.zip(RootController.smEnableOuts).foreach{case (l,r) => r := l}
    RootController.sm.io.break := false.B
    RootController.mask := true.B & true.B
    RootController.configure("RootController", None, None, isSwitchCase = false)
    RootController.kernel()
    done_latch.io.input.set := RootController.done
    Ledger.finish()
  }
}
