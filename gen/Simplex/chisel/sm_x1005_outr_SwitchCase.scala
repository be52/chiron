package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x1005_outr_SwitchCase **/
class x1005_outr_SwitchCase_kernel(
  list_b47: List[FixedPoint],
  list_x597_tab_sram_0: List[StandardInterface],
  list_x582_ncons: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 2, isFSM = false   , latency = 0.0.toInt, myName = "x1005_outr_SwitchCase_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1005_outr_SwitchCase_iiCtr"))
  
  abstract class x1005_outr_SwitchCase_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x582_ncons = Input(UInt(64.W))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x672_no_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x672_no_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x668_iter_0 = Flipped(new StandardInterface(ModuleParams.getParams("x668_iter_0_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x675_pivot_row_min_ratio_0 = Flipped(new StandardInterface(ModuleParams.getParams("x675_pivot_row_min_ratio_0_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x673_no_pivot_row_count_0 = Flipped(new StandardInterface(ModuleParams.getParams("x673_no_pivot_row_count_0_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_b47 = Input(new FixedPoint(true, 32, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x582_ncons = {io.in_x582_ncons} 
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x672_no_pivot_row_0 = {io.in_x672_no_pivot_row_0} ; io.in_x672_no_pivot_row_0 := DontCare
    def x668_iter_0 = {io.in_x668_iter_0} ; io.in_x668_iter_0 := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x675_pivot_row_min_ratio_0 = {io.in_x675_pivot_row_min_ratio_0} ; io.in_x675_pivot_row_min_ratio_0 := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x580_nrow = {io.in_x580_nrow} 
    def x673_no_pivot_row_count_0 = {io.in_x673_no_pivot_row_count_0} ; io.in_x673_no_pivot_row_count_0 := DontCare
    def x581_ncol = {io.in_x581_ncol} 
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
    def b47 = {io.in_b47} 
  }
  def connectWires0(module: x1005_outr_SwitchCase_module)(implicit stack: List[KernelHash]): Unit = {
    module.io.in_x582_ncons <> x582_ncons
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x672_no_pivot_row_0.connectLedger(module.io.in_x672_no_pivot_row_0)
    x668_iter_0.connectLedger(module.io.in_x668_iter_0)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x675_pivot_row_min_ratio_0.connectLedger(module.io.in_x675_pivot_row_min_ratio_0)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    module.io.in_x580_nrow <> x580_nrow
    x673_no_pivot_row_count_0.connectLedger(module.io.in_x673_no_pivot_row_count_0)
    module.io.in_x581_ncol <> x581_ncol
    x684_reg.connectLedger(module.io.in_x684_reg)
    module.io.in_b47 <> b47
  }
  val b47 = list_b47(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x598_tab_sram_1 = list_x597_tab_sram_0(1)
  val x679_pivot_col_0 = list_x597_tab_sram_0(2)
  val x674_pivot_element_0 = list_x597_tab_sram_0(3)
  val x672_no_pivot_row_0 = list_x597_tab_sram_0(4)
  val x668_iter_0 = list_x597_tab_sram_0(5)
  val x685_reg = list_x597_tab_sram_0(6)
  val x675_pivot_row_min_ratio_0 = list_x597_tab_sram_0(7)
  val x680_pivot_row_0 = list_x597_tab_sram_0(8)
  val x673_no_pivot_row_count_0 = list_x597_tab_sram_0(9)
  val x684_reg = list_x597_tab_sram_0(10)
  val x582_ncons = list_x582_ncons(0)
  val x580_nrow = list_x582_ncons(1)
  val x581_ncol = list_x582_ncons(2)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1005_outr_SwitchCase_obj")
    implicit val stack = ControllerStack.stack.toList
    class x1005_outr_SwitchCase_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1005_outr_SwitchCase_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      // Controller Stack: Stack(x1006, x1020, x1137)
      val x757_reg = (new x757_reg).m.io.asInstanceOf[StandardInterface]
      val x758_reg = (new x758_reg).m.io.asInstanceOf[StandardInterface]
      val x759_reg = (new x759_reg).m.io.asInstanceOf[StandardInterface]
      val x760_reg = (new x760_reg).m.io.asInstanceOf[StandardInterface]
      val x767_inr_UnitPipe = new x767_inr_UnitPipe_kernel(List(b47), List(x760_reg,x757_reg,x758_reg,x759_reg) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x767_inr_UnitPipe.sm.io.ctrDone := risingEdge(x767_inr_UnitPipe.sm.io.ctrInc)
      x767_inr_UnitPipe.backpressure := true.B | x767_inr_UnitPipe.sm.io.doneLatch
      x767_inr_UnitPipe.forwardpressure := true.B | x767_inr_UnitPipe.sm.io.doneLatch
      x767_inr_UnitPipe.sm.io.enableOut.zip(x767_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x767_inr_UnitPipe.sm.io.break := false.B
      x767_inr_UnitPipe.mask := true.B & true.B
      x767_inr_UnitPipe.configure("x767_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x767_inr_UnitPipe.kernel()
      val x1091_rd_x757 = Wire(Bool()).suggestName("""x1091_rd_x757""")
      val x1091_rd_x757_banks = List[UInt]()
      val x1091_rd_x757_ofs = List[UInt]()
      val x1091_rd_x757_en = List[Bool](true.B)
      x1091_rd_x757.toSeq.zip(x757_reg.connectRPort(1091, x1091_rd_x757_banks, x1091_rd_x757_ofs, io.sigsIn.backpressure, x1091_rd_x757_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1092_rd_x759 = Wire(Bool()).suggestName("""x1092_rd_x759""")
      val x1092_rd_x759_banks = List[UInt]()
      val x1092_rd_x759_ofs = List[UInt]()
      val x1092_rd_x759_en = List[Bool](true.B)
      x1092_rd_x759.toSeq.zip(x759_reg.connectRPort(1092, x1092_rd_x759_banks, x1092_rd_x759_ofs, io.sigsIn.backpressure, x1092_rd_x759_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1003_outr_Switch_obj = new x1003_outr_Switch_kernel(List(x1091_rd_x757,x1092_rd_x759), List(b47), List(x760_reg,x597_tab_sram_0,x598_tab_sram_1,x757_reg,x679_pivot_col_0,x674_pivot_element_0,x672_no_pivot_row_0,x758_reg,x668_iter_0,x685_reg,x675_pivot_row_min_ratio_0,x680_pivot_row_0,x673_no_pivot_row_count_0,x759_reg,x684_reg), List(x582_ncons,x580_nrow,x581_ncol) , Some(me), List(), 1, 2, 1, List(1), List(32), breakpoints, rr)
      val x844_outr_SwitchCase_switch_sel_reg = RegInit(false.B)
      x844_outr_SwitchCase_switch_sel_reg := Mux(risingEdge(x1003_outr_Switch_obj.en), x1091_rd_x757, x844_outr_SwitchCase_switch_sel_reg)
      x1003_outr_Switch_obj.sm.io.selectsIn(0) := x1091_rd_x757
      val x1002_outr_SwitchCase_switch_sel_reg = RegInit(false.B)
      x1002_outr_SwitchCase_switch_sel_reg := Mux(risingEdge(x1003_outr_Switch_obj.en), x1092_rd_x759, x1002_outr_SwitchCase_switch_sel_reg)
      x1003_outr_Switch_obj.sm.io.selectsIn(1) := x1092_rd_x759
      x1003_outr_Switch_obj.backpressure := true.B | x1003_outr_Switch_obj.sm.io.doneLatch
      x1003_outr_Switch_obj.forwardpressure := true.B | x1003_outr_Switch_obj.sm.io.doneLatch
      x1003_outr_Switch_obj.sm.io.enableOut.zip(x1003_outr_Switch_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x1003_outr_Switch_obj.sm.io.break := false.B
      x1003_outr_Switch_obj.mask := true.B & true.B
      x1003_outr_Switch_obj.configure("x1003_outr_Switch_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1003_outr_Switch_obj.kernel()
    }
    val module = Module(new x1005_outr_SwitchCase_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END SwitchCase x1005_outr_SwitchCase **/
