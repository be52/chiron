package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x665 -> x666 -> x667 -> x1134 -> x1137 **/
/** BEGIN None x665_inr_Foreach **/
class x665_inr_Foreach_kernel(
  list_b632: List[Bool],
  list_b631: List[FixedPoint],
  list_x602: List[DecoupledIO[AppLoadData]],
  list_x597_tab_sram_0: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Pipelined, false   , latency = 8.0.toInt, myName = "x665_inr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x665_inr_Foreach_iiCtr"))
  
  abstract class x665_inr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x602 = Flipped(Decoupled(new AppLoadData(ModuleParams.getParams("x602_p").asInstanceOf[(Int, Int)] )))
      val in_x634_reg = Flipped(new StandardInterface(ModuleParams.getParams("x634_reg_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x633_reg = Flipped(new StandardInterface(ModuleParams.getParams("x633_reg_p").asInstanceOf[MemParams] ))
      val in_b631 = Input(new FixedPoint(true, 32, 0))
      val in_b632 = Input(Bool())
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x602 = {io.in_x602} 
    def x634_reg = {io.in_x634_reg} ; io.in_x634_reg := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x633_reg = {io.in_x633_reg} ; io.in_x633_reg := DontCare
    def b631 = {io.in_b631} 
    def b632 = {io.in_b632} 
  }
  def connectWires0(module: x665_inr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    module.io.in_x602 <> x602
    x634_reg.connectLedger(module.io.in_x634_reg)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x633_reg.connectLedger(module.io.in_x633_reg)
    module.io.in_b631 <> b631
    module.io.in_b632 <> b632
  }
  val b632 = list_b632(0)
  val b631 = list_b631(0)
  val x602 = list_x602(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x634_reg = list_x597_tab_sram_0(1)
  val x598_tab_sram_1 = list_x597_tab_sram_0(2)
  val x633_reg = list_x597_tab_sram_0(3)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x665_inr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x665_inr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x665_inr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b648 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b648.suggestName("b648")
      val b649 = ~io.sigsIn.cchainOutputs.head.oobs(0); b649.suggestName("b649")
      val x650_rd_x633 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x650_rd_x633""")
      val x650_rd_x633_banks = List[UInt]()
      val x650_rd_x633_ofs = List[UInt]()
      val x650_rd_x633_en = List[Bool](true.B)
      x650_rd_x633.toSeq.zip(x633_reg.connectRPort(650, x650_rd_x633_banks, x650_rd_x633_ofs, io.sigsIn.backpressure, x650_rd_x633_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x651 = Wire(Bool()).suggestName("""x651""")
      x651.r := Math.lte(x650_rd_x633, b648, Some(0.4), true.B,"x651").r
      val x652_rd_x634 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x652_rd_x634""")
      val x652_rd_x634_banks = List[UInt]()
      val x652_rd_x634_ofs = List[UInt]()
      val x652_rd_x634_en = List[Bool](true.B)
      x652_rd_x634.toSeq.zip(x634_reg.connectRPort(652, x652_rd_x634_banks, x652_rd_x634_ofs, io.sigsIn.backpressure, x652_rd_x634_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x653 = Wire(Bool()).suggestName("""x653""")
      x653.r := Math.lt(b648, x652_rd_x634, Some(0.4), true.B,"x653").r
      val x654 = Wire(Bool()).suggestName("""x654""")
      x654 := x651 & x653
      val x655_sub = Wire(new FixedPoint(true, 32, 0)).suggestName("""x655_sub""")
      x655_sub.r := Math.sub(b648,x650_rd_x633,Some(1.0), true.B, Truncate, Wrapping, "x655_sub").r
      val x656 = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x656""")
      x602.ready := b649 & b632 & (io.sigsIn.datapathEn) 
      (0 until 1).map{ i => x656(i).r := x602.bits.rdata(i).r }
      val x1146 = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("x1146_x656_D1") 
      (0 until 1).foreach{i => x1146(i).r := getRetimed(x656(i).r, 1.toInt, io.sigsIn.backpressure)}
      val x657_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x657_elem_0""")
      x657_elem_0.r := x1146(0).r
      val x1147 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1147_b631_D1") 
      x1147.r := getRetimed(b631.r, 1.toInt, io.sigsIn.backpressure)
      val x1123 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1123""")
      x1123.r := Math.fma(x1147,10L.FP(true, 32, 0),x655_sub,Some(6.0), true.B, "x1123").toFixed(x1123, "cast_x1123").r
      val x1148 = Wire(Bool()).suggestName("x1148_x654_D7") 
      x1148.r := getRetimed(x654.r, 7.toInt, io.sigsIn.backpressure)
      val x1149 = Wire(new FloatingPoint(24, 8)).suggestName("x1149_x657_elem_0_D6") 
      x1149.r := getRetimed(x657_elem_0.r, 6.toInt, io.sigsIn.backpressure)
      val x1150 = Wire(Bool()).suggestName("x1150_b649_D7") 
      x1150.r := getRetimed(b649.r, 7.toInt, io.sigsIn.backpressure)
      val x1151 = Wire(Bool()).suggestName("x1151_b632_D7") 
      x1151.r := getRetimed(b632.r, 7.toInt, io.sigsIn.backpressure)
      val x663_wr_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x663_wr_ofs = List[UInt](x1123.r)
      val x663_wr_en = List[Bool](true.B)
      val x663_wr_data = List[UInt](x1149.r)
      x597_tab_sram_0.connectWPort(663, x663_wr_banks, x663_wr_ofs, x663_wr_data, x663_wr_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(7.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1148 & x1150 & x1151))
      val x664_wr_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x664_wr_ofs = List[UInt](x1123.r)
      val x664_wr_en = List[Bool](true.B)
      val x664_wr_data = List[UInt](x1149.r)
      x598_tab_sram_1.connectWPort(664, x664_wr_banks, x664_wr_ofs, x664_wr_data, x664_wr_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(7.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1148 & x1150 & x1151))
    }
    val module = Module(new x665_inr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x665_inr_Foreach **/
