package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x756 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x756_outr_SwitchCase **/
class x756_outr_SwitchCase_kernel(
  list_x683_reg: List[StandardInterface],
  list_x580_nrow: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 4, isFSM = false   , latency = 0.0.toInt, myName = "x756_outr_SwitchCase_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x756_outr_SwitchCase_iiCtr"))
  
  abstract class x756_outr_SwitchCase_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x683_reg = Flipped(new StandardInterface(ModuleParams.getParams("x683_reg_p").asInstanceOf[MemParams] ))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x682_reg = Flipped(new StandardInterface(ModuleParams.getParams("x682_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x671_no_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x671_no_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(4, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(4, 1))
      val rr = Input(Bool())
    })
    def x683_reg = {io.in_x683_reg} ; io.in_x683_reg := DontCare
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x682_reg = {io.in_x682_reg} ; io.in_x682_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x580_nrow = {io.in_x580_nrow} 
    def x671_no_pivot_col_0 = {io.in_x671_no_pivot_col_0} ; io.in_x671_no_pivot_col_0 := DontCare
    def x581_ncol = {io.in_x581_ncol} 
  }
  def connectWires0(module: x756_outr_SwitchCase_module)(implicit stack: List[KernelHash]): Unit = {
    x683_reg.connectLedger(module.io.in_x683_reg)
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x682_reg.connectLedger(module.io.in_x682_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    module.io.in_x580_nrow <> x580_nrow
    x671_no_pivot_col_0.connectLedger(module.io.in_x671_no_pivot_col_0)
    module.io.in_x581_ncol <> x581_ncol
  }
  val x683_reg = list_x683_reg(0)
  val x597_tab_sram_0 = list_x683_reg(1)
  val x598_tab_sram_1 = list_x683_reg(2)
  val x682_reg = list_x683_reg(3)
  val x679_pivot_col_0 = list_x683_reg(4)
  val x671_no_pivot_col_0 = list_x683_reg(5)
  val x580_nrow = list_x580_nrow(0)
  val x581_ncol = list_x580_nrow(1)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x756_outr_SwitchCase_obj")
    implicit val stack = ControllerStack.stack.toList
    class x756_outr_SwitchCase_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x756_outr_SwitchCase_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      // Controller Stack: Stack(x1006, x1020, x1137)
      val x697_inr_UnitPipe = new x697_inr_UnitPipe_kernel(List(x682_reg,x679_pivot_col_0) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x697_inr_UnitPipe.sm.io.ctrDone := risingEdge(x697_inr_UnitPipe.sm.io.ctrInc)
      x697_inr_UnitPipe.backpressure := true.B | x697_inr_UnitPipe.sm.io.doneLatch
      x697_inr_UnitPipe.forwardpressure := true.B | x697_inr_UnitPipe.sm.io.doneLatch
      x697_inr_UnitPipe.sm.io.enableOut.zip(x697_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x697_inr_UnitPipe.sm.io.break := false.B
      x697_inr_UnitPipe.mask := true.B & true.B
      x697_inr_UnitPipe.configure("x697_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x697_inr_UnitPipe.kernel()
      val x1087_rd_x581 = Wire(new FixedPoint(true, 32, 0))
      x1087_rd_x581.r := x581_ncol.r
      val x699_ctr = new CtrObject(Left(Some(0)), Right(x1087_rd_x581), Left(Some(1)), 1, 32, false)
      val x700_ctrchain = (new CChainObject(List[CtrObject](x699_ctr), "x700_ctrchain")).cchain.io 
      x700_ctrchain.setup.isStream := false.B
      ModuleParams.addParams("x700_ctrchain_p", (x700_ctrchain.par, x700_ctrchain.widths))
      val x1088_rd_x683 = Wire(Bool()).suggestName("""x1088_rd_x683""")
      val x1088_rd_x683_banks = List[UInt]()
      val x1088_rd_x683_ofs = List[UInt]()
      val x1088_rd_x683_en = List[Bool](true.B)
      x1088_rd_x683.toSeq.zip(x683_reg.connectRPort(1088, x1088_rd_x683_banks, x1088_rd_x683_ofs, io.sigsIn.backpressure, x1088_rd_x683_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x728_inr_Foreach = new x728_inr_Foreach_kernel(List(x1088_rd_x683), List(x597_tab_sram_0,x598_tab_sram_1,x682_reg,x679_pivot_col_0), List(x580_nrow) , Some(me), List(x700_ctrchain), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x728_inr_Foreach.sm.io.ctrDone := (x728_inr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x728_inr_Foreach.backpressure := true.B | x728_inr_Foreach.sm.io.doneLatch
      x728_inr_Foreach.forwardpressure := true.B | x728_inr_Foreach.sm.io.doneLatch
      x728_inr_Foreach.sm.io.enableOut.zip(x728_inr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x728_inr_Foreach.sm.io.break := false.B
      x728_inr_Foreach.mask := ~x728_inr_Foreach.cchain.head.output.noop & x1088_rd_x683
      x728_inr_Foreach.configure("x728_inr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x728_inr_Foreach.kernel()
      val x729_reg = (new x729_reg).m.io.asInstanceOf[StandardInterface]
      val x730_reg = (new x730_reg).m.io.asInstanceOf[StandardInterface]
      val x746_inr_UnitPipe = new x746_inr_UnitPipe_kernel(List(x730_reg,x597_tab_sram_0,x729_reg,x682_reg,x679_pivot_col_0), List(x580_nrow) , Some(me), List(), 2, 1, 1, List(1), List(32), breakpoints, rr)
      x746_inr_UnitPipe.sm.io.ctrDone := risingEdge(x746_inr_UnitPipe.sm.io.ctrInc)
      x746_inr_UnitPipe.backpressure := true.B | x746_inr_UnitPipe.sm.io.doneLatch
      x746_inr_UnitPipe.forwardpressure := true.B | x746_inr_UnitPipe.sm.io.doneLatch
      x746_inr_UnitPipe.sm.io.enableOut.zip(x746_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x746_inr_UnitPipe.sm.io.break := false.B
      x746_inr_UnitPipe.mask := true.B & true.B
      x746_inr_UnitPipe.configure("x746_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x746_inr_UnitPipe.kernel()
      val x1089_rd_x729 = Wire(Bool()).suggestName("""x1089_rd_x729""")
      val x1089_rd_x729_banks = List[UInt]()
      val x1089_rd_x729_ofs = List[UInt]()
      val x1089_rd_x729_en = List[Bool](true.B)
      x1089_rd_x729.toSeq.zip(x729_reg.connectRPort(1089, x1089_rd_x729_banks, x1089_rd_x729_ofs, io.sigsIn.backpressure, x1089_rd_x729_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1090_rd_x730 = Wire(Bool()).suggestName("""x1090_rd_x730""")
      val x1090_rd_x730_banks = List[UInt]()
      val x1090_rd_x730_ofs = List[UInt]()
      val x1090_rd_x730_en = List[Bool](true.B)
      x1090_rd_x730.toSeq.zip(x730_reg.connectRPort(1090, x1090_rd_x730_banks, x1090_rd_x730_ofs, io.sigsIn.backpressure, x1090_rd_x730_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x754_inr_Switch_obj = new x754_inr_Switch_kernel(List(x1089_rd_x729,x1090_rd_x730), List(x729_reg,x682_reg,x671_no_pivot_col_0) , Some(me), List(), 3, 2, 1, List(1), List(32), breakpoints, rr)
      x754_inr_Switch_obj.sm.io.selectsIn(0) := x1089_rd_x729
      x754_inr_Switch_obj.sm.io.selectsIn(1) := x1090_rd_x730
      x754_inr_Switch_obj.backpressure := true.B | x754_inr_Switch_obj.sm.io.doneLatch
      x754_inr_Switch_obj.forwardpressure := true.B | x754_inr_Switch_obj.sm.io.doneLatch
      x754_inr_Switch_obj.sm.io.enableOut.zip(x754_inr_Switch_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x754_inr_Switch_obj.sm.io.break := false.B
      x754_inr_Switch_obj.mask := true.B & true.B
      x754_inr_Switch_obj.configure("x754_inr_Switch_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x754_inr_Switch_obj.kernel()
    }
    val module = Module(new x756_outr_SwitchCase_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END SwitchCase x756_outr_SwitchCase **/
