package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x984 -> x985 -> x1135 -> x1136 -> x997 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x984_outr_Switch **/
class x984_outr_Switch_kernel(
  list_b916: List[FixedPoint],
  list_x920_reg: List[NBufInterface],
  list_x581_ncol: List[UInt],
  list_x760_reg: List[StandardInterface],
  list_x1111_rd_x918: List[Bool],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Fork, 2, isFSM = false   ,cases = 2, latency = 0.0.toInt, myName = "x984_outr_Switch_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x984_outr_Switch_iiCtr"))
  
  abstract class x984_outr_Switch_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x760_reg = Flipped(new StandardInterface(ModuleParams.getParams("x760_reg_p").asInstanceOf[MemParams] ))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x898_reg = Flipped(new StandardInterface(ModuleParams.getParams("x898_reg_p").asInstanceOf[MemParams] ))
      val in_x920_reg = Flipped(new NBufInterface(ModuleParams.getParams("x920_reg_p").asInstanceOf[NBufParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_b916 = Input(new FixedPoint(true, 32, 0))
      val in_x847_reg = Flipped(new StandardInterface(ModuleParams.getParams("x847_reg_p").asInstanceOf[MemParams] ))
      val in_x1111_rd_x918 = Input(Bool())
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x919_reg = Flipped(new NBufInterface(ModuleParams.getParams("x919_reg_p").asInstanceOf[NBufParams] ))
      val in_x848_reg = Flipped(new StandardInterface(ModuleParams.getParams("x848_reg_p").asInstanceOf[MemParams] ))
      val in_x1112_rd_x920 = Input(Bool())
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x899_reg = Flipped(new StandardInterface(ModuleParams.getParams("x899_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_x918_reg = Flipped(new NBufInterface(ModuleParams.getParams("x918_reg_p").asInstanceOf[NBufParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x760_reg = {io.in_x760_reg} ; io.in_x760_reg := DontCare
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x898_reg = {io.in_x898_reg} ; io.in_x898_reg := DontCare
    def x920_reg = {io.in_x920_reg} ; io.in_x920_reg := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def b916 = {io.in_b916} 
    def x847_reg = {io.in_x847_reg} ; io.in_x847_reg := DontCare
    def x1111_rd_x918 = {io.in_x1111_rd_x918} 
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x919_reg = {io.in_x919_reg} ; io.in_x919_reg := DontCare
    def x848_reg = {io.in_x848_reg} ; io.in_x848_reg := DontCare
    def x1112_rd_x920 = {io.in_x1112_rd_x920} 
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x581_ncol = {io.in_x581_ncol} 
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x899_reg = {io.in_x899_reg} ; io.in_x899_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
    def x918_reg = {io.in_x918_reg} ; io.in_x918_reg := DontCare
  }
  def connectWires0(module: x984_outr_Switch_module)(implicit stack: List[KernelHash]): Unit = {
    x760_reg.connectLedger(module.io.in_x760_reg)
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x898_reg.connectLedger(module.io.in_x898_reg)
    x920_reg.connectLedger(module.io.in_x920_reg)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    module.io.in_b916 <> b916
    x847_reg.connectLedger(module.io.in_x847_reg)
    module.io.in_x1111_rd_x918 <> x1111_rd_x918
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x919_reg.connectLedger(module.io.in_x919_reg)
    x848_reg.connectLedger(module.io.in_x848_reg)
    module.io.in_x1112_rd_x920 <> x1112_rd_x920
    x685_reg.connectLedger(module.io.in_x685_reg)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    module.io.in_x581_ncol <> x581_ncol
    x759_reg.connectLedger(module.io.in_x759_reg)
    x899_reg.connectLedger(module.io.in_x899_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
    x918_reg.connectLedger(module.io.in_x918_reg)
  }
  val b916 = list_b916(0)
  val x920_reg = list_x920_reg(0)
  val x919_reg = list_x920_reg(1)
  val x918_reg = list_x920_reg(2)
  val x581_ncol = list_x581_ncol(0)
  val x760_reg = list_x760_reg(0)
  val x597_tab_sram_0 = list_x760_reg(1)
  val x898_reg = list_x760_reg(2)
  val x598_tab_sram_1 = list_x760_reg(3)
  val x847_reg = list_x760_reg(4)
  val x679_pivot_col_0 = list_x760_reg(5)
  val x674_pivot_element_0 = list_x760_reg(6)
  val x848_reg = list_x760_reg(7)
  val x685_reg = list_x760_reg(8)
  val x680_pivot_row_0 = list_x760_reg(9)
  val x759_reg = list_x760_reg(10)
  val x899_reg = list_x760_reg(11)
  val x684_reg = list_x760_reg(12)
  val x1111_rd_x918 = list_x1111_rd_x918(0)
  val x1112_rd_x920 = list_x1111_rd_x918(1)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x984_outr_Switch_obj")
    implicit val stack = ControllerStack.stack.toList
    class x984_outr_Switch_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x984_outr_Switch_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x982_outr_SwitchCase_obj = new x982_outr_SwitchCase_kernel(List(b916), List(x919_reg,x918_reg), List(x760_reg,x597_tab_sram_0,x898_reg,x598_tab_sram_1,x847_reg,x679_pivot_col_0,x674_pivot_element_0,x848_reg,x685_reg,x680_pivot_row_0,x759_reg,x899_reg,x684_reg), List(x581_ncol) , Some(me), List(), 0, 2, 1, List(1), List(32), breakpoints, rr)
      x982_outr_SwitchCase_obj.sm.io.ctrDone := risingEdge(x982_outr_SwitchCase_obj.sm.io.ctrInc)
      x982_outr_SwitchCase_obj.backpressure := true.B | x982_outr_SwitchCase_obj.sm.io.doneLatch
      x982_outr_SwitchCase_obj.forwardpressure := true.B | x982_outr_SwitchCase_obj.sm.io.doneLatch
      x982_outr_SwitchCase_obj.sm.io.enableOut.zip(x982_outr_SwitchCase_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x982_outr_SwitchCase_obj.sm.io.break := false.B
      x982_outr_SwitchCase_obj.mask := true.B & true.B
      x982_outr_SwitchCase_obj.configure("x982_outr_SwitchCase_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x982_outr_SwitchCase_obj.kernel()
      val x983_inr_SwitchCase_obj = new x983_inr_SwitchCase_kernel(  Some(me), List(), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x983_inr_SwitchCase_obj.baseEn := io.sigsIn.smSelectsOut(x983_inr_SwitchCase_obj.childId)
      x983_inr_SwitchCase_obj.sm.io.ctrDone := risingEdge(x983_inr_SwitchCase_obj.sm.io.ctrInc)
      x983_inr_SwitchCase_obj.backpressure := true.B | x983_inr_SwitchCase_obj.sm.io.doneLatch
      x983_inr_SwitchCase_obj.forwardpressure := true.B | x983_inr_SwitchCase_obj.sm.io.doneLatch
      x983_inr_SwitchCase_obj.sm.io.enableOut.zip(x983_inr_SwitchCase_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x983_inr_SwitchCase_obj.sm.io.break := false.B
      x983_inr_SwitchCase_obj.mask := true.B & true.B
      x983_inr_SwitchCase_obj.configure("x983_inr_SwitchCase_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x983_inr_SwitchCase_obj.kernel()
      x918_reg.connectStageCtrl((io.sigsIn.done).DS(1.toInt, rr, io.sigsIn.backpressure), io.sigsIn.baseEn, 1)
      x919_reg.connectStageCtrl((io.sigsIn.done).DS(1.toInt, rr, io.sigsIn.backpressure), io.sigsIn.baseEn, 1)
      x920_reg.connectStageCtrl((io.sigsIn.done).DS(1.toInt, rr, io.sigsIn.backpressure), io.sigsIn.baseEn, 1)
    }
    val module = Module(new x984_outr_Switch_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END Switch x984_outr_Switch **/
