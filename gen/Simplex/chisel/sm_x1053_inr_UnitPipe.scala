package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1053 -> x1075 -> x1080 -> x1081 -> x1137 **/
/** BEGIN None x1053_inr_UnitPipe **/
class x1053_inr_UnitPipe_kernel(
  list_b1027: List[FixedPoint],
  list_x1022: List[DecoupledIO[AppCommandDense]],
  list_x1031_reg: List[StandardInterface],
  list_x581_ncol: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 10.8.toInt, myName = "x1053_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1053_inr_UnitPipe_iiCtr"))
  
  abstract class x1053_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x1031_reg = Flipped(new StandardInterface(ModuleParams.getParams("x1031_reg_p").asInstanceOf[MemParams] ))
      val in_b1027 = Input(new FixedPoint(true, 32, 0))
      val in_x1029_reg = Flipped(new StandardInterface(ModuleParams.getParams("x1029_reg_p").asInstanceOf[MemParams] ))
      val in_x1022 = Decoupled(new AppCommandDense(ModuleParams.getParams("x1022_p").asInstanceOf[(Int,Int)] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x1030_reg = Flipped(new StandardInterface(ModuleParams.getParams("x1030_reg_p").asInstanceOf[MemParams] ))
      val in_x596_tab_dram_out = Input(new FixedPoint(true, 64, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x1031_reg = {io.in_x1031_reg} ; io.in_x1031_reg := DontCare
    def b1027 = {io.in_b1027} 
    def x1029_reg = {io.in_x1029_reg} ; io.in_x1029_reg := DontCare
    def x1022 = {io.in_x1022} 
    def x581_ncol = {io.in_x581_ncol} 
    def x1030_reg = {io.in_x1030_reg} ; io.in_x1030_reg := DontCare
    def x596_tab_dram_out = {io.in_x596_tab_dram_out} 
  }
  def connectWires0(module: x1053_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x1031_reg.connectLedger(module.io.in_x1031_reg)
    module.io.in_b1027 <> b1027
    x1029_reg.connectLedger(module.io.in_x1029_reg)
    module.io.in_x1022 <> x1022
    module.io.in_x581_ncol <> x581_ncol
    x1030_reg.connectLedger(module.io.in_x1030_reg)
    module.io.in_x596_tab_dram_out <> x596_tab_dram_out
  }
  val b1027 = list_b1027(0)
  val x596_tab_dram_out = list_b1027(1)
  val x1022 = list_x1022(0)
  val x1031_reg = list_x1031_reg(0)
  val x1029_reg = list_x1031_reg(1)
  val x1030_reg = list_x1031_reg(2)
  val x581_ncol = list_x581_ncol(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1053_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x1053_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1053_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x1032_rd_x581 = Wire(new FixedPoint(true, 32, 0))
      x1032_rd_x581.r := x581_ncol.r
      val x1033_mul = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1033_mul""")
      val ensig0 = Wire(Bool())
      ensig0 := x1022.ready
      x1033_mul.r := (Math.mul(b1027, x1032_rd_x581, Some(6.0), ensig0, Truncate, Wrapping, "x1033_mul")).r
      val x1034 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1034""")
      x1034.r := Math.arith_right_shift(x1033_mul, 4, Some(0.2), ensig0,"x1034").r
      val x1035 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1035""")
      x1035.r := Math.arith_left_shift(x1034, 4, Some(0.2), ensig0,"x1035").r
      val x1131 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1131""")
      x1131.r := Math.arith_left_shift(x1034, 6, Some(0.2), ensig0,"x1131").r
      val x1037_sub = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1037_sub""")
      x1037_sub.r := Math.sub(x1033_mul,x1035,Some(1.0), ensig0, Truncate, Wrapping, "x1037_sub").r
      val x1225 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1225_x1032_rd_x581_D7") 
      x1225.r := getRetimed(x1032_rd_x581.r, 7.toInt, io.sigsIn.backpressure)
      val x1039_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1039_sum""")
      x1039_sum.r := Math.add(x1037_sub,x1225,Some(1.0), ensig0, Truncate, Wrapping, "x1039_sum").r
      val x1040_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1040_sum""")
      x1040_sum.r := Math.add(x1039_sum,15L.FP(true, 32, 0),Some(1.0), ensig0, Truncate, Wrapping, "x1040_sum").r
      val x1041 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1041""")
      x1041.r := Math.arith_right_shift(x1040_sum, 4, Some(0.2), ensig0,"x1041").r
      val x1042 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1042""")
      x1042.r := Math.arith_left_shift(x1041, 4, Some(0.2), ensig0,"x1042").r
      val x1132 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1132""")
      x1132.r := Math.arith_left_shift(x1041, 6, Some(0.2), ensig0,"x1132").r
      val x1044 = Wire(new FixedPoint(true, 64, 0)).suggestName("""x1044""")
      x1044.r := Math.fix2fix(x1131, true, 64, 0, Some(0.0), ensig0, Truncate, Wrapping, "x1044").r
      val x1045 = x596_tab_dram_out
      val x1226 = Wire(new FixedPoint(true, 64, 0)).suggestName("x1226_x1045_D6") 
      x1226.r := getRetimed(x1045.r, 6.toInt, io.sigsIn.backpressure)
      val x1046_sum = Wire(new FixedPoint(true, 64, 0)).suggestName("""x1046_sum""")
      x1046_sum.r := Math.add(x1044,x1226,Some(2.0), ensig0, Truncate, Wrapping, "x1046_sum").r
      val x1227 = Wire(new FixedPoint(true, 64, 0)).suggestName("x1227_x1046_sum_D1") 
      x1227.r := getRetimed(x1046_sum.r, 1.toInt, io.sigsIn.backpressure)
      val x1047_tuple = Wire(UInt(97.W)).suggestName("""x1047_tuple""")
      x1047_tuple.r := Cat(false.B,x1132.r,x1227.r)
      val x1048 = true.B
      val x1228 = Wire(Bool()).suggestName("x1228_x1048_D9") 
      x1228.r := getRetimed(x1048.r, 9.toInt, io.sigsIn.backpressure)
      x1022.valid := (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(9.8.toInt.toInt, rr, io.sigsIn.backpressure) & x1228 & io.sigsIn.backpressure
      x1022.bits.addr := x1047_tuple(63,0)
      x1022.bits.size := x1047_tuple(95,64)
      val x1050_wr_x1029_banks = List[UInt]()
      val x1050_wr_x1029_ofs = List[UInt]()
      val x1050_wr_x1029_en = List[Bool](true.B)
      val x1050_wr_x1029_data = List[UInt](x1037_sub.r)
      x1029_reg.connectWPort(1050, x1050_wr_x1029_banks, x1050_wr_x1029_ofs, x1050_wr_x1029_data, x1050_wr_x1029_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(7.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x1051_wr_x1030_banks = List[UInt]()
      val x1051_wr_x1030_ofs = List[UInt]()
      val x1051_wr_x1030_en = List[Bool](true.B)
      val x1051_wr_x1030_data = List[UInt](x1039_sum.r)
      x1030_reg.connectWPort(1051, x1051_wr_x1030_banks, x1051_wr_x1030_ofs, x1051_wr_x1030_data, x1051_wr_x1030_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(8.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x1052_wr_x1031_banks = List[UInt]()
      val x1052_wr_x1031_ofs = List[UInt]()
      val x1052_wr_x1031_en = List[Bool](true.B)
      val x1052_wr_x1031_data = List[UInt](x1042.r)
      x1031_reg.connectWPort(1052, x1052_wr_x1031_banks, x1052_wr_x1031_ofs, x1052_wr_x1031_data, x1052_wr_x1031_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(9.8.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x1053_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x1053_inr_UnitPipe **/
