package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x833 -> x844 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x833_inr_UnitPipe **/
class x833_inr_UnitPipe_kernel(
  list_x825_reg: List[StandardInterface],
  list_x582_ncons: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.4.toInt, myName = "x833_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x833_inr_UnitPipe_iiCtr"))
  
  abstract class x833_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x582_ncons = Input(UInt(64.W))
      val in_x825_reg = Flipped(new StandardInterface(ModuleParams.getParams("x825_reg_p").asInstanceOf[MemParams] ))
      val in_x826_reg = Flipped(new StandardInterface(ModuleParams.getParams("x826_reg_p").asInstanceOf[MemParams] ))
      val in_x673_no_pivot_row_count_0 = Flipped(new StandardInterface(ModuleParams.getParams("x673_no_pivot_row_count_0_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x582_ncons = {io.in_x582_ncons} 
    def x825_reg = {io.in_x825_reg} ; io.in_x825_reg := DontCare
    def x826_reg = {io.in_x826_reg} ; io.in_x826_reg := DontCare
    def x673_no_pivot_row_count_0 = {io.in_x673_no_pivot_row_count_0} ; io.in_x673_no_pivot_row_count_0 := DontCare
  }
  def connectWires0(module: x833_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    module.io.in_x582_ncons <> x582_ncons
    x825_reg.connectLedger(module.io.in_x825_reg)
    x826_reg.connectLedger(module.io.in_x826_reg)
    x673_no_pivot_row_count_0.connectLedger(module.io.in_x673_no_pivot_row_count_0)
  }
  val x825_reg = list_x825_reg(0)
  val x826_reg = list_x825_reg(1)
  val x673_no_pivot_row_count_0 = list_x825_reg(2)
  val x582_ncons = list_x582_ncons(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x833_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x833_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x833_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x827_rd_x673 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x827_rd_x673""")
      val x827_rd_x673_banks = List[UInt]()
      val x827_rd_x673_ofs = List[UInt]()
      val x827_rd_x673_en = List[Bool](true.B)
      x827_rd_x673.toSeq.zip(x673_no_pivot_row_count_0.connectRPort(827, x827_rd_x673_banks, x827_rd_x673_ofs, io.sigsIn.backpressure, x827_rd_x673_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x828_rd_x582 = Wire(new FixedPoint(true, 32, 0))
      x828_rd_x582.r := x582_ncons.r
      val x829 = Wire(Bool()).suggestName("""x829""")
      x829.r := Math.eql(x827_rd_x673, x828_rd_x582, Some(0.2), true.B,"x829").r
      val x830 = Wire(Bool()).suggestName("""x830""")
      x830 := ~x829
      val x831_wr_x825_banks = List[UInt]()
      val x831_wr_x825_ofs = List[UInt]()
      val x831_wr_x825_en = List[Bool](true.B)
      val x831_wr_x825_data = List[UInt](x829.r)
      x825_reg.connectWPort(831, x831_wr_x825_banks, x831_wr_x825_ofs, x831_wr_x825_data, x831_wr_x825_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x832_wr_x826_banks = List[UInt]()
      val x832_wr_x826_ofs = List[UInt]()
      val x832_wr_x826_en = List[Bool](true.B)
      val x832_wr_x826_data = List[UInt](x830.r)
      x826_reg.connectWPort(832, x832_wr_x826_banks, x832_wr_x826_ofs, x832_wr_x826_data, x832_wr_x826_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x833_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x833_inr_UnitPipe **/
