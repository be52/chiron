package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x982 -> x984 -> x985 -> x1135 -> x1136 -> x997 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x982_outr_SwitchCase **/
class x982_outr_SwitchCase_kernel(
  list_b916: List[FixedPoint],
  list_x919_reg: List[NBufInterface],
  list_x760_reg: List[StandardInterface],
  list_x581_ncol: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 2, isFSM = false   , latency = 0.0.toInt, myName = "x982_outr_SwitchCase_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x982_outr_SwitchCase_iiCtr"))
  
  abstract class x982_outr_SwitchCase_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x760_reg = Flipped(new StandardInterface(ModuleParams.getParams("x760_reg_p").asInstanceOf[MemParams] ))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x898_reg = Flipped(new StandardInterface(ModuleParams.getParams("x898_reg_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_b916 = Input(new FixedPoint(true, 32, 0))
      val in_x847_reg = Flipped(new StandardInterface(ModuleParams.getParams("x847_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x919_reg = Flipped(new NBufInterface(ModuleParams.getParams("x919_reg_p").asInstanceOf[NBufParams] ))
      val in_x848_reg = Flipped(new StandardInterface(ModuleParams.getParams("x848_reg_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x899_reg = Flipped(new StandardInterface(ModuleParams.getParams("x899_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_x918_reg = Flipped(new NBufInterface(ModuleParams.getParams("x918_reg_p").asInstanceOf[NBufParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x760_reg = {io.in_x760_reg} ; io.in_x760_reg := DontCare
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x898_reg = {io.in_x898_reg} ; io.in_x898_reg := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def b916 = {io.in_b916} 
    def x847_reg = {io.in_x847_reg} ; io.in_x847_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x919_reg = {io.in_x919_reg} ; io.in_x919_reg := DontCare
    def x848_reg = {io.in_x848_reg} ; io.in_x848_reg := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x581_ncol = {io.in_x581_ncol} 
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x899_reg = {io.in_x899_reg} ; io.in_x899_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
    def x918_reg = {io.in_x918_reg} ; io.in_x918_reg := DontCare
  }
  def connectWires0(module: x982_outr_SwitchCase_module)(implicit stack: List[KernelHash]): Unit = {
    x760_reg.connectLedger(module.io.in_x760_reg)
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x898_reg.connectLedger(module.io.in_x898_reg)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    module.io.in_b916 <> b916
    x847_reg.connectLedger(module.io.in_x847_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x919_reg.connectLedger(module.io.in_x919_reg)
    x848_reg.connectLedger(module.io.in_x848_reg)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    module.io.in_x581_ncol <> x581_ncol
    x759_reg.connectLedger(module.io.in_x759_reg)
    x899_reg.connectLedger(module.io.in_x899_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
    x918_reg.connectLedger(module.io.in_x918_reg)
  }
  val b916 = list_b916(0)
  val x919_reg = list_x919_reg(0)
  val x918_reg = list_x919_reg(1)
  val x760_reg = list_x760_reg(0)
  val x597_tab_sram_0 = list_x760_reg(1)
  val x898_reg = list_x760_reg(2)
  val x598_tab_sram_1 = list_x760_reg(3)
  val x847_reg = list_x760_reg(4)
  val x679_pivot_col_0 = list_x760_reg(5)
  val x674_pivot_element_0 = list_x760_reg(6)
  val x848_reg = list_x760_reg(7)
  val x685_reg = list_x760_reg(8)
  val x680_pivot_row_0 = list_x760_reg(9)
  val x759_reg = list_x760_reg(10)
  val x899_reg = list_x760_reg(11)
  val x684_reg = list_x760_reg(12)
  val x581_ncol = list_x581_ncol(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x982_outr_SwitchCase_obj")
    implicit val stack = ControllerStack.stack.toList
    class x982_outr_SwitchCase_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x982_outr_SwitchCase_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      // Controller Stack: Stack(x984, x985, x1135, x1136, x997, x999, x1000, x1002, x1003, x1005, x1006, x1020, x1137)
      val x944_inr_UnitPipe = new x944_inr_UnitPipe_kernel(List(b916), List(x918_reg), List(x597_tab_sram_0,x898_reg,x847_reg,x679_pivot_col_0,x674_pivot_element_0,x759_reg,x684_reg) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x944_inr_UnitPipe.sm.io.ctrDone := risingEdge(x944_inr_UnitPipe.sm.io.ctrInc)
      x944_inr_UnitPipe.backpressure := true.B | x944_inr_UnitPipe.sm.io.doneLatch
      x944_inr_UnitPipe.forwardpressure := true.B | x944_inr_UnitPipe.sm.io.doneLatch
      x944_inr_UnitPipe.sm.io.enableOut.zip(x944_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x944_inr_UnitPipe.sm.io.break := false.B
      x944_inr_UnitPipe.mask := true.B & true.B
      x944_inr_UnitPipe.configure("x944_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x944_inr_UnitPipe.kernel()
      val x1113_rd_x581 = Wire(new FixedPoint(true, 32, 0))
      x1113_rd_x581.r := x581_ncol.r
      val x946_ctr = new CtrObject(Left(Some(0)), Right(x1113_rd_x581), Left(Some(1)), 1, 32, false)
      val x947_ctrchain = (new CChainObject(List[CtrObject](x946_ctr), "x947_ctrchain")).cchain.io 
      x947_ctrchain.setup.isStream := false.B
      ModuleParams.addParams("x947_ctrchain_p", (x947_ctrchain.par, x947_ctrchain.widths))
      val x1114_rd_x685 = Wire(Bool()).suggestName("""x1114_rd_x685""")
      val x1114_rd_x685_banks = List[UInt]()
      val x1114_rd_x685_ofs = List[UInt]()
      val x1114_rd_x685_en = List[Bool](true.B)
      x1114_rd_x685.toSeq.zip(x685_reg.connectRPort(1114, x1114_rd_x685_banks, x1114_rd_x685_ofs, io.sigsIn.backpressure, x1114_rd_x685_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1115_rd_x759 = Wire(Bool()).suggestName("""x1115_rd_x759""")
      val x1115_rd_x759_banks = List[UInt]()
      val x1115_rd_x759_ofs = List[UInt]()
      val x1115_rd_x759_en = List[Bool](true.B)
      x1115_rd_x759.toSeq.zip(x759_reg.connectRPort(1115, x1115_rd_x759_banks, x1115_rd_x759_ofs, io.sigsIn.backpressure, x1115_rd_x759_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1116_rd_x847 = Wire(Bool()).suggestName("""x1116_rd_x847""")
      val x1116_rd_x847_banks = List[UInt]()
      val x1116_rd_x847_ofs = List[UInt]()
      val x1116_rd_x847_en = List[Bool](true.B)
      x1116_rd_x847.toSeq.zip(x847_reg.connectRPort(1116, x1116_rd_x847_banks, x1116_rd_x847_ofs, io.sigsIn.backpressure, x1116_rd_x847_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1117_rd_x899 = Wire(Bool()).suggestName("""x1117_rd_x899""")
      val x1117_rd_x899_banks = List[UInt]()
      val x1117_rd_x899_ofs = List[UInt]()
      val x1117_rd_x899_en = List[Bool](true.B)
      x1117_rd_x899.toSeq.zip(x899_reg.connectRPort(1117, x1117_rd_x899_banks, x1117_rd_x899_ofs, io.sigsIn.backpressure, x1117_rd_x899_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1118_rd_x918 = Wire(Bool()).suggestName("""x1118_rd_x918""")
      val x1118_rd_x918_banks = List[UInt]()
      val x1118_rd_x918_ofs = List[UInt]()
      val x1118_rd_x918_en = List[Bool](true.B)
      x1118_rd_x918.toSeq.zip(x918_reg.connectRPort(1118, x1118_rd_x918_banks, x1118_rd_x918_ofs, io.sigsIn.backpressure, x1118_rd_x918_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x980_inr_Foreach = new x980_inr_Foreach_kernel(List(x1115_rd_x759,x1118_rd_x918,x1114_rd_x685,x1117_rd_x899,x1116_rd_x847), List(b916), List(x919_reg), List(x760_reg,x597_tab_sram_0,x898_reg,x598_tab_sram_1,x674_pivot_element_0,x848_reg,x680_pivot_row_0,x684_reg) , Some(me), List(x947_ctrchain), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x980_inr_Foreach.sm.io.ctrDone := (x980_inr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x980_inr_Foreach.backpressure := true.B | x980_inr_Foreach.sm.io.doneLatch
      x980_inr_Foreach.forwardpressure := true.B | x980_inr_Foreach.sm.io.doneLatch
      x980_inr_Foreach.sm.io.enableOut.zip(x980_inr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x980_inr_Foreach.sm.io.break := false.B
      x980_inr_Foreach.mask := ~x980_inr_Foreach.cchain.head.output.noop & x1116_rd_x847 & x1118_rd_x918 & x1117_rd_x899 & x1114_rd_x685 & x1115_rd_x759
      x980_inr_Foreach.configure("x980_inr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x980_inr_Foreach.kernel()
    }
    val module = Module(new x982_outr_SwitchCase_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END SwitchCase x982_outr_SwitchCase **/
