package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1137 **/
/** BEGIN None RootController **/
class RootController_kernel(
  list_x596_tab_dram_out: List[FixedPoint],
  list_x1024: List[DecoupledIO[Bool]],
  list_x582_ncons: List[UInt],
  list_x1022: List[DecoupledIO[AppCommandDense]],
  list_x1023: List[DecoupledIO[AppStoreData]],
  list_x602: List[DecoupledIO[AppLoadData]],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 3, isFSM = false   , latency = 0.0.toInt, myName = "RootController_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "RootController_iiCtr"))
  
  abstract class RootController_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x1024 = Flipped(Decoupled(Bool()))
      val in_x582_ncons = Input(UInt(64.W))
      val in_x602 = Flipped(Decoupled(new AppLoadData(ModuleParams.getParams("x602_p").asInstanceOf[(Int, Int)] )))
      val in_x1022 = Decoupled(new AppCommandDense(ModuleParams.getParams("x1022_p").asInstanceOf[(Int,Int)] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x1023 = Decoupled(new AppStoreData(ModuleParams.getParams("x1023_p").asInstanceOf[(Int,Int)] ))
      val in_x600 = Decoupled(new AppCommandDense(ModuleParams.getParams("x600_p").asInstanceOf[(Int,Int)] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x596_tab_dram_out = Input(new FixedPoint(true, 64, 0))
      val in_x592_tab_dram = Input(new FixedPoint(true, 64, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(3, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(3, 1))
      val rr = Input(Bool())
    })
    def x1024 = {io.in_x1024} 
    def x582_ncons = {io.in_x582_ncons} 
    def x602 = {io.in_x602} 
    def x1022 = {io.in_x1022} 
    def x580_nrow = {io.in_x580_nrow} 
    def x1023 = {io.in_x1023} 
    def x600 = {io.in_x600} 
    def x581_ncol = {io.in_x581_ncol} 
    def x596_tab_dram_out = {io.in_x596_tab_dram_out} 
    def x592_tab_dram = {io.in_x592_tab_dram} 
  }
  def connectWires0(module: RootController_module)(implicit stack: List[KernelHash]): Unit = {
    module.io.in_x1024 <> x1024
    module.io.in_x582_ncons <> x582_ncons
    module.io.in_x602 <> x602
    module.io.in_x1022 <> x1022
    module.io.in_x580_nrow <> x580_nrow
    module.io.in_x1023 <> x1023
    module.io.in_x600 <> x600
    module.io.in_x581_ncol <> x581_ncol
    module.io.in_x596_tab_dram_out <> x596_tab_dram_out
    module.io.in_x592_tab_dram <> x592_tab_dram
  }
  val x596_tab_dram_out = list_x596_tab_dram_out(0)
  val x592_tab_dram = list_x596_tab_dram_out(1)
  val x1024 = list_x1024(0)
  val x582_ncons = list_x582_ncons(0)
  val x580_nrow = list_x582_ncons(1)
  val x581_ncol = list_x582_ncons(2)
  val x1022 = list_x1022(0)
  val x600 = list_x1022(1)
  val x1023 = list_x1023(0)
  val x602 = list_x602(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "RootController")
    implicit val stack = ControllerStack.stack.toList
    class RootController_concrete(depth: Int)(implicit stack: List[KernelHash]) extends RootController_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x597_tab_sram_0 = (new x597_tab_sram_0).m.io.asInstanceOf[StandardInterface]
      val x598_tab_sram_1 = (new x598_tab_sram_1).m.io.asInstanceOf[StandardInterface]
      val x668_iter_0 = (new x668_iter_0).m.io.asInstanceOf[StandardInterface]
      val x671_no_pivot_col_0 = (new x671_no_pivot_col_0).m.io.asInstanceOf[StandardInterface]
      val x672_no_pivot_row_0 = (new x672_no_pivot_row_0).m.io.asInstanceOf[StandardInterface]
      val x673_no_pivot_row_count_0 = (new x673_no_pivot_row_count_0).m.io.asInstanceOf[StandardInterface]
      val x674_pivot_element_0 = (new x674_pivot_element_0).m.io.asInstanceOf[StandardInterface]
      val x675_pivot_row_min_ratio_0 = (new x675_pivot_row_min_ratio_0).m.io.asInstanceOf[StandardInterface]
      val x1134 = new x1134_kernel(List(x592_tab_dram), List(x597_tab_sram_0,x598_tab_sram_1,x672_no_pivot_row_0,x668_iter_0,x671_no_pivot_col_0), List(x600), List(x580_nrow,x581_ncol), List(x602) , Some(me), List(), 0, 3, 1, List(1), List(32), breakpoints, rr)
      x1134.sm.io.ctrDone := risingEdge(x1134.sm.io.ctrInc)
      x1134.backpressure := true.B | x1134.sm.io.doneLatch
      x1134.forwardpressure := true.B | x1134.sm.io.doneLatch
      x1134.sm.io.enableOut.zip(x1134.smEnableOuts).foreach{case (l,r) => r := l}
      x1134.sm.io.break := false.B
      x1134.mask := true.B & true.B
      x1134.configure("x1134", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1134.kernel()
      val x679_pivot_col_0 = (new x679_pivot_col_0).m.io.asInstanceOf[StandardInterface]
      val x680_pivot_row_0 = (new x680_pivot_row_0).m.io.asInstanceOf[StandardInterface]
      val x1020_outr_FSM = new x1020_outr_FSM_kernel(List(x597_tab_sram_0,x598_tab_sram_1,x679_pivot_col_0,x674_pivot_element_0,x672_no_pivot_row_0,x668_iter_0,x675_pivot_row_min_ratio_0,x680_pivot_row_0,x673_no_pivot_row_count_0,x671_no_pivot_col_0), List(x582_ncons,x580_nrow,x581_ncol) , Some(me), List(), 1, 3, 1, List(1), List(32), breakpoints, rr)
      x1020_outr_FSM.sm.io.ctrDone := risingEdge(x1020_outr_FSM.sm.io.ctrInc)
      x1020_outr_FSM.backpressure := true.B | x1020_outr_FSM.sm.io.doneLatch
      x1020_outr_FSM.forwardpressure := true.B | x1020_outr_FSM.sm.io.doneLatch
      x1020_outr_FSM.sm.io.enableOut.zip(x1020_outr_FSM.smEnableOuts).foreach{case (l,r) => r := l}
      x1020_outr_FSM.sm.io.break := false.B
      x1020_outr_FSM.mask := true.B & true.B
      x1020_outr_FSM.configure("x1020_outr_FSM", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1020_outr_FSM.kernel()
      val x1081_outr_UnitPipe = new x1081_outr_UnitPipe_kernel(List(x596_tab_dram_out), List(x1024), List(x597_tab_sram_0), List(x1023), List(x1022), List(x580_nrow,x581_ncol) , Some(me), List(), 2, 1, 1, List(1), List(32), breakpoints, rr)
      x1081_outr_UnitPipe.backpressure := true.B | x1081_outr_UnitPipe.sm.io.doneLatch
      x1081_outr_UnitPipe.forwardpressure := true.B | x1081_outr_UnitPipe.sm.io.doneLatch
      x1081_outr_UnitPipe.sm.io.enableOut.zip(x1081_outr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x1081_outr_UnitPipe.sm.io.break := false.B
      x1081_outr_UnitPipe.mask := true.B & true.B
      x1081_outr_UnitPipe.configure("x1081_outr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1081_outr_UnitPipe.kernel()
    }
    val module = Module(new RootController_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END AccelScope RootController **/
