package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x754 -> x756 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x754_inr_Switch **/
class x754_inr_Switch_kernel(
  list_x1089_rd_x729: List[Bool],
  list_x729_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Fork, false   ,cases = 2, latency = 1.0.toInt, myName = "x754_inr_Switch_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x754_inr_Switch_iiCtr"))
  
  abstract class x754_inr_Switch_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x729_reg = Flipped(new StandardInterface(ModuleParams.getParams("x729_reg_p").asInstanceOf[MemParams] ))
      val in_x682_reg = Flipped(new StandardInterface(ModuleParams.getParams("x682_reg_p").asInstanceOf[MemParams] ))
      val in_x1089_rd_x729 = Input(Bool())
      val in_x1090_rd_x730 = Input(Bool())
      val in_x671_no_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x671_no_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x729_reg = {io.in_x729_reg} ; io.in_x729_reg := DontCare
    def x682_reg = {io.in_x682_reg} ; io.in_x682_reg := DontCare
    def x1089_rd_x729 = {io.in_x1089_rd_x729} 
    def x1090_rd_x730 = {io.in_x1090_rd_x730} 
    def x671_no_pivot_col_0 = {io.in_x671_no_pivot_col_0} ; io.in_x671_no_pivot_col_0 := DontCare
  }
  def connectWires0(module: x754_inr_Switch_module)(implicit stack: List[KernelHash]): Unit = {
    x729_reg.connectLedger(module.io.in_x729_reg)
    x682_reg.connectLedger(module.io.in_x682_reg)
    module.io.in_x1089_rd_x729 <> x1089_rd_x729
    module.io.in_x1090_rd_x730 <> x1090_rd_x730
    x671_no_pivot_col_0.connectLedger(module.io.in_x671_no_pivot_col_0)
  }
  val x1089_rd_x729 = list_x1089_rd_x729(0)
  val x1090_rd_x730 = list_x1089_rd_x729(1)
  val x729_reg = list_x729_reg(0)
  val x682_reg = list_x729_reg(1)
  val x671_no_pivot_col_0 = list_x729_reg(2)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x754_inr_Switch_obj")
    implicit val stack = ControllerStack.stack.toList
    class x754_inr_Switch_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x754_inr_Switch_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x752_inr_SwitchCase_obj = new x752_inr_SwitchCase_kernel(List(x729_reg,x682_reg,x671_no_pivot_col_0) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x752_inr_SwitchCase_obj.baseEn := io.sigsIn.smSelectsOut(x752_inr_SwitchCase_obj.childId)
      x752_inr_SwitchCase_obj.sm.io.ctrDone := risingEdge(x752_inr_SwitchCase_obj.sm.io.ctrInc)
      x752_inr_SwitchCase_obj.backpressure := true.B | x752_inr_SwitchCase_obj.sm.io.doneLatch
      x752_inr_SwitchCase_obj.forwardpressure := true.B | x752_inr_SwitchCase_obj.sm.io.doneLatch
      x752_inr_SwitchCase_obj.sm.io.enableOut.zip(x752_inr_SwitchCase_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x752_inr_SwitchCase_obj.sm.io.break := false.B
      x752_inr_SwitchCase_obj.mask := true.B & true.B
      x752_inr_SwitchCase_obj.configure("x752_inr_SwitchCase_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = true)
      x752_inr_SwitchCase_obj.kernel()
      val x753_inr_SwitchCase_obj = new x753_inr_SwitchCase_kernel(  Some(me), List(), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x753_inr_SwitchCase_obj.baseEn := io.sigsIn.smSelectsOut(x753_inr_SwitchCase_obj.childId)
      x753_inr_SwitchCase_obj.sm.io.ctrDone := risingEdge(x753_inr_SwitchCase_obj.sm.io.ctrInc)
      x753_inr_SwitchCase_obj.backpressure := true.B | x753_inr_SwitchCase_obj.sm.io.doneLatch
      x753_inr_SwitchCase_obj.forwardpressure := true.B | x753_inr_SwitchCase_obj.sm.io.doneLatch
      x753_inr_SwitchCase_obj.sm.io.enableOut.zip(x753_inr_SwitchCase_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x753_inr_SwitchCase_obj.sm.io.break := false.B
      x753_inr_SwitchCase_obj.mask := true.B & true.B
      x753_inr_SwitchCase_obj.configure("x753_inr_SwitchCase_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = true)
      x753_inr_SwitchCase_obj.kernel()
    }
    val module = Module(new x754_inr_Switch_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END Switch x754_inr_Switch **/
