package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x871 -> x897 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x871_inr_UnitPipe **/
class x871_inr_UnitPipe_kernel(
  list_x597_tab_sram_0: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 9.0.toInt, myName = "x871_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x871_inr_UnitPipe_iiCtr"))
  
  abstract class x871_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x845_reg = Flipped(new StandardInterface(ModuleParams.getParams("x845_reg_p").asInstanceOf[MemParams] ))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x845_reg = {io.in_x845_reg} ; io.in_x845_reg := DontCare
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x871_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    x845_reg.connectLedger(module.io.in_x845_reg)
    x759_reg.connectLedger(module.io.in_x759_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x679_pivot_col_0 = list_x597_tab_sram_0(1)
  val x674_pivot_element_0 = list_x597_tab_sram_0(2)
  val x680_pivot_row_0 = list_x597_tab_sram_0(3)
  val x845_reg = list_x597_tab_sram_0(4)
  val x759_reg = list_x597_tab_sram_0(5)
  val x684_reg = list_x597_tab_sram_0(6)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x871_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x871_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x871_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x858_rd_x679 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x858_rd_x679""")
      val x858_rd_x679_banks = List[UInt]()
      val x858_rd_x679_ofs = List[UInt]()
      val x858_rd_x679_en = List[Bool](true.B)
      x858_rd_x679.toSeq.zip(x679_pivot_col_0.connectRPort(858, x858_rd_x679_banks, x858_rd_x679_ofs, io.sigsIn.backpressure, x858_rd_x679_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x859_rd_x680 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x859_rd_x680""")
      val x859_rd_x680_banks = List[UInt]()
      val x859_rd_x680_ofs = List[UInt]()
      val x859_rd_x680_en = List[Bool](true.B)
      x859_rd_x680.toSeq.zip(x680_pivot_row_0.connectRPort(859, x859_rd_x680_banks, x859_rd_x680_ofs, io.sigsIn.backpressure, x859_rd_x680_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x860_rd_x845 = Wire(Bool()).suggestName("""x860_rd_x845""")
      val x860_rd_x845_banks = List[UInt]()
      val x860_rd_x845_ofs = List[UInt]()
      val x860_rd_x845_en = List[Bool](true.B)
      x860_rd_x845.toSeq.zip(x845_reg.connectRPort(860, x860_rd_x845_banks, x860_rd_x845_ofs, io.sigsIn.backpressure, x860_rd_x845_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x861_rd_x759 = Wire(Bool()).suggestName("""x861_rd_x759""")
      val x861_rd_x759_banks = List[UInt]()
      val x861_rd_x759_ofs = List[UInt]()
      val x861_rd_x759_en = List[Bool](true.B)
      x861_rd_x759.toSeq.zip(x759_reg.connectRPort(861, x861_rd_x759_banks, x861_rd_x759_ofs, io.sigsIn.backpressure, x861_rd_x759_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x862_rd_x684 = Wire(Bool()).suggestName("""x862_rd_x684""")
      val x862_rd_x684_banks = List[UInt]()
      val x862_rd_x684_ofs = List[UInt]()
      val x862_rd_x684_en = List[Bool](true.B)
      x862_rd_x684.toSeq.zip(x684_reg.connectRPort(862, x862_rd_x684_banks, x862_rd_x684_ofs, io.sigsIn.backpressure, x862_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1125 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1125""")
      x1125.r := Math.fma(x859_rd_x680,10L.FP(true, 32, 0),x858_rd_x679,Some(6.0), true.B, "x1125").toFixed(x1125, "cast_x1125").r
      val x1173 = Wire(Bool()).suggestName("x1173_x861_rd_x759_D6") 
      x1173.r := getRetimed(x861_rd_x759.r, 6.toInt, io.sigsIn.backpressure)
      val x1174 = Wire(Bool()).suggestName("x1174_x862_rd_x684_D6") 
      x1174.r := getRetimed(x862_rd_x684.r, 6.toInt, io.sigsIn.backpressure)
      val x1175 = Wire(Bool()).suggestName("x1175_x860_rd_x845_D6") 
      x1175.r := getRetimed(x860_rd_x845.r, 6.toInt, io.sigsIn.backpressure)
      val x868_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x868_rd""")
      val x868_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x868_rd_ofs = List[UInt](x1125.r)
      val x868_rd_en = List[Bool](true.B)
      x868_rd.toSeq.zip(x597_tab_sram_0.connectRPort(868, x868_rd_banks, x868_rd_ofs, io.sigsIn.backpressure, x868_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(6.0.toInt, rr, io.sigsIn.backpressure) && x1174 & x1173 & x1175), true)).foreach{case (a,b) => a := b}
      val x869_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x869_elem_0""")
      x869_elem_0.r := x868_rd(0).r
      val x1176 = Wire(Bool()).suggestName("x1176_x861_rd_x759_D8") 
      x1176.r := getRetimed(x861_rd_x759.r, 8.toInt, io.sigsIn.backpressure)
      val x1177 = Wire(Bool()).suggestName("x1177_x862_rd_x684_D8") 
      x1177.r := getRetimed(x862_rd_x684.r, 8.toInt, io.sigsIn.backpressure)
      val x1178 = Wire(Bool()).suggestName("x1178_x860_rd_x845_D8") 
      x1178.r := getRetimed(x860_rd_x845.r, 8.toInt, io.sigsIn.backpressure)
      val x870_wr_x674_banks = List[UInt]()
      val x870_wr_x674_ofs = List[UInt]()
      val x870_wr_x674_en = List[Bool](true.B)
      val x870_wr_x674_data = List[UInt](x869_elem_0.r)
      x674_pivot_element_0.connectWPort(870, x870_wr_x674_banks, x870_wr_x674_ofs, x870_wr_x674_data, x870_wr_x674_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(8.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1177 & x1176 & x1178))
    }
    val module = Module(new x871_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x871_inr_UnitPipe **/
