package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1080 -> x1081 -> x1137 **/
/** BEGIN None x1080_outr_Foreach **/
class x1080_outr_Foreach_kernel(
  list_x596_tab_dram_out: List[FixedPoint],
  list_x1024: List[DecoupledIO[Bool]],
  list_x597_tab_sram_0: List[StandardInterface],
  list_x581_ncol: List[UInt],
  list_x1023: List[DecoupledIO[AppStoreData]],
  list_x1022: List[DecoupledIO[AppCommandDense]],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 2, isFSM = false   , latency = 0.0.toInt, myName = "x1080_outr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1080_outr_Foreach_iiCtr"))
  
  abstract class x1080_outr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x1024 = Flipped(Decoupled(Bool()))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x1022 = Decoupled(new AppCommandDense(ModuleParams.getParams("x1022_p").asInstanceOf[(Int,Int)] ))
      val in_x1023 = Decoupled(new AppStoreData(ModuleParams.getParams("x1023_p").asInstanceOf[(Int,Int)] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x596_tab_dram_out = Input(new FixedPoint(true, 64, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x1024 = {io.in_x1024} 
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x1022 = {io.in_x1022} 
    def x1023 = {io.in_x1023} 
    def x581_ncol = {io.in_x581_ncol} 
    def x596_tab_dram_out = {io.in_x596_tab_dram_out} 
  }
  def connectWires0(module: x1080_outr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    module.io.in_x1024 <> x1024
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    module.io.in_x1022 <> x1022
    module.io.in_x1023 <> x1023
    module.io.in_x581_ncol <> x581_ncol
    module.io.in_x596_tab_dram_out <> x596_tab_dram_out
  }
  val x596_tab_dram_out = list_x596_tab_dram_out(0)
  val x1024 = list_x1024(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x581_ncol = list_x581_ncol(0)
  val x1023 = list_x1023(0)
  val x1022 = list_x1022(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1080_outr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x1080_outr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1080_outr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b1027 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b1027.suggestName("b1027")
      val b1028 = ~io.sigsIn.cchainOutputs.head.oobs(0); b1028.suggestName("b1028")
      val x1075_outr_UnitPipe = new x1075_outr_UnitPipe_kernel(List(x597_tab_sram_0), List(x581_ncol), List(b1027,x596_tab_dram_out), List(x1023), List(x1022), List(b1028) , Some(me), List(), 0, 2, 1, List(1), List(32), breakpoints, rr)
      x1075_outr_UnitPipe.sm.io.ctrDone := risingEdge(x1075_outr_UnitPipe.sm.io.ctrInc)
      x1075_outr_UnitPipe.backpressure := true.B | x1075_outr_UnitPipe.sm.io.doneLatch
      x1075_outr_UnitPipe.forwardpressure := true.B | x1075_outr_UnitPipe.sm.io.doneLatch
      x1075_outr_UnitPipe.sm.io.enableOut.zip(x1075_outr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x1075_outr_UnitPipe.sm.io.break := false.B
      x1075_outr_UnitPipe.mask := true.B & b1028
      x1075_outr_UnitPipe.configure("x1075_outr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1075_outr_UnitPipe.kernel()
      val x1079_inr_UnitPipe = new x1079_inr_UnitPipe_kernel(List(b1028), List(x1024) , Some(me), List(), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x1079_inr_UnitPipe.sm.io.ctrDone := risingEdge(x1079_inr_UnitPipe.sm.io.ctrInc)
      x1079_inr_UnitPipe.backpressure := true.B | x1079_inr_UnitPipe.sm.io.doneLatch
      x1079_inr_UnitPipe.forwardpressure := x1024.valid | x1079_inr_UnitPipe.sm.io.doneLatch
      x1079_inr_UnitPipe.sm.io.enableOut.zip(x1079_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x1079_inr_UnitPipe.sm.io.break := false.B
      x1079_inr_UnitPipe.mask := true.B & b1028
      x1079_inr_UnitPipe.configure("x1079_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1079_inr_UnitPipe.kernel()
    }
    val module = Module(new x1080_outr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x1080_outr_Foreach **/
