package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x666 -> x667 -> x1134 -> x1137 **/
/** BEGIN None x666_outr_Foreach **/
class x666_outr_Foreach_kernel(
  list_x601_fifo: List[FIFOInterface],
  list_x602: List[DecoupledIO[AppLoadData]],
  list_x597_tab_sram_0: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 2, isFSM = false   , latency = 0.0.toInt, myName = "x666_outr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x666_outr_Foreach_iiCtr"))
  
  abstract class x666_outr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x602 = Flipped(Decoupled(new AppLoadData(ModuleParams.getParams("x602_p").asInstanceOf[(Int, Int)] )))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x601_fifo = Flipped(new FIFOInterface(ModuleParams.getParams("x601_fifo_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x602 = {io.in_x602} 
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x601_fifo = {io.in_x601_fifo} ; io.in_x601_fifo := DontCare
  }
  def connectWires0(module: x666_outr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    module.io.in_x602 <> x602
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x601_fifo.connectLedger(module.io.in_x601_fifo)
  }
  val x601_fifo = list_x601_fifo(0)
  val x602 = list_x602(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x598_tab_sram_1 = list_x597_tab_sram_0(1)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x666_outr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x666_outr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x666_outr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b631 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b631.suggestName("b631")
      val b632 = ~io.sigsIn.cchainOutputs.head.oobs(0); b632.suggestName("b632")
      val x633_reg = (new x633_reg).m.io.asInstanceOf[StandardInterface]
      val x634_reg = (new x634_reg).m.io.asInstanceOf[StandardInterface]
      val x635_reg = (new x635_reg).m.io.asInstanceOf[StandardInterface]
      val x644_inr_UnitPipe = new x644_inr_UnitPipe_kernel(List(b632), List(x601_fifo), List(x634_reg,x633_reg,x635_reg) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x644_inr_UnitPipe.sm.io.ctrDone := risingEdge(x644_inr_UnitPipe.sm.io.ctrInc)
      x644_inr_UnitPipe.backpressure := true.B | x644_inr_UnitPipe.sm.io.doneLatch
      x644_inr_UnitPipe.forwardpressure := (~x601_fifo.empty | ~(x601_fifo.accessActivesOut(1))) | x644_inr_UnitPipe.sm.io.doneLatch
      x644_inr_UnitPipe.sm.io.enableOut.zip(x644_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x644_inr_UnitPipe.sm.io.break := false.B
      x644_inr_UnitPipe.mask := true.B & b632
      x644_inr_UnitPipe.configure("x644_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x644_inr_UnitPipe.kernel()
      val x1084_rd_x635 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1084_rd_x635""")
      val x1084_rd_x635_banks = List[UInt]()
      val x1084_rd_x635_ofs = List[UInt]()
      val x1084_rd_x635_en = List[Bool](true.B)
      x1084_rd_x635.toSeq.zip(x635_reg.connectRPort(1084, x1084_rd_x635_banks, x1084_rd_x635_ofs, io.sigsIn.backpressure, x1084_rd_x635_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x646_ctr = new CtrObject(Left(Some(0)), Right(x1084_rd_x635), Left(Some(1)), 1, 32, false)
      val x647_ctrchain = (new CChainObject(List[CtrObject](x646_ctr), "x647_ctrchain")).cchain.io 
      x647_ctrchain.setup.isStream := false.B
      ModuleParams.addParams("x647_ctrchain_p", (x647_ctrchain.par, x647_ctrchain.widths))
      val x665_inr_Foreach = new x665_inr_Foreach_kernel(List(b632), List(b631), List(x602), List(x597_tab_sram_0,x634_reg,x598_tab_sram_1,x633_reg) , Some(me), List(x647_ctrchain), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x665_inr_Foreach.sm.io.ctrDone := (x665_inr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x665_inr_Foreach.backpressure := true.B | x665_inr_Foreach.sm.io.doneLatch
      x665_inr_Foreach.forwardpressure := x602.valid | x665_inr_Foreach.sm.io.doneLatch
      x665_inr_Foreach.sm.io.enableOut.zip(x665_inr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x665_inr_Foreach.sm.io.break := false.B
      x665_inr_Foreach.mask := ~x665_inr_Foreach.cchain.head.output.noop & b632
      x665_inr_Foreach.configure("x665_inr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x665_inr_Foreach.kernel()
    }
    val module = Module(new x666_outr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x666_outr_Foreach **/
