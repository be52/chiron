package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x993 -> x1135 -> x1136 -> x997 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x993_inr_UnitPipe **/
class x993_inr_UnitPipe_kernel(
  list_x898_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 2.0.toInt, myName = "x993_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(2.0.toInt, 2 + _root_.utils.math.log2Up(2.0.toInt), "x993_inr_UnitPipe_iiCtr"))
  
  abstract class x993_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x898_reg = Flipped(new StandardInterface(ModuleParams.getParams("x898_reg_p").asInstanceOf[MemParams] ))
      val in_x847_reg = Flipped(new StandardInterface(ModuleParams.getParams("x847_reg_p").asInstanceOf[MemParams] ))
      val in_x668_iter_0 = Flipped(new StandardInterface(ModuleParams.getParams("x668_iter_0_p").asInstanceOf[MemParams] ))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x898_reg = {io.in_x898_reg} ; io.in_x898_reg := DontCare
    def x847_reg = {io.in_x847_reg} ; io.in_x847_reg := DontCare
    def x668_iter_0 = {io.in_x668_iter_0} ; io.in_x668_iter_0 := DontCare
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x993_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x898_reg.connectLedger(module.io.in_x898_reg)
    x847_reg.connectLedger(module.io.in_x847_reg)
    x668_iter_0.connectLedger(module.io.in_x668_iter_0)
    x759_reg.connectLedger(module.io.in_x759_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x898_reg = list_x898_reg(0)
  val x847_reg = list_x898_reg(1)
  val x668_iter_0 = list_x898_reg(2)
  val x759_reg = list_x898_reg(3)
  val x684_reg = list_x898_reg(4)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x993_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x993_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x993_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x986_rd_x668 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x986_rd_x668""")
      val x986_rd_x668_banks = List[UInt]()
      val x986_rd_x668_ofs = List[UInt]()
      val x986_rd_x668_en = List[Bool](true.B)
      x986_rd_x668.toSeq.zip(x668_iter_0.connectRPort(986, x986_rd_x668_banks, x986_rd_x668_ofs, io.sigsIn.backpressure, x986_rd_x668_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x987_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x987_sum""")
      x987_sum.r := Math.add(x986_rd_x668,1L.FP(true, 32, 0),Some(1.0), true.B, Truncate, Wrapping, "x987_sum").r
      val x988_rd_x898 = Wire(Bool()).suggestName("""x988_rd_x898""")
      val x988_rd_x898_banks = List[UInt]()
      val x988_rd_x898_ofs = List[UInt]()
      val x988_rd_x898_en = List[Bool](true.B)
      x988_rd_x898.toSeq.zip(x898_reg.connectRPort(988, x988_rd_x898_banks, x988_rd_x898_ofs, io.sigsIn.backpressure, x988_rd_x898_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x989_rd_x847 = Wire(Bool()).suggestName("""x989_rd_x847""")
      val x989_rd_x847_banks = List[UInt]()
      val x989_rd_x847_ofs = List[UInt]()
      val x989_rd_x847_en = List[Bool](true.B)
      x989_rd_x847.toSeq.zip(x847_reg.connectRPort(989, x989_rd_x847_banks, x989_rd_x847_ofs, io.sigsIn.backpressure, x989_rd_x847_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x990_rd_x759 = Wire(Bool()).suggestName("""x990_rd_x759""")
      val x990_rd_x759_banks = List[UInt]()
      val x990_rd_x759_ofs = List[UInt]()
      val x990_rd_x759_en = List[Bool](true.B)
      x990_rd_x759.toSeq.zip(x759_reg.connectRPort(990, x990_rd_x759_banks, x990_rd_x759_ofs, io.sigsIn.backpressure, x990_rd_x759_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x991_rd_x684 = Wire(Bool()).suggestName("""x991_rd_x684""")
      val x991_rd_x684_banks = List[UInt]()
      val x991_rd_x684_ofs = List[UInt]()
      val x991_rd_x684_en = List[Bool](true.B)
      x991_rd_x684.toSeq.zip(x684_reg.connectRPort(991, x991_rd_x684_banks, x991_rd_x684_ofs, io.sigsIn.backpressure, x991_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1220 = Wire(Bool()).suggestName("x1220_x990_rd_x759_D1") 
      x1220.r := getRetimed(x990_rd_x759.r, 1.toInt, io.sigsIn.backpressure)
      val x1221 = Wire(Bool()).suggestName("x1221_x988_rd_x898_D1") 
      x1221.r := getRetimed(x988_rd_x898.r, 1.toInt, io.sigsIn.backpressure)
      val x1222 = Wire(Bool()).suggestName("x1222_x991_rd_x684_D1") 
      x1222.r := getRetimed(x991_rd_x684.r, 1.toInt, io.sigsIn.backpressure)
      val x1223 = Wire(Bool()).suggestName("x1223_x989_rd_x847_D1") 
      x1223.r := getRetimed(x989_rd_x847.r, 1.toInt, io.sigsIn.backpressure)
      val x992_wr_x668_banks = List[UInt]()
      val x992_wr_x668_ofs = List[UInt]()
      val x992_wr_x668_en = List[Bool](true.B)
      val x992_wr_x668_data = List[UInt](x987_sum.r)
      x668_iter_0.connectWPort(992, x992_wr_x668_banks, x992_wr_x668_ofs, x992_wr_x668_data, x992_wr_x668_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(1.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1222 & x1220 & x1223 & x1221))
    }
    val module = Module(new x993_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x993_inr_UnitPipe **/
