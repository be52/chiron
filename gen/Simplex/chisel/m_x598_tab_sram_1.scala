package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

class x598_tab_sram_1 {
  val w0 = Access(664, 2, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val w1 = Access(894, 0, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val w2 = Access(979, 1, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r0 = Access(719, 0, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r1 = Access(791, 1, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r2 = Access(965, 2, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val m = Module(new BankedSRAM( 
    List[Int](10,10),
     32, 
    List[Int](1),
    List[Int](1),
    List[Int](1,1),
    List(w0,w1,w2),
    List(r0,r1,r2),
    BankedMemory, 
    None, 
    true, 
    0,
    6, 
    myName = "x598_tab_sram_1"
  ))
  m.io.asInstanceOf[StandardInterface] <> DontCare
  m.io.reset := false.B
  ModuleParams.addParams("x598_tab_sram_1_p", m.io.p)
}
