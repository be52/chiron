package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x692 -> x1020 -> x1137 **/
/** BEGIN None x692_inr_UnitPipe **/
class x692_inr_UnitPipe_kernel(
  list_b47: List[FixedPoint],
  list_x683_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.4.toInt, myName = "x692_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x692_inr_UnitPipe_iiCtr"))
  
  abstract class x692_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x683_reg = Flipped(new StandardInterface(ModuleParams.getParams("x683_reg_p").asInstanceOf[MemParams] ))
      val in_x682_reg = Flipped(new StandardInterface(ModuleParams.getParams("x682_reg_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_b47 = Input(new FixedPoint(true, 32, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x683_reg = {io.in_x683_reg} ; io.in_x683_reg := DontCare
    def x682_reg = {io.in_x682_reg} ; io.in_x682_reg := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
    def b47 = {io.in_b47} 
  }
  def connectWires0(module: x692_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x683_reg.connectLedger(module.io.in_x683_reg)
    x682_reg.connectLedger(module.io.in_x682_reg)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
    module.io.in_b47 <> b47
  }
  val b47 = list_b47(0)
  val x683_reg = list_x683_reg(0)
  val x682_reg = list_x683_reg(1)
  val x685_reg = list_x683_reg(2)
  val x684_reg = list_x683_reg(3)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x692_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x692_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x692_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x686 = Wire(Bool()).suggestName("""x686""")
      x686.r := Math.eql(b47, 0L.FP(true, 32, 0), Some(0.2), true.B,"x686").r
      val x687 = Wire(Bool()).suggestName("""x687""")
      x687 := ~x686
      val x688_wr_x683_banks = List[UInt]()
      val x688_wr_x683_ofs = List[UInt]()
      val x688_wr_x683_en = List[Bool](true.B)
      val x688_wr_x683_data = List[UInt](x686.r)
      x683_reg.connectWPort(688, x688_wr_x683_banks, x688_wr_x683_ofs, x688_wr_x683_data, x688_wr_x683_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x689_wr_x682_banks = List[UInt]()
      val x689_wr_x682_ofs = List[UInt]()
      val x689_wr_x682_en = List[Bool](true.B)
      val x689_wr_x682_data = List[UInt](x686.r)
      x682_reg.connectWPort(689, x689_wr_x682_banks, x689_wr_x682_ofs, x689_wr_x682_data, x689_wr_x682_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x690_wr_x684_banks = List[UInt]()
      val x690_wr_x684_ofs = List[UInt]()
      val x690_wr_x684_en = List[Bool](true.B)
      val x690_wr_x684_data = List[UInt](x687.r)
      x684_reg.connectWPort(690, x690_wr_x684_banks, x690_wr_x684_ofs, x690_wr_x684_data, x690_wr_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x691_wr_x685_banks = List[UInt]()
      val x691_wr_x685_ofs = List[UInt]()
      val x691_wr_x685_en = List[Bool](true.B)
      val x691_wr_x685_data = List[UInt](x687.r)
      x685_reg.connectWPort(691, x691_wr_x685_banks, x691_wr_x685_ofs, x691_wr_x685_data, x691_wr_x685_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x692_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x692_inr_UnitPipe **/
