package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x774 -> x844 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x774_inr_UnitPipe **/
class x774_inr_UnitPipe_kernel(
  list_x757_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.0.toInt, myName = "x774_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x774_inr_UnitPipe_iiCtr"))
  
  abstract class x774_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x757_reg = Flipped(new StandardInterface(ModuleParams.getParams("x757_reg_p").asInstanceOf[MemParams] ))
      val in_x675_pivot_row_min_ratio_0 = Flipped(new StandardInterface(ModuleParams.getParams("x675_pivot_row_min_ratio_0_p").asInstanceOf[MemParams] ))
      val in_x673_no_pivot_row_count_0 = Flipped(new StandardInterface(ModuleParams.getParams("x673_no_pivot_row_count_0_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x757_reg = {io.in_x757_reg} ; io.in_x757_reg := DontCare
    def x675_pivot_row_min_ratio_0 = {io.in_x675_pivot_row_min_ratio_0} ; io.in_x675_pivot_row_min_ratio_0 := DontCare
    def x673_no_pivot_row_count_0 = {io.in_x673_no_pivot_row_count_0} ; io.in_x673_no_pivot_row_count_0 := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x774_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x757_reg.connectLedger(module.io.in_x757_reg)
    x675_pivot_row_min_ratio_0.connectLedger(module.io.in_x675_pivot_row_min_ratio_0)
    x673_no_pivot_row_count_0.connectLedger(module.io.in_x673_no_pivot_row_count_0)
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x757_reg = list_x757_reg(0)
  val x675_pivot_row_min_ratio_0 = list_x757_reg(1)
  val x673_no_pivot_row_count_0 = list_x757_reg(2)
  val x684_reg = list_x757_reg(3)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x774_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x774_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x774_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x770_rd_x757 = Wire(Bool()).suggestName("""x770_rd_x757""")
      val x770_rd_x757_banks = List[UInt]()
      val x770_rd_x757_ofs = List[UInt]()
      val x770_rd_x757_en = List[Bool](true.B)
      x770_rd_x757.toSeq.zip(x757_reg.connectRPort(770, x770_rd_x757_banks, x770_rd_x757_ofs, io.sigsIn.backpressure, x770_rd_x757_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x771_rd_x684 = Wire(Bool()).suggestName("""x771_rd_x684""")
      val x771_rd_x684_banks = List[UInt]()
      val x771_rd_x684_ofs = List[UInt]()
      val x771_rd_x684_en = List[Bool](true.B)
      x771_rd_x684.toSeq.zip(x684_reg.connectRPort(771, x771_rd_x684_banks, x771_rd_x684_ofs, io.sigsIn.backpressure, x771_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x772_wr_x673_banks = List[UInt]()
      val x772_wr_x673_ofs = List[UInt]()
      val x772_wr_x673_en = List[Bool](true.B)
      val x772_wr_x673_data = List[UInt](0L.FP(true, 32, 0).r)
      x673_no_pivot_row_count_0.connectWPort(772, x772_wr_x673_banks, x772_wr_x673_ofs, x772_wr_x673_data, x772_wr_x673_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x771_rd_x684 & x770_rd_x757))
      val x773_wr_x675_banks = List[UInt]()
      val x773_wr_x675_ofs = List[UInt]()
      val x773_wr_x675_en = List[Bool](true.B)
      val x773_wr_x675_data = List[UInt](99999.0000000000000000.FlP(24, 8).r)
      x675_pivot_row_min_ratio_0.connectWPort(773, x773_wr_x675_banks, x773_wr_x675_ofs, x773_wr_x675_data, x773_wr_x675_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x771_rd_x684 & x770_rd_x757))
    }
    val module = Module(new x774_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x774_inr_UnitPipe **/
