package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x927 -> x985 -> x1135 -> x1136 -> x997 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x927_inr_UnitPipe **/
class x927_inr_UnitPipe_kernel(
  list_b917: List[Bool],
  list_b916: List[FixedPoint],
  list_x680_pivot_row_0: List[StandardInterface],
  list_x920_reg: List[NBufInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.4.toInt, myName = "x927_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x927_inr_UnitPipe_iiCtr"))
  
  abstract class x927_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_b917 = Input(Bool())
      val in_x920_reg = Flipped(new NBufInterface(ModuleParams.getParams("x920_reg_p").asInstanceOf[NBufParams] ))
      val in_b916 = Input(new FixedPoint(true, 32, 0))
      val in_x919_reg = Flipped(new NBufInterface(ModuleParams.getParams("x919_reg_p").asInstanceOf[NBufParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x918_reg = Flipped(new NBufInterface(ModuleParams.getParams("x918_reg_p").asInstanceOf[NBufParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def b917 = {io.in_b917} 
    def x920_reg = {io.in_x920_reg} ; io.in_x920_reg := DontCare
    def b916 = {io.in_b916} 
    def x919_reg = {io.in_x919_reg} ; io.in_x919_reg := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x918_reg = {io.in_x918_reg} ; io.in_x918_reg := DontCare
  }
  def connectWires0(module: x927_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    module.io.in_b917 <> b917
    x920_reg.connectLedger(module.io.in_x920_reg)
    module.io.in_b916 <> b916
    x919_reg.connectLedger(module.io.in_x919_reg)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    x918_reg.connectLedger(module.io.in_x918_reg)
  }
  val b917 = list_b917(0)
  val b916 = list_b916(0)
  val x680_pivot_row_0 = list_x680_pivot_row_0(0)
  val x920_reg = list_x920_reg(0)
  val x919_reg = list_x920_reg(1)
  val x918_reg = list_x920_reg(2)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x927_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x927_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x927_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x921_rd_x680 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x921_rd_x680""")
      val x921_rd_x680_banks = List[UInt]()
      val x921_rd_x680_ofs = List[UInt]()
      val x921_rd_x680_en = List[Bool](true.B)
      x921_rd_x680.toSeq.zip(x680_pivot_row_0.connectRPort(921, x921_rd_x680_banks, x921_rd_x680_ofs, io.sigsIn.backpressure, x921_rd_x680_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x922 = Wire(Bool()).suggestName("""x922""")
      x922.r := Math.neq(b916, x921_rd_x680, Some(0.2), true.B,"x922").r
      val x923 = Wire(Bool()).suggestName("""x923""")
      x923 := ~x922
      val x924_wr_x919_banks = List[UInt]()
      val x924_wr_x919_ofs = List[UInt]()
      val x924_wr_x919_en = List[Bool](true.B)
      val x924_wr_x919_data = List[UInt](x922.r)
      x919_reg.connectWPort(924, x924_wr_x919_banks, x924_wr_x919_ofs, x924_wr_x919_data, x924_wr_x919_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x925_wr_x918_banks = List[UInt]()
      val x925_wr_x918_ofs = List[UInt]()
      val x925_wr_x918_en = List[Bool](true.B)
      val x925_wr_x918_data = List[UInt](x922.r)
      x918_reg.connectWPort(925, x925_wr_x918_banks, x925_wr_x918_ofs, x925_wr_x918_data, x925_wr_x918_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x926_wr_x920_banks = List[UInt]()
      val x926_wr_x920_ofs = List[UInt]()
      val x926_wr_x920_en = List[Bool](true.B)
      val x926_wr_x920_data = List[UInt](x923.r)
      x920_reg.connectWPort(926, x926_wr_x920_banks, x926_wr_x920_ofs, x926_wr_x920_data, x926_wr_x920_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      x918_reg.connectStageCtrl((io.sigsIn.done).DS(1.toInt, rr, io.sigsIn.backpressure), io.sigsIn.baseEn, 0)
      x919_reg.connectStageCtrl((io.sigsIn.done).DS(1.toInt, rr, io.sigsIn.backpressure), io.sigsIn.baseEn, 0)
      x920_reg.connectStageCtrl((io.sigsIn.done).DS(1.toInt, rr, io.sigsIn.backpressure), io.sigsIn.baseEn, 0)
    }
    val module = Module(new x927_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x927_inr_UnitPipe **/
