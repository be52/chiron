package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x767 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x767_inr_UnitPipe **/
class x767_inr_UnitPipe_kernel(
  list_b47: List[FixedPoint],
  list_x760_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.4.toInt, myName = "x767_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x767_inr_UnitPipe_iiCtr"))
  
  abstract class x767_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x760_reg = Flipped(new StandardInterface(ModuleParams.getParams("x760_reg_p").asInstanceOf[MemParams] ))
      val in_x757_reg = Flipped(new StandardInterface(ModuleParams.getParams("x757_reg_p").asInstanceOf[MemParams] ))
      val in_x758_reg = Flipped(new StandardInterface(ModuleParams.getParams("x758_reg_p").asInstanceOf[MemParams] ))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_b47 = Input(new FixedPoint(true, 32, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x760_reg = {io.in_x760_reg} ; io.in_x760_reg := DontCare
    def x757_reg = {io.in_x757_reg} ; io.in_x757_reg := DontCare
    def x758_reg = {io.in_x758_reg} ; io.in_x758_reg := DontCare
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def b47 = {io.in_b47} 
  }
  def connectWires0(module: x767_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x760_reg.connectLedger(module.io.in_x760_reg)
    x757_reg.connectLedger(module.io.in_x757_reg)
    x758_reg.connectLedger(module.io.in_x758_reg)
    x759_reg.connectLedger(module.io.in_x759_reg)
    module.io.in_b47 <> b47
  }
  val b47 = list_b47(0)
  val x760_reg = list_x760_reg(0)
  val x757_reg = list_x760_reg(1)
  val x758_reg = list_x760_reg(2)
  val x759_reg = list_x760_reg(3)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x767_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x767_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x767_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x761 = Wire(Bool()).suggestName("""x761""")
      x761.r := Math.eql(b47, 1L.FP(true, 32, 0), Some(0.2), true.B,"x761").r
      val x762 = Wire(Bool()).suggestName("""x762""")
      x762 := ~x761
      val x763_wr_x758_banks = List[UInt]()
      val x763_wr_x758_ofs = List[UInt]()
      val x763_wr_x758_en = List[Bool](true.B)
      val x763_wr_x758_data = List[UInt](x761.r)
      x758_reg.connectWPort(763, x763_wr_x758_banks, x763_wr_x758_ofs, x763_wr_x758_data, x763_wr_x758_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x764_wr_x757_banks = List[UInt]()
      val x764_wr_x757_ofs = List[UInt]()
      val x764_wr_x757_en = List[Bool](true.B)
      val x764_wr_x757_data = List[UInt](x761.r)
      x757_reg.connectWPort(764, x764_wr_x757_banks, x764_wr_x757_ofs, x764_wr_x757_data, x764_wr_x757_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x765_wr_x759_banks = List[UInt]()
      val x765_wr_x759_ofs = List[UInt]()
      val x765_wr_x759_en = List[Bool](true.B)
      val x765_wr_x759_data = List[UInt](x762.r)
      x759_reg.connectWPort(765, x765_wr_x759_banks, x765_wr_x759_ofs, x765_wr_x759_data, x765_wr_x759_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x766_wr_x760_banks = List[UInt]()
      val x766_wr_x760_ofs = List[UInt]()
      val x766_wr_x760_en = List[Bool](true.B)
      val x766_wr_x760_data = List[UInt](x762.r)
      x760_reg.connectWPort(766, x766_wr_x760_banks, x766_wr_x760_ofs, x766_wr_x760_data, x766_wr_x760_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x767_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x767_inr_UnitPipe **/
