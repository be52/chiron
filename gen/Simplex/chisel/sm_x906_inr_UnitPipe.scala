package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x906 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x906_inr_UnitPipe **/
class x906_inr_UnitPipe_kernel(
  list_b47: List[FixedPoint],
  list_x898_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.4.toInt, myName = "x906_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x906_inr_UnitPipe_iiCtr"))
  
  abstract class x906_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x898_reg = Flipped(new StandardInterface(ModuleParams.getParams("x898_reg_p").asInstanceOf[MemParams] ))
      val in_x900_reg = Flipped(new StandardInterface(ModuleParams.getParams("x900_reg_p").asInstanceOf[MemParams] ))
      val in_x899_reg = Flipped(new StandardInterface(ModuleParams.getParams("x899_reg_p").asInstanceOf[MemParams] ))
      val in_b47 = Input(new FixedPoint(true, 32, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x898_reg = {io.in_x898_reg} ; io.in_x898_reg := DontCare
    def x900_reg = {io.in_x900_reg} ; io.in_x900_reg := DontCare
    def x899_reg = {io.in_x899_reg} ; io.in_x899_reg := DontCare
    def b47 = {io.in_b47} 
  }
  def connectWires0(module: x906_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x898_reg.connectLedger(module.io.in_x898_reg)
    x900_reg.connectLedger(module.io.in_x900_reg)
    x899_reg.connectLedger(module.io.in_x899_reg)
    module.io.in_b47 <> b47
  }
  val b47 = list_b47(0)
  val x898_reg = list_x898_reg(0)
  val x900_reg = list_x898_reg(1)
  val x899_reg = list_x898_reg(2)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x906_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x906_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x906_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x901 = Wire(Bool()).suggestName("""x901""")
      x901.r := Math.eql(b47, 3L.FP(true, 32, 0), Some(0.2), true.B,"x901").r
      val x902 = Wire(Bool()).suggestName("""x902""")
      x902 := ~x901
      val x903_wr_x899_banks = List[UInt]()
      val x903_wr_x899_ofs = List[UInt]()
      val x903_wr_x899_en = List[Bool](true.B)
      val x903_wr_x899_data = List[UInt](x901.r)
      x899_reg.connectWPort(903, x903_wr_x899_banks, x903_wr_x899_ofs, x903_wr_x899_data, x903_wr_x899_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x904_wr_x898_banks = List[UInt]()
      val x904_wr_x898_ofs = List[UInt]()
      val x904_wr_x898_en = List[Bool](true.B)
      val x904_wr_x898_data = List[UInt](x901.r)
      x898_reg.connectWPort(904, x904_wr_x898_banks, x904_wr_x898_ofs, x904_wr_x898_data, x904_wr_x898_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x905_wr_x900_banks = List[UInt]()
      val x905_wr_x900_ofs = List[UInt]()
      val x905_wr_x900_en = List[Bool](true.B)
      val x905_wr_x900_data = List[UInt](x902.r)
      x900_reg.connectWPort(905, x905_wr_x900_banks, x905_wr_x900_ofs, x905_wr_x900_data, x905_wr_x900_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x906_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x906_inr_UnitPipe **/
