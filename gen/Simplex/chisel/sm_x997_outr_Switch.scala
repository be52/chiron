package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x997 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x997_outr_Switch **/
class x997_outr_Switch_kernel(
  list_x1105_rd_x900: List[Bool],
  list_x760_reg: List[StandardInterface],
  list_x580_nrow: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Fork, 2, isFSM = false   ,cases = 2, latency = 0.0.toInt, myName = "x997_outr_Switch_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x997_outr_Switch_iiCtr"))
  
  abstract class x997_outr_Switch_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x760_reg = Flipped(new StandardInterface(ModuleParams.getParams("x760_reg_p").asInstanceOf[MemParams] ))
      val in_x1105_rd_x900 = Input(Bool())
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x898_reg = Flipped(new StandardInterface(ModuleParams.getParams("x898_reg_p").asInstanceOf[MemParams] ))
      val in_x1104_rd_x898 = Input(Bool())
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x847_reg = Flipped(new StandardInterface(ModuleParams.getParams("x847_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x848_reg = Flipped(new StandardInterface(ModuleParams.getParams("x848_reg_p").asInstanceOf[MemParams] ))
      val in_x668_iter_0 = Flipped(new StandardInterface(ModuleParams.getParams("x668_iter_0_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x899_reg = Flipped(new StandardInterface(ModuleParams.getParams("x899_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x760_reg = {io.in_x760_reg} ; io.in_x760_reg := DontCare
    def x1105_rd_x900 = {io.in_x1105_rd_x900} 
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x898_reg = {io.in_x898_reg} ; io.in_x898_reg := DontCare
    def x1104_rd_x898 = {io.in_x1104_rd_x898} 
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x847_reg = {io.in_x847_reg} ; io.in_x847_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x848_reg = {io.in_x848_reg} ; io.in_x848_reg := DontCare
    def x668_iter_0 = {io.in_x668_iter_0} ; io.in_x668_iter_0 := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x580_nrow = {io.in_x580_nrow} 
    def x581_ncol = {io.in_x581_ncol} 
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x899_reg = {io.in_x899_reg} ; io.in_x899_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x997_outr_Switch_module)(implicit stack: List[KernelHash]): Unit = {
    x760_reg.connectLedger(module.io.in_x760_reg)
    module.io.in_x1105_rd_x900 <> x1105_rd_x900
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x898_reg.connectLedger(module.io.in_x898_reg)
    module.io.in_x1104_rd_x898 <> x1104_rd_x898
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x847_reg.connectLedger(module.io.in_x847_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x848_reg.connectLedger(module.io.in_x848_reg)
    x668_iter_0.connectLedger(module.io.in_x668_iter_0)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    module.io.in_x580_nrow <> x580_nrow
    module.io.in_x581_ncol <> x581_ncol
    x759_reg.connectLedger(module.io.in_x759_reg)
    x899_reg.connectLedger(module.io.in_x899_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x1105_rd_x900 = list_x1105_rd_x900(0)
  val x1104_rd_x898 = list_x1105_rd_x900(1)
  val x760_reg = list_x760_reg(0)
  val x597_tab_sram_0 = list_x760_reg(1)
  val x898_reg = list_x760_reg(2)
  val x598_tab_sram_1 = list_x760_reg(3)
  val x847_reg = list_x760_reg(4)
  val x679_pivot_col_0 = list_x760_reg(5)
  val x674_pivot_element_0 = list_x760_reg(6)
  val x848_reg = list_x760_reg(7)
  val x668_iter_0 = list_x760_reg(8)
  val x685_reg = list_x760_reg(9)
  val x680_pivot_row_0 = list_x760_reg(10)
  val x759_reg = list_x760_reg(11)
  val x899_reg = list_x760_reg(12)
  val x684_reg = list_x760_reg(13)
  val x580_nrow = list_x580_nrow(0)
  val x581_ncol = list_x580_nrow(1)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x997_outr_Switch_obj")
    implicit val stack = ControllerStack.stack.toList
    class x997_outr_Switch_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x997_outr_Switch_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x1136_outr_SwitchCase_obj = new x1136_outr_SwitchCase_kernel(List(x760_reg,x597_tab_sram_0,x898_reg,x598_tab_sram_1,x847_reg,x679_pivot_col_0,x674_pivot_element_0,x848_reg,x668_iter_0,x685_reg,x680_pivot_row_0,x759_reg,x899_reg,x684_reg), List(x580_nrow,x581_ncol) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x1136_outr_SwitchCase_obj.sm.io.ctrDone := risingEdge(x1136_outr_SwitchCase_obj.sm.io.ctrInc)
      x1136_outr_SwitchCase_obj.backpressure := true.B | x1136_outr_SwitchCase_obj.sm.io.doneLatch
      x1136_outr_SwitchCase_obj.forwardpressure := true.B | x1136_outr_SwitchCase_obj.sm.io.doneLatch
      x1136_outr_SwitchCase_obj.sm.io.enableOut.zip(x1136_outr_SwitchCase_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x1136_outr_SwitchCase_obj.sm.io.break := false.B
      x1136_outr_SwitchCase_obj.mask := true.B & true.B
      x1136_outr_SwitchCase_obj.configure("x1136_outr_SwitchCase_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1136_outr_SwitchCase_obj.kernel()
      val x996_inr_SwitchCase_obj = new x996_inr_SwitchCase_kernel(  Some(me), List(), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x996_inr_SwitchCase_obj.baseEn := io.sigsIn.smSelectsOut(x996_inr_SwitchCase_obj.childId)
      x996_inr_SwitchCase_obj.sm.io.ctrDone := risingEdge(x996_inr_SwitchCase_obj.sm.io.ctrInc)
      x996_inr_SwitchCase_obj.backpressure := true.B | x996_inr_SwitchCase_obj.sm.io.doneLatch
      x996_inr_SwitchCase_obj.forwardpressure := true.B | x996_inr_SwitchCase_obj.sm.io.doneLatch
      x996_inr_SwitchCase_obj.sm.io.enableOut.zip(x996_inr_SwitchCase_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x996_inr_SwitchCase_obj.sm.io.break := false.B
      x996_inr_SwitchCase_obj.mask := true.B & true.B
      x996_inr_SwitchCase_obj.configure("x996_inr_SwitchCase_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x996_inr_SwitchCase_obj.kernel()
    }
    val module = Module(new x997_outr_Switch_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END Switch x997_outr_Switch **/
