package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1018 -> x1020 -> x1137 **/
/** BEGIN None x1018_inr_UnitPipe **/
class x1018_inr_UnitPipe_kernel(
  list_b47: List[FixedPoint],
  list_x1007_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 2.7.toInt, myName = "x1018_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1018_inr_UnitPipe_iiCtr"))
  
  abstract class x1018_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x1007_reg = Flipped(new StandardInterface(ModuleParams.getParams("x1007_reg_p").asInstanceOf[MemParams] ))
      val in_x672_no_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x672_no_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x668_iter_0 = Flipped(new StandardInterface(ModuleParams.getParams("x668_iter_0_p").asInstanceOf[MemParams] ))
      val in_x671_no_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x671_no_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_b47 = Input(new FixedPoint(true, 32, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x1007_reg = {io.in_x1007_reg} ; io.in_x1007_reg := DontCare
    def x672_no_pivot_row_0 = {io.in_x672_no_pivot_row_0} ; io.in_x672_no_pivot_row_0 := DontCare
    def x668_iter_0 = {io.in_x668_iter_0} ; io.in_x668_iter_0 := DontCare
    def x671_no_pivot_col_0 = {io.in_x671_no_pivot_col_0} ; io.in_x671_no_pivot_col_0 := DontCare
    def b47 = {io.in_b47} 
  }
  def connectWires0(module: x1018_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x1007_reg.connectLedger(module.io.in_x1007_reg)
    x672_no_pivot_row_0.connectLedger(module.io.in_x672_no_pivot_row_0)
    x668_iter_0.connectLedger(module.io.in_x668_iter_0)
    x671_no_pivot_col_0.connectLedger(module.io.in_x671_no_pivot_col_0)
    module.io.in_b47 <> b47
  }
  val b47 = list_b47(0)
  val x1007_reg = list_x1007_reg(0)
  val x672_no_pivot_row_0 = list_x1007_reg(1)
  val x668_iter_0 = list_x1007_reg(2)
  val x671_no_pivot_col_0 = list_x1007_reg(3)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1018_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x1018_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1018_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x1008_rd_x668 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1008_rd_x668""")
      val x1008_rd_x668_banks = List[UInt]()
      val x1008_rd_x668_ofs = List[UInt]()
      val x1008_rd_x668_en = List[Bool](true.B)
      x1008_rd_x668.toSeq.zip(x668_iter_0.connectRPort(1008, x1008_rd_x668_banks, x1008_rd_x668_ofs, io.sigsIn.backpressure, x1008_rd_x668_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1009 = Wire(Bool()).suggestName("""x1009""")
      x1009.r := Math.lte(10L.FP(true, 32, 0), x1008_rd_x668, Some(0.4), true.B,"x1009").r
      val x1010_rd_x671 = Wire(Bool()).suggestName("""x1010_rd_x671""")
      val x1010_rd_x671_banks = List[UInt]()
      val x1010_rd_x671_ofs = List[UInt]()
      val x1010_rd_x671_en = List[Bool](true.B)
      x1010_rd_x671.toSeq.zip(x671_no_pivot_col_0.connectRPort(1010, x1010_rd_x671_banks, x1010_rd_x671_ofs, io.sigsIn.backpressure, x1010_rd_x671_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1011 = Wire(Bool()).suggestName("""x1011""")
      x1011 := x1009 | x1010_rd_x671
      val x1012_rd_x672 = Wire(Bool()).suggestName("""x1012_rd_x672""")
      val x1012_rd_x672_banks = List[UInt]()
      val x1012_rd_x672_ofs = List[UInt]()
      val x1012_rd_x672_en = List[Bool](true.B)
      x1012_rd_x672.toSeq.zip(x672_no_pivot_row_0.connectRPort(1012, x1012_rd_x672_banks, x1012_rd_x672_ofs, io.sigsIn.backpressure, x1012_rd_x672_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1013 = Wire(Bool()).suggestName("""x1013""")
      x1013 := x1011 | x1012_rd_x672
      val x1014_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1014_sum""")
      x1014_sum.r := Math.add(b47,1L.FP(true, 32, 0),Some(1.0), true.B, Truncate, Wrapping, "x1014_sum").r
      val x1130 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1130""")
      x1130.r := Math.and(x1014_sum,3L.FP(true, 32, 0),Some(0.2), true.B,"x1130").r
      val x1224 = Wire(Bool()).suggestName("x1224_x1013_D1") 
      x1224.r := getRetimed(x1013.r, 1.toInt, io.sigsIn.backpressure)
      val x1016 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1016""")
      x1016.r := Mux((x1224), 4L.FP(true, 32, 0).r, x1130.r)
      val x1017_wr_x1007_banks = List[UInt]()
      val x1017_wr_x1007_ofs = List[UInt]()
      val x1017_wr_x1007_en = List[Bool](true.B)
      val x1017_wr_x1007_data = List[UInt](x1016.r)
      x1007_reg.connectWPort(1017, x1017_wr_x1007_banks, x1017_wr_x1007_ofs, x1017_wr_x1007_data, x1017_wr_x1007_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(1.7.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x1018_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x1018_inr_UnitPipe **/
