package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x980 -> x982 -> x984 -> x985 -> x1135 -> x1136 -> x997 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x980_inr_Foreach **/
class x980_inr_Foreach_kernel(
  list_x1115_rd_x759: List[Bool],
  list_b916: List[FixedPoint],
  list_x919_reg: List[NBufInterface],
  list_x760_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Pipelined, false   , latency = 29.0.toInt, myName = "x980_inr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x980_inr_Foreach_iiCtr"))
  
  abstract class x980_inr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x760_reg = Flipped(new StandardInterface(ModuleParams.getParams("x760_reg_p").asInstanceOf[MemParams] ))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x898_reg = Flipped(new StandardInterface(ModuleParams.getParams("x898_reg_p").asInstanceOf[MemParams] ))
      val in_x1115_rd_x759 = Input(Bool())
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_b916 = Input(new FixedPoint(true, 32, 0))
      val in_x1118_rd_x918 = Input(Bool())
      val in_x1114_rd_x685 = Input(Bool())
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x919_reg = Flipped(new NBufInterface(ModuleParams.getParams("x919_reg_p").asInstanceOf[NBufParams] ))
      val in_x1117_rd_x899 = Input(Bool())
      val in_x848_reg = Flipped(new StandardInterface(ModuleParams.getParams("x848_reg_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_x1116_rd_x847 = Input(Bool())
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x760_reg = {io.in_x760_reg} ; io.in_x760_reg := DontCare
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x898_reg = {io.in_x898_reg} ; io.in_x898_reg := DontCare
    def x1115_rd_x759 = {io.in_x1115_rd_x759} 
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def b916 = {io.in_b916} 
    def x1118_rd_x918 = {io.in_x1118_rd_x918} 
    def x1114_rd_x685 = {io.in_x1114_rd_x685} 
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x919_reg = {io.in_x919_reg} ; io.in_x919_reg := DontCare
    def x1117_rd_x899 = {io.in_x1117_rd_x899} 
    def x848_reg = {io.in_x848_reg} ; io.in_x848_reg := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
    def x1116_rd_x847 = {io.in_x1116_rd_x847} 
  }
  def connectWires0(module: x980_inr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    x760_reg.connectLedger(module.io.in_x760_reg)
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x898_reg.connectLedger(module.io.in_x898_reg)
    module.io.in_x1115_rd_x759 <> x1115_rd_x759
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    module.io.in_b916 <> b916
    module.io.in_x1118_rd_x918 <> x1118_rd_x918
    module.io.in_x1114_rd_x685 <> x1114_rd_x685
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x919_reg.connectLedger(module.io.in_x919_reg)
    module.io.in_x1117_rd_x899 <> x1117_rd_x899
    x848_reg.connectLedger(module.io.in_x848_reg)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    x684_reg.connectLedger(module.io.in_x684_reg)
    module.io.in_x1116_rd_x847 <> x1116_rd_x847
  }
  val x1115_rd_x759 = list_x1115_rd_x759(0)
  val x1118_rd_x918 = list_x1115_rd_x759(1)
  val x1114_rd_x685 = list_x1115_rd_x759(2)
  val x1117_rd_x899 = list_x1115_rd_x759(3)
  val x1116_rd_x847 = list_x1115_rd_x759(4)
  val b916 = list_b916(0)
  val x919_reg = list_x919_reg(0)
  val x760_reg = list_x760_reg(0)
  val x597_tab_sram_0 = list_x760_reg(1)
  val x898_reg = list_x760_reg(2)
  val x598_tab_sram_1 = list_x760_reg(3)
  val x674_pivot_element_0 = list_x760_reg(4)
  val x848_reg = list_x760_reg(5)
  val x680_pivot_row_0 = list_x760_reg(6)
  val x684_reg = list_x760_reg(7)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x980_inr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x980_inr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x980_inr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b953 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b953.suggestName("b953")
      val b954 = ~io.sigsIn.cchainOutputs.head.oobs(0); b954.suggestName("b954")
      val x955_rd_x919 = Wire(Bool()).suggestName("""x955_rd_x919""")
      val x955_rd_x919_banks = List[UInt]()
      val x955_rd_x919_ofs = List[UInt]()
      val x955_rd_x919_en = List[Bool](true.B)
      x955_rd_x919.toSeq.zip(x919_reg.connectRPort(955, x955_rd_x919_banks, x955_rd_x919_ofs, io.sigsIn.backpressure, x955_rd_x919_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x956_rd_x898 = Wire(Bool()).suggestName("""x956_rd_x898""")
      val x956_rd_x898_banks = List[UInt]()
      val x956_rd_x898_ofs = List[UInt]()
      val x956_rd_x898_en = List[Bool](true.B)
      x956_rd_x898.toSeq.zip(x898_reg.connectRPort(956, x956_rd_x898_banks, x956_rd_x898_ofs, io.sigsIn.backpressure, x956_rd_x898_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x957_rd_x848 = Wire(Bool()).suggestName("""x957_rd_x848""")
      val x957_rd_x848_banks = List[UInt]()
      val x957_rd_x848_ofs = List[UInt]()
      val x957_rd_x848_en = List[Bool](true.B)
      x957_rd_x848.toSeq.zip(x848_reg.connectRPort(957, x957_rd_x848_banks, x957_rd_x848_ofs, io.sigsIn.backpressure, x957_rd_x848_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x958_rd_x760 = Wire(Bool()).suggestName("""x958_rd_x760""")
      val x958_rd_x760_banks = List[UInt]()
      val x958_rd_x760_ofs = List[UInt]()
      val x958_rd_x760_en = List[Bool](true.B)
      x958_rd_x760.toSeq.zip(x760_reg.connectRPort(958, x958_rd_x760_banks, x958_rd_x760_ofs, io.sigsIn.backpressure, x958_rd_x760_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x959_rd_x684 = Wire(Bool()).suggestName("""x959_rd_x684""")
      val x959_rd_x684_banks = List[UInt]()
      val x959_rd_x684_ofs = List[UInt]()
      val x959_rd_x684_en = List[Bool](true.B)
      x959_rd_x684.toSeq.zip(x684_reg.connectRPort(959, x959_rd_x684_banks, x959_rd_x684_ofs, io.sigsIn.backpressure, x959_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1128 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1128""")
      x1128.r := Math.fma(b916,10L.FP(true, 32, 0),b953,Some(6.0), true.B, "x1128").toFixed(x1128, "cast_x1128").r
      val x1199 = Wire(Bool()).suggestName("x1199_x956_rd_x898_D14") 
      x1199.r := getRetimed(x956_rd_x898.r, 14.toInt, io.sigsIn.backpressure)
      val x1200 = Wire(Bool()).suggestName("x1200_x957_rd_x848_D14") 
      x1200.r := getRetimed(x957_rd_x848.r, 14.toInt, io.sigsIn.backpressure)
      val x1201 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1201_x1128_D8") 
      x1201.r := getRetimed(x1128.r, 8.toInt, io.sigsIn.backpressure)
      val x1202 = Wire(Bool()).suggestName("x1202_x958_rd_x760_D14") 
      x1202.r := getRetimed(x958_rd_x760.r, 14.toInt, io.sigsIn.backpressure)
      val x1203 = Wire(Bool()).suggestName("x1203_x959_rd_x684_D14") 
      x1203.r := getRetimed(x959_rd_x684.r, 14.toInt, io.sigsIn.backpressure)
      val x1204 = Wire(Bool()).suggestName("x1204_b954_D14") 
      x1204.r := getRetimed(b954.r, 14.toInt, io.sigsIn.backpressure)
      val x1205 = Wire(Bool()).suggestName("x1205_x955_rd_x919_D14") 
      x1205.r := getRetimed(x955_rd_x919.r, 14.toInt, io.sigsIn.backpressure)
      val x965_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x965_rd""")
      val x965_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x965_rd_ofs = List[UInt](x1201.r)
      val x965_rd_en = List[Bool](true.B)
      x965_rd.toSeq.zip(x598_tab_sram_1.connectRPort(965, x965_rd_banks, x965_rd_ofs, io.sigsIn.backpressure, x965_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(14.0.toInt, rr, io.sigsIn.backpressure) && x1202 & x1205 & x1199 & x1203 & x1200 & x1204), true)).foreach{case (a,b) => a := b}
      val x966_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x966_elem_0""")
      x966_elem_0.r := x965_rd(0).r
      val x967_rd_x674 = Wire(new FloatingPoint(24, 8)).suggestName("""x967_rd_x674""")
      val x967_rd_x674_banks = List[UInt]()
      val x967_rd_x674_ofs = List[UInt]()
      val x967_rd_x674_en = List[Bool](true.B)
      x967_rd_x674.toSeq.zip(x674_pivot_element_0.connectRPort(967, x967_rd_x674_banks, x967_rd_x674_ofs, io.sigsIn.backpressure, x967_rd_x674_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x968_rd_x680 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x968_rd_x680""")
      val x968_rd_x680_banks = List[UInt]()
      val x968_rd_x680_ofs = List[UInt]()
      val x968_rd_x680_en = List[Bool](true.B)
      x968_rd_x680.toSeq.zip(x680_pivot_row_0.connectRPort(968, x968_rd_x680_banks, x968_rd_x680_ofs, io.sigsIn.backpressure, x968_rd_x680_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1129 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1129""")
      x1129.r := Math.fma(x968_rd_x680,10L.FP(true, 32, 0),b953,Some(6.0), true.B, "x1129").toFixed(x1129, "cast_x1129").r
      val x1206 = Wire(Bool()).suggestName("x1206_x956_rd_x898_D6") 
      x1206.r := getRetimed(x956_rd_x898.r, 6.toInt, io.sigsIn.backpressure)
      val x1207 = Wire(Bool()).suggestName("x1207_x957_rd_x848_D6") 
      x1207.r := getRetimed(x957_rd_x848.r, 6.toInt, io.sigsIn.backpressure)
      val x1208 = Wire(Bool()).suggestName("x1208_x958_rd_x760_D6") 
      x1208.r := getRetimed(x958_rd_x760.r, 6.toInt, io.sigsIn.backpressure)
      val x1209 = Wire(Bool()).suggestName("x1209_x959_rd_x684_D6") 
      x1209.r := getRetimed(x959_rd_x684.r, 6.toInt, io.sigsIn.backpressure)
      val x1210 = Wire(Bool()).suggestName("x1210_b954_D6") 
      x1210.r := getRetimed(b954.r, 6.toInt, io.sigsIn.backpressure)
      val x1211 = Wire(Bool()).suggestName("x1211_x955_rd_x919_D6") 
      x1211.r := getRetimed(x955_rd_x919.r, 6.toInt, io.sigsIn.backpressure)
      val x974_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x974_rd""")
      val x974_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x974_rd_ofs = List[UInt](x1129.r)
      val x974_rd_en = List[Bool](true.B)
      x974_rd.toSeq.zip(x597_tab_sram_0.connectRPort(974, x974_rd_banks, x974_rd_ofs, io.sigsIn.backpressure, x974_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(6.0.toInt, rr, io.sigsIn.backpressure) && x1208 & x1206 & x1207 & x1211 & x1209 & x1210), true)).foreach{case (a,b) => a := b}
      val x975_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x975_elem_0""")
      x975_elem_0.r := x974_rd(0).r
      val x1212 = Wire(new FloatingPoint(24, 8)).suggestName("x1212_x967_rd_x674_D8") 
      x1212.r := getRetimed(x967_rd_x674.r, 8.toInt, io.sigsIn.backpressure)
      val x976 = Wire(new FloatingPoint(24, 8)).suggestName("""x976""")
      x976.r := Math.fmul(x1212, x975_elem_0, Some(8.0), true.B,"x976").r
      val x977 = Wire(new FloatingPoint(24, 8)).suggestName("""x977""")
      x977.r := Math.fsub(x966_elem_0, x976, Some(12.0), true.B,"x977").r
      val x1213 = Wire(Bool()).suggestName("x1213_x956_rd_x898_D28") 
      x1213.r := getRetimed(x956_rd_x898.r, 28.toInt, io.sigsIn.backpressure)
      val x1214 = Wire(Bool()).suggestName("x1214_x957_rd_x848_D28") 
      x1214.r := getRetimed(x957_rd_x848.r, 28.toInt, io.sigsIn.backpressure)
      val x1215 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1215_x1128_D22") 
      x1215.r := getRetimed(x1128.r, 22.toInt, io.sigsIn.backpressure)
      val x1216 = Wire(Bool()).suggestName("x1216_x958_rd_x760_D28") 
      x1216.r := getRetimed(x958_rd_x760.r, 28.toInt, io.sigsIn.backpressure)
      val x1217 = Wire(Bool()).suggestName("x1217_x959_rd_x684_D28") 
      x1217.r := getRetimed(x959_rd_x684.r, 28.toInt, io.sigsIn.backpressure)
      val x1218 = Wire(Bool()).suggestName("x1218_b954_D28") 
      x1218.r := getRetimed(b954.r, 28.toInt, io.sigsIn.backpressure)
      val x1219 = Wire(Bool()).suggestName("x1219_x955_rd_x919_D28") 
      x1219.r := getRetimed(x955_rd_x919.r, 28.toInt, io.sigsIn.backpressure)
      val x978_wr_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x978_wr_ofs = List[UInt](x1215.r)
      val x978_wr_en = List[Bool](true.B)
      val x978_wr_data = List[UInt](x977.r)
      x597_tab_sram_0.connectWPort(978, x978_wr_banks, x978_wr_ofs, x978_wr_data, x978_wr_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(28.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1216 & x1219 & x1213 & x1217 & x1214 & x1218))
      val x979_wr_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x979_wr_ofs = List[UInt](x1215.r)
      val x979_wr_en = List[Bool](true.B)
      val x979_wr_data = List[UInt](x977.r)
      x598_tab_sram_1.connectWPort(979, x979_wr_banks, x979_wr_ofs, x979_wr_data, x979_wr_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(28.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1216 & x1219 & x1213 & x1217 & x1214 & x1218))
    }
    val module = Module(new x980_inr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x980_inr_Foreach **/
