package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x644 -> x666 -> x667 -> x1134 -> x1137 **/
/** BEGIN None x644_inr_UnitPipe **/
class x644_inr_UnitPipe_kernel(
  list_b632: List[Bool],
  list_x601_fifo: List[FIFOInterface],
  list_x634_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 3.0.toInt, myName = "x644_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x644_inr_UnitPipe_iiCtr"))
  
  abstract class x644_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x634_reg = Flipped(new StandardInterface(ModuleParams.getParams("x634_reg_p").asInstanceOf[MemParams] ))
      val in_x633_reg = Flipped(new StandardInterface(ModuleParams.getParams("x633_reg_p").asInstanceOf[MemParams] ))
      val in_x601_fifo = Flipped(new FIFOInterface(ModuleParams.getParams("x601_fifo_p").asInstanceOf[MemParams] ))
      val in_b632 = Input(Bool())
      val in_x635_reg = Flipped(new StandardInterface(ModuleParams.getParams("x635_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x634_reg = {io.in_x634_reg} ; io.in_x634_reg := DontCare
    def x633_reg = {io.in_x633_reg} ; io.in_x633_reg := DontCare
    def x601_fifo = {io.in_x601_fifo} ; io.in_x601_fifo := DontCare
    def b632 = {io.in_b632} 
    def x635_reg = {io.in_x635_reg} ; io.in_x635_reg := DontCare
  }
  def connectWires0(module: x644_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x634_reg.connectLedger(module.io.in_x634_reg)
    x633_reg.connectLedger(module.io.in_x633_reg)
    x601_fifo.connectLedger(module.io.in_x601_fifo)
    module.io.in_b632 <> b632
    x635_reg.connectLedger(module.io.in_x635_reg)
  }
  val b632 = list_b632(0)
  val x601_fifo = list_x601_fifo(0)
  val x634_reg = list_x634_reg(0)
  val x633_reg = list_x634_reg(1)
  val x635_reg = list_x634_reg(2)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x644_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x644_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x644_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x636_deq_x601 = Wire(Vec(1, UInt(96.W))).suggestName("""x636_deq_x601""")
      val x636_deq_x601_banks = List[UInt]()
      val x636_deq_x601_ofs = List[UInt]()
      val x636_deq_x601_en = List[Bool](true.B)
      x636_deq_x601.toSeq.zip(x601_fifo.connectRPort(636, x636_deq_x601_banks, x636_deq_x601_ofs, io.sigsIn.backpressure, x636_deq_x601_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      x601_fifo.connectAccessActivesIn(1, ((true.B)))
      val x1145 = Wire(Vec(1, UInt(96.W))).suggestName("x1145_x636_deq_x601_D2") 
      (0 until 1).foreach{i => x1145(i).r := getRetimed(x636_deq_x601(i).r, 2.toInt, io.sigsIn.backpressure)}
      val x637_elem_0 = Wire(UInt(96.W)).suggestName("""x637_elem_0""")
      x637_elem_0.r := x1145(0).r
      val x638_apply = Wire(new FixedPoint(true, 32, 0)).suggestName("""x638_apply""")
      x638_apply.r := x637_elem_0(63, 32)
      val x639_wr_x633_banks = List[UInt]()
      val x639_wr_x633_ofs = List[UInt]()
      val x639_wr_x633_en = List[Bool](true.B)
      val x639_wr_x633_data = List[UInt](x638_apply.r)
      x633_reg.connectWPort(639, x639_wr_x633_banks, x639_wr_x633_ofs, x639_wr_x633_data, x639_wr_x633_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(2.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x640_apply = Wire(new FixedPoint(true, 32, 0)).suggestName("""x640_apply""")
      x640_apply.r := x637_elem_0(95, 64)
      val x641_wr_x634_banks = List[UInt]()
      val x641_wr_x634_ofs = List[UInt]()
      val x641_wr_x634_en = List[Bool](true.B)
      val x641_wr_x634_data = List[UInt](x640_apply.r)
      x634_reg.connectWPort(641, x641_wr_x634_banks, x641_wr_x634_ofs, x641_wr_x634_data, x641_wr_x634_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(2.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x642_apply = Wire(new FixedPoint(true, 32, 0)).suggestName("""x642_apply""")
      x642_apply.r := x637_elem_0(31, 0)
      val x643_wr_x635_banks = List[UInt]()
      val x643_wr_x635_ofs = List[UInt]()
      val x643_wr_x635_en = List[Bool](true.B)
      val x643_wr_x635_data = List[UInt](x642_apply.r)
      x635_reg.connectWPort(643, x643_wr_x635_banks, x643_wr_x635_ofs, x643_wr_x635_data, x643_wr_x635_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(2.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x644_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x644_inr_UnitPipe **/
