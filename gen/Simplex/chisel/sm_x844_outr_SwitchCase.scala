package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x844 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x844_outr_SwitchCase **/
class x844_outr_SwitchCase_kernel(
  list_x597_tab_sram_0: List[StandardInterface],
  list_x582_ncons: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 4, isFSM = false   , latency = 0.0.toInt, myName = "x844_outr_SwitchCase_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x844_outr_SwitchCase_iiCtr"))
  
  abstract class x844_outr_SwitchCase_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x582_ncons = Input(UInt(64.W))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x757_reg = Flipped(new StandardInterface(ModuleParams.getParams("x757_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x672_no_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x672_no_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x758_reg = Flipped(new StandardInterface(ModuleParams.getParams("x758_reg_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x675_pivot_row_min_ratio_0 = Flipped(new StandardInterface(ModuleParams.getParams("x675_pivot_row_min_ratio_0_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x673_no_pivot_row_count_0 = Flipped(new StandardInterface(ModuleParams.getParams("x673_no_pivot_row_count_0_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(4, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(4, 1))
      val rr = Input(Bool())
    })
    def x582_ncons = {io.in_x582_ncons} 
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x757_reg = {io.in_x757_reg} ; io.in_x757_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x672_no_pivot_row_0 = {io.in_x672_no_pivot_row_0} ; io.in_x672_no_pivot_row_0 := DontCare
    def x758_reg = {io.in_x758_reg} ; io.in_x758_reg := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x675_pivot_row_min_ratio_0 = {io.in_x675_pivot_row_min_ratio_0} ; io.in_x675_pivot_row_min_ratio_0 := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x673_no_pivot_row_count_0 = {io.in_x673_no_pivot_row_count_0} ; io.in_x673_no_pivot_row_count_0 := DontCare
    def x581_ncol = {io.in_x581_ncol} 
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x844_outr_SwitchCase_module)(implicit stack: List[KernelHash]): Unit = {
    module.io.in_x582_ncons <> x582_ncons
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x757_reg.connectLedger(module.io.in_x757_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x672_no_pivot_row_0.connectLedger(module.io.in_x672_no_pivot_row_0)
    x758_reg.connectLedger(module.io.in_x758_reg)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x675_pivot_row_min_ratio_0.connectLedger(module.io.in_x675_pivot_row_min_ratio_0)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    x673_no_pivot_row_count_0.connectLedger(module.io.in_x673_no_pivot_row_count_0)
    module.io.in_x581_ncol <> x581_ncol
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x598_tab_sram_1 = list_x597_tab_sram_0(1)
  val x757_reg = list_x597_tab_sram_0(2)
  val x679_pivot_col_0 = list_x597_tab_sram_0(3)
  val x674_pivot_element_0 = list_x597_tab_sram_0(4)
  val x672_no_pivot_row_0 = list_x597_tab_sram_0(5)
  val x758_reg = list_x597_tab_sram_0(6)
  val x685_reg = list_x597_tab_sram_0(7)
  val x675_pivot_row_min_ratio_0 = list_x597_tab_sram_0(8)
  val x680_pivot_row_0 = list_x597_tab_sram_0(9)
  val x673_no_pivot_row_count_0 = list_x597_tab_sram_0(10)
  val x684_reg = list_x597_tab_sram_0(11)
  val x582_ncons = list_x582_ncons(0)
  val x581_ncol = list_x582_ncons(1)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x844_outr_SwitchCase_obj")
    implicit val stack = ControllerStack.stack.toList
    class x844_outr_SwitchCase_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x844_outr_SwitchCase_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      // Controller Stack: Stack(x1003, x1005, x1006, x1020, x1137)
      val x774_inr_UnitPipe = new x774_inr_UnitPipe_kernel(List(x757_reg,x675_pivot_row_min_ratio_0,x673_no_pivot_row_count_0,x684_reg) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x774_inr_UnitPipe.sm.io.ctrDone := risingEdge(x774_inr_UnitPipe.sm.io.ctrInc)
      x774_inr_UnitPipe.backpressure := true.B | x774_inr_UnitPipe.sm.io.doneLatch
      x774_inr_UnitPipe.forwardpressure := true.B | x774_inr_UnitPipe.sm.io.doneLatch
      x774_inr_UnitPipe.sm.io.enableOut.zip(x774_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x774_inr_UnitPipe.sm.io.break := false.B
      x774_inr_UnitPipe.mask := true.B & true.B
      x774_inr_UnitPipe.configure("x774_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x774_inr_UnitPipe.kernel()
      val x1093_rd_x582 = Wire(new FixedPoint(true, 32, 0))
      x1093_rd_x582.r := x582_ncons.r
      val x776_ctr = new CtrObject(Left(Some(0)), Right(x1093_rd_x582), Left(Some(1)), 1, 32, false)
      val x777_ctrchain = (new CChainObject(List[CtrObject](x776_ctr), "x777_ctrchain")).cchain.io 
      x777_ctrchain.setup.isStream := false.B
      ModuleParams.addParams("x777_ctrchain_p", (x777_ctrchain.par, x777_ctrchain.widths))
      val x1094_rd_x684 = Wire(Bool()).suggestName("""x1094_rd_x684""")
      val x1094_rd_x684_banks = List[UInt]()
      val x1094_rd_x684_ofs = List[UInt]()
      val x1094_rd_x684_en = List[Bool](true.B)
      x1094_rd_x684.toSeq.zip(x684_reg.connectRPort(1094, x1094_rd_x684_banks, x1094_rd_x684_ofs, io.sigsIn.backpressure, x1094_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1095_rd_x757 = Wire(Bool()).suggestName("""x1095_rd_x757""")
      val x1095_rd_x757_banks = List[UInt]()
      val x1095_rd_x757_ofs = List[UInt]()
      val x1095_rd_x757_en = List[Bool](true.B)
      x1095_rd_x757.toSeq.zip(x757_reg.connectRPort(1095, x1095_rd_x757_banks, x1095_rd_x757_ofs, io.sigsIn.backpressure, x1095_rd_x757_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x824_inr_Foreach = new x824_inr_Foreach_kernel(List(x1095_rd_x757,x1094_rd_x684), List(x597_tab_sram_0,x598_tab_sram_1,x757_reg,x679_pivot_col_0,x674_pivot_element_0,x758_reg,x685_reg,x675_pivot_row_min_ratio_0,x680_pivot_row_0,x673_no_pivot_row_count_0,x684_reg), List(x581_ncol) , Some(me), List(x777_ctrchain), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x824_inr_Foreach.sm.io.ctrDone := (x824_inr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x824_inr_Foreach.backpressure := true.B | x824_inr_Foreach.sm.io.doneLatch
      x824_inr_Foreach.forwardpressure := true.B | x824_inr_Foreach.sm.io.doneLatch
      x824_inr_Foreach.sm.io.enableOut.zip(x824_inr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x824_inr_Foreach.sm.io.break := false.B
      x824_inr_Foreach.mask := ~x824_inr_Foreach.cchain.head.output.noop & x1094_rd_x684 & x1095_rd_x757
      x824_inr_Foreach.configure("x824_inr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x824_inr_Foreach.kernel()
      val x825_reg = (new x825_reg).m.io.asInstanceOf[StandardInterface]
      val x826_reg = (new x826_reg).m.io.asInstanceOf[StandardInterface]
      val x833_inr_UnitPipe = new x833_inr_UnitPipe_kernel(List(x825_reg,x826_reg,x673_no_pivot_row_count_0), List(x582_ncons) , Some(me), List(), 2, 1, 1, List(1), List(32), breakpoints, rr)
      x833_inr_UnitPipe.sm.io.ctrDone := risingEdge(x833_inr_UnitPipe.sm.io.ctrInc)
      x833_inr_UnitPipe.backpressure := true.B | x833_inr_UnitPipe.sm.io.doneLatch
      x833_inr_UnitPipe.forwardpressure := true.B | x833_inr_UnitPipe.sm.io.doneLatch
      x833_inr_UnitPipe.sm.io.enableOut.zip(x833_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x833_inr_UnitPipe.sm.io.break := false.B
      x833_inr_UnitPipe.mask := true.B & true.B
      x833_inr_UnitPipe.configure("x833_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x833_inr_UnitPipe.kernel()
      val x1096_rd_x825 = Wire(Bool()).suggestName("""x1096_rd_x825""")
      val x1096_rd_x825_banks = List[UInt]()
      val x1096_rd_x825_ofs = List[UInt]()
      val x1096_rd_x825_en = List[Bool](true.B)
      x1096_rd_x825.toSeq.zip(x825_reg.connectRPort(1096, x1096_rd_x825_banks, x1096_rd_x825_ofs, io.sigsIn.backpressure, x1096_rd_x825_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1097_rd_x826 = Wire(Bool()).suggestName("""x1097_rd_x826""")
      val x1097_rd_x826_banks = List[UInt]()
      val x1097_rd_x826_ofs = List[UInt]()
      val x1097_rd_x826_en = List[Bool](true.B)
      x1097_rd_x826.toSeq.zip(x826_reg.connectRPort(1097, x1097_rd_x826_banks, x1097_rd_x826_ofs, io.sigsIn.backpressure, x1097_rd_x826_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x842_inr_Switch_obj = new x842_inr_Switch_kernel(List(x1096_rd_x825,x1097_rd_x826), List(x825_reg,x757_reg,x672_no_pivot_row_0,x684_reg) , Some(me), List(), 3, 2, 1, List(1), List(32), breakpoints, rr)
      x842_inr_Switch_obj.sm.io.selectsIn(0) := x1096_rd_x825
      x842_inr_Switch_obj.sm.io.selectsIn(1) := x1097_rd_x826
      x842_inr_Switch_obj.backpressure := true.B | x842_inr_Switch_obj.sm.io.doneLatch
      x842_inr_Switch_obj.forwardpressure := true.B | x842_inr_Switch_obj.sm.io.doneLatch
      x842_inr_Switch_obj.sm.io.enableOut.zip(x842_inr_Switch_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x842_inr_Switch_obj.sm.io.break := false.B
      x842_inr_Switch_obj.mask := true.B & true.B
      x842_inr_Switch_obj.configure("x842_inr_Switch_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x842_inr_Switch_obj.kernel()
    }
    val module = Module(new x844_outr_SwitchCase_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END SwitchCase x844_outr_SwitchCase **/
