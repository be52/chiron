package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x678 -> x1134 -> x1137 **/
/** BEGIN None x678_inr_UnitPipe **/
class x678_inr_UnitPipe_kernel(
  list_x671_no_pivot_col_0: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.0.toInt, myName = "x678_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x678_inr_UnitPipe_iiCtr"))
  
  abstract class x678_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x671_no_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x671_no_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x672_no_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x672_no_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x671_no_pivot_col_0 = {io.in_x671_no_pivot_col_0} ; io.in_x671_no_pivot_col_0 := DontCare
    def x672_no_pivot_row_0 = {io.in_x672_no_pivot_row_0} ; io.in_x672_no_pivot_row_0 := DontCare
  }
  def connectWires0(module: x678_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x671_no_pivot_col_0.connectLedger(module.io.in_x671_no_pivot_col_0)
    x672_no_pivot_row_0.connectLedger(module.io.in_x672_no_pivot_row_0)
  }
  val x671_no_pivot_col_0 = list_x671_no_pivot_col_0(0)
  val x672_no_pivot_row_0 = list_x671_no_pivot_col_0(1)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x678_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x678_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x678_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x676_wr_x671_banks = List[UInt]()
      val x676_wr_x671_ofs = List[UInt]()
      val x676_wr_x671_en = List[Bool](true.B)
      val x676_wr_x671_data = List[UInt](false.B.r)
      x671_no_pivot_col_0.connectWPort(676, x676_wr_x671_banks, x676_wr_x671_ofs, x676_wr_x671_data, x676_wr_x671_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x677_wr_x672_banks = List[UInt]()
      val x677_wr_x672_ofs = List[UInt]()
      val x677_wr_x672_en = List[Bool](true.B)
      val x677_wr_x672_data = List[UInt](false.B.r)
      x672_no_pivot_row_0.connectWPort(677, x677_wr_x672_banks, x677_wr_x672_ofs, x677_wr_x672_data, x677_wr_x672_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x678_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x678_inr_UnitPipe **/
