package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1075 -> x1080 -> x1081 -> x1137 **/
/** BEGIN None x1075_outr_UnitPipe **/
class x1075_outr_UnitPipe_kernel(
  list_x597_tab_sram_0: List[StandardInterface],
  list_x581_ncol: List[UInt],
  list_b1027: List[FixedPoint],
  list_x1023: List[DecoupledIO[AppStoreData]],
  list_x1022: List[DecoupledIO[AppCommandDense]],
  list_b1028: List[Bool],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 2, isFSM = false   , latency = 0.0.toInt, myName = "x1075_outr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1075_outr_UnitPipe_iiCtr"))
  
  abstract class x1075_outr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_b1027 = Input(new FixedPoint(true, 32, 0))
      val in_b1028 = Input(Bool())
      val in_x1022 = Decoupled(new AppCommandDense(ModuleParams.getParams("x1022_p").asInstanceOf[(Int,Int)] ))
      val in_x1023 = Decoupled(new AppStoreData(ModuleParams.getParams("x1023_p").asInstanceOf[(Int,Int)] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x596_tab_dram_out = Input(new FixedPoint(true, 64, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def b1027 = {io.in_b1027} 
    def b1028 = {io.in_b1028} 
    def x1022 = {io.in_x1022} 
    def x1023 = {io.in_x1023} 
    def x581_ncol = {io.in_x581_ncol} 
    def x596_tab_dram_out = {io.in_x596_tab_dram_out} 
  }
  def connectWires0(module: x1075_outr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    module.io.in_b1027 <> b1027
    module.io.in_b1028 <> b1028
    module.io.in_x1022 <> x1022
    module.io.in_x1023 <> x1023
    module.io.in_x581_ncol <> x581_ncol
    module.io.in_x596_tab_dram_out <> x596_tab_dram_out
  }
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x581_ncol = list_x581_ncol(0)
  val b1027 = list_b1027(0)
  val x596_tab_dram_out = list_b1027(1)
  val x1023 = list_x1023(0)
  val x1022 = list_x1022(0)
  val b1028 = list_b1028(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1075_outr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x1075_outr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1075_outr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x1029_reg = (new x1029_reg).m.io.asInstanceOf[StandardInterface]
      val x1030_reg = (new x1030_reg).m.io.asInstanceOf[StandardInterface]
      val x1031_reg = (new x1031_reg).m.io.asInstanceOf[StandardInterface]
      val x1053_inr_UnitPipe = new x1053_inr_UnitPipe_kernel(List(b1027,x596_tab_dram_out), List(x1022), List(x1031_reg,x1029_reg,x1030_reg), List(x581_ncol) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x1053_inr_UnitPipe.sm.io.ctrDone := risingEdge(x1053_inr_UnitPipe.sm.io.ctrInc)
      x1053_inr_UnitPipe.backpressure := x1022.ready | x1053_inr_UnitPipe.sm.io.doneLatch
      x1053_inr_UnitPipe.forwardpressure := true.B | x1053_inr_UnitPipe.sm.io.doneLatch
      x1053_inr_UnitPipe.sm.io.enableOut.zip(x1053_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x1053_inr_UnitPipe.sm.io.break := false.B
      x1053_inr_UnitPipe.mask := true.B & true.B
      x1053_inr_UnitPipe.configure("x1053_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1053_inr_UnitPipe.kernel()
      val x1120_rd_x1031 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1120_rd_x1031""")
      val x1120_rd_x1031_banks = List[UInt]()
      val x1120_rd_x1031_ofs = List[UInt]()
      val x1120_rd_x1031_en = List[Bool](true.B)
      x1120_rd_x1031.toSeq.zip(x1031_reg.connectRPort(1120, x1120_rd_x1031_banks, x1120_rd_x1031_ofs, io.sigsIn.backpressure, x1120_rd_x1031_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1055_ctr = new CtrObject(Left(Some(0)), Right(x1120_rd_x1031), Left(Some(1)), 1, 32, false)
      val x1056_ctrchain = (new CChainObject(List[CtrObject](x1055_ctr), "x1056_ctrchain")).cchain.io 
      x1056_ctrchain.setup.isStream := false.B
      ModuleParams.addParams("x1056_ctrchain_p", (x1056_ctrchain.par, x1056_ctrchain.widths))
      val x1074_inr_Foreach = new x1074_inr_Foreach_kernel(List(b1027), List(x597_tab_sram_0,x1029_reg,x1030_reg), List(x1023) , Some(me), List(x1056_ctrchain), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x1074_inr_Foreach.sm.io.ctrDone := (x1074_inr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x1074_inr_Foreach.backpressure := x1023.ready | x1074_inr_Foreach.sm.io.doneLatch
      x1074_inr_Foreach.forwardpressure := true.B | x1074_inr_Foreach.sm.io.doneLatch
      x1074_inr_Foreach.sm.io.enableOut.zip(x1074_inr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x1074_inr_Foreach.sm.io.break := false.B
      x1074_inr_Foreach.mask := ~x1074_inr_Foreach.cchain.head.output.noop & true.B
      x1074_inr_Foreach.configure("x1074_inr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1074_inr_Foreach.kernel()
    }
    val module = Module(new x1075_outr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x1075_outr_UnitPipe **/
