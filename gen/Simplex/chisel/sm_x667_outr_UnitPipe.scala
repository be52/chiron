package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x667 -> x1134 -> x1137 **/
/** BEGIN None x667_outr_UnitPipe **/
class x667_outr_UnitPipe_kernel(
  list_x592_tab_dram: List[FixedPoint],
  list_x597_tab_sram_0: List[StandardInterface],
  list_x600: List[DecoupledIO[AppCommandDense]],
  list_x580_nrow: List[UInt],
  list_x602: List[DecoupledIO[AppLoadData]],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Streaming, 2, isFSM = false   , latency = 0.0.toInt, myName = "x667_outr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x667_outr_UnitPipe_iiCtr"))
  
  abstract class x667_outr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x602 = Flipped(Decoupled(new AppLoadData(ModuleParams.getParams("x602_p").asInstanceOf[(Int, Int)] )))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x600 = Decoupled(new AppCommandDense(ModuleParams.getParams("x600_p").asInstanceOf[(Int,Int)] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x592_tab_dram = Input(new FixedPoint(true, 64, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 2, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 2))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x602 = {io.in_x602} 
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x580_nrow = {io.in_x580_nrow} 
    def x600 = {io.in_x600} 
    def x581_ncol = {io.in_x581_ncol} 
    def x592_tab_dram = {io.in_x592_tab_dram} 
  }
  def connectWires0(module: x667_outr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    module.io.in_x602 <> x602
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    module.io.in_x580_nrow <> x580_nrow
    module.io.in_x600 <> x600
    module.io.in_x581_ncol <> x581_ncol
    module.io.in_x592_tab_dram <> x592_tab_dram
  }
  val x592_tab_dram = list_x592_tab_dram(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x598_tab_sram_1 = list_x597_tab_sram_0(1)
  val x600 = list_x600(0)
  val x580_nrow = list_x580_nrow(0)
  val x581_ncol = list_x580_nrow(1)
  val x602 = list_x602(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x667_outr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x667_outr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x667_outr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x601_fifo = (new x601_fifo).m.io.asInstanceOf[FIFOInterface]
      val x1082_rd_x580 = Wire(new FixedPoint(true, 32, 0))
      x1082_rd_x580.r := x580_nrow.r
      val x603_ctr = new CtrObject(Left(Some(0)), Right(x1082_rd_x580), Left(Some(1)), 1, 32, false)
      val x604_ctrchain = (new CChainObject(List[CtrObject](x603_ctr), "x604_ctrchain")).cchain.io 
      x604_ctrchain.setup.isStream := false.B
      ModuleParams.addParams("x604_ctrchain_p", (x604_ctrchain.par, x604_ctrchain.widths))
      val x627_inr_Foreach = new x627_inr_Foreach_kernel(List(x592_tab_dram), List(x601_fifo), List(x600), List(x581_ncol) , Some(me), List(x604_ctrchain), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x627_inr_Foreach.sm.io.ctrDone := (x627_inr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x627_inr_Foreach.backpressure := (~x601_fifo.full | ~(x601_fifo.accessActivesOut(0))) & x600.ready | x627_inr_Foreach.sm.io.doneLatch
      x627_inr_Foreach.forwardpressure := true.B | x627_inr_Foreach.sm.io.doneLatch
      x627_inr_Foreach.sm.io.enableOut.zip(x627_inr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x627_inr_Foreach.sm.io.break := false.B
      x627_inr_Foreach.mask := ~x627_inr_Foreach.cchain.head.output.noop & true.B
      x627_inr_Foreach.configure("x627_inr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x627_inr_Foreach.kernel()
      val x1083_rd_x580 = Wire(new FixedPoint(true, 32, 0))
      x1083_rd_x580.r := x580_nrow.r
      val x629_ctr = new CtrObject(Left(Some(0)), Right(x1083_rd_x580), Left(Some(1)), 1, 32, false)
      val x630_ctrchain = (new CChainObject(List[CtrObject](x629_ctr), "x630_ctrchain")).cchain.io 
      x630_ctrchain.setup.isStream := false.B
      ModuleParams.addParams("x630_ctrchain_p", (x630_ctrchain.par, x630_ctrchain.widths))
      val x666_outr_Foreach = new x666_outr_Foreach_kernel(List(x601_fifo), List(x602), List(x597_tab_sram_0,x598_tab_sram_1) , Some(me), List(x630_ctrchain), 1, 2, 1, List(1), List(32), breakpoints, rr)
      x666_outr_Foreach.sm.io.ctrDone := (x666_outr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x666_outr_Foreach.backpressure := true.B | x666_outr_Foreach.sm.io.doneLatch
      x666_outr_Foreach.forwardpressure := true.B | x666_outr_Foreach.sm.io.doneLatch
      x666_outr_Foreach.sm.io.enableOut.zip(x666_outr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x666_outr_Foreach.sm.io.break := false.B
      x666_outr_Foreach.mask := ~x666_outr_Foreach.cchain.head.output.noop & true.B
      x666_outr_Foreach.configure("x666_outr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x666_outr_Foreach.kernel()
    }
    val module = Module(new x667_outr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x667_outr_UnitPipe **/
