package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x697 -> x756 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x697_inr_UnitPipe **/
class x697_inr_UnitPipe_kernel(
  list_x682_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.0.toInt, myName = "x697_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x697_inr_UnitPipe_iiCtr"))
  
  abstract class x697_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x682_reg = Flipped(new StandardInterface(ModuleParams.getParams("x682_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x682_reg = {io.in_x682_reg} ; io.in_x682_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
  }
  def connectWires0(module: x697_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x682_reg.connectLedger(module.io.in_x682_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
  }
  val x682_reg = list_x682_reg(0)
  val x679_pivot_col_0 = list_x682_reg(1)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x697_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x697_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x697_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x695_rd_x682 = Wire(Bool()).suggestName("""x695_rd_x682""")
      val x695_rd_x682_banks = List[UInt]()
      val x695_rd_x682_ofs = List[UInt]()
      val x695_rd_x682_en = List[Bool](true.B)
      x695_rd_x682.toSeq.zip(x682_reg.connectRPort(695, x695_rd_x682_banks, x695_rd_x682_ofs, io.sigsIn.backpressure, x695_rd_x682_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x696_wr_x679_banks = List[UInt]()
      val x696_wr_x679_ofs = List[UInt]()
      val x696_wr_x679_en = List[Bool](true.B)
      val x696_wr_x679_data = List[UInt](0L.FP(true, 32, 0).r)
      x679_pivot_col_0.connectWPort(696, x696_wr_x679_banks, x696_wr_x679_ofs, x696_wr_x679_data, x696_wr_x679_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x695_rd_x682))
    }
    val module = Module(new x697_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x697_inr_UnitPipe **/
