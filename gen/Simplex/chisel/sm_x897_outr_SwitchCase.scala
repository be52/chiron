package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x897 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x897_outr_SwitchCase **/
class x897_outr_SwitchCase_kernel(
  list_x760_reg: List[StandardInterface],
  list_x581_ncol: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 2, isFSM = false   , latency = 0.0.toInt, myName = "x897_outr_SwitchCase_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x897_outr_SwitchCase_iiCtr"))
  
  abstract class x897_outr_SwitchCase_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x760_reg = Flipped(new StandardInterface(ModuleParams.getParams("x760_reg_p").asInstanceOf[MemParams] ))
      val in_x846_reg = Flipped(new StandardInterface(ModuleParams.getParams("x846_reg_p").asInstanceOf[MemParams] ))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x845_reg = Flipped(new StandardInterface(ModuleParams.getParams("x845_reg_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x760_reg = {io.in_x760_reg} ; io.in_x760_reg := DontCare
    def x846_reg = {io.in_x846_reg} ; io.in_x846_reg := DontCare
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x845_reg = {io.in_x845_reg} ; io.in_x845_reg := DontCare
    def x581_ncol = {io.in_x581_ncol} 
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x897_outr_SwitchCase_module)(implicit stack: List[KernelHash]): Unit = {
    x760_reg.connectLedger(module.io.in_x760_reg)
    x846_reg.connectLedger(module.io.in_x846_reg)
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    x845_reg.connectLedger(module.io.in_x845_reg)
    module.io.in_x581_ncol <> x581_ncol
    x759_reg.connectLedger(module.io.in_x759_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x760_reg = list_x760_reg(0)
  val x846_reg = list_x760_reg(1)
  val x597_tab_sram_0 = list_x760_reg(2)
  val x598_tab_sram_1 = list_x760_reg(3)
  val x679_pivot_col_0 = list_x760_reg(4)
  val x674_pivot_element_0 = list_x760_reg(5)
  val x685_reg = list_x760_reg(6)
  val x680_pivot_row_0 = list_x760_reg(7)
  val x845_reg = list_x760_reg(8)
  val x759_reg = list_x760_reg(9)
  val x684_reg = list_x760_reg(10)
  val x581_ncol = list_x581_ncol(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x897_outr_SwitchCase_obj")
    implicit val stack = ControllerStack.stack.toList
    class x897_outr_SwitchCase_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x897_outr_SwitchCase_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      // Controller Stack: Stack(x1000, x1002, x1003, x1005, x1006, x1020, x1137)
      val x871_inr_UnitPipe = new x871_inr_UnitPipe_kernel(List(x597_tab_sram_0,x679_pivot_col_0,x674_pivot_element_0,x680_pivot_row_0,x845_reg,x759_reg,x684_reg) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x871_inr_UnitPipe.sm.io.ctrDone := risingEdge(x871_inr_UnitPipe.sm.io.ctrInc)
      x871_inr_UnitPipe.backpressure := true.B | x871_inr_UnitPipe.sm.io.doneLatch
      x871_inr_UnitPipe.forwardpressure := true.B | x871_inr_UnitPipe.sm.io.doneLatch
      x871_inr_UnitPipe.sm.io.enableOut.zip(x871_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x871_inr_UnitPipe.sm.io.break := false.B
      x871_inr_UnitPipe.mask := true.B & true.B
      x871_inr_UnitPipe.configure("x871_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x871_inr_UnitPipe.kernel()
      val x1100_rd_x581 = Wire(new FixedPoint(true, 32, 0))
      x1100_rd_x581.r := x581_ncol.r
      val x873_ctr = new CtrObject(Left(Some(0)), Right(x1100_rd_x581), Left(Some(1)), 1, 32, false)
      val x874_ctrchain = (new CChainObject(List[CtrObject](x873_ctr), "x874_ctrchain")).cchain.io 
      x874_ctrchain.setup.isStream := false.B
      ModuleParams.addParams("x874_ctrchain_p", (x874_ctrchain.par, x874_ctrchain.widths))
      val x1101_rd_x684 = Wire(Bool()).suggestName("""x1101_rd_x684""")
      val x1101_rd_x684_banks = List[UInt]()
      val x1101_rd_x684_ofs = List[UInt]()
      val x1101_rd_x684_en = List[Bool](true.B)
      x1101_rd_x684.toSeq.zip(x684_reg.connectRPort(1101, x1101_rd_x684_banks, x1101_rd_x684_ofs, io.sigsIn.backpressure, x1101_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1102_rd_x760 = Wire(Bool()).suggestName("""x1102_rd_x760""")
      val x1102_rd_x760_banks = List[UInt]()
      val x1102_rd_x760_ofs = List[UInt]()
      val x1102_rd_x760_en = List[Bool](true.B)
      x1102_rd_x760.toSeq.zip(x760_reg.connectRPort(1102, x1102_rd_x760_banks, x1102_rd_x760_ofs, io.sigsIn.backpressure, x1102_rd_x760_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1103_rd_x845 = Wire(Bool()).suggestName("""x1103_rd_x845""")
      val x1103_rd_x845_banks = List[UInt]()
      val x1103_rd_x845_ofs = List[UInt]()
      val x1103_rd_x845_en = List[Bool](true.B)
      x1103_rd_x845.toSeq.zip(x845_reg.connectRPort(1103, x1103_rd_x845_banks, x1103_rd_x845_ofs, io.sigsIn.backpressure, x1103_rd_x845_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x895_inr_Foreach = new x895_inr_Foreach_kernel(List(x1103_rd_x845,x1102_rd_x760,x1101_rd_x684), List(x846_reg,x597_tab_sram_0,x598_tab_sram_1,x674_pivot_element_0,x685_reg,x680_pivot_row_0,x759_reg) , Some(me), List(x874_ctrchain), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x895_inr_Foreach.sm.io.ctrDone := (x895_inr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x895_inr_Foreach.backpressure := true.B | x895_inr_Foreach.sm.io.doneLatch
      x895_inr_Foreach.forwardpressure := true.B | x895_inr_Foreach.sm.io.doneLatch
      x895_inr_Foreach.sm.io.enableOut.zip(x895_inr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x895_inr_Foreach.sm.io.break := false.B
      x895_inr_Foreach.mask := ~x895_inr_Foreach.cchain.head.output.noop & x1101_rd_x684 & x1102_rd_x760 & x1103_rd_x845
      x895_inr_Foreach.configure("x895_inr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x895_inr_Foreach.kernel()
    }
    val module = Module(new x897_outr_SwitchCase_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END SwitchCase x897_outr_SwitchCase **/
