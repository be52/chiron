package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x985 -> x1135 -> x1136 -> x997 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x985_outr_Foreach **/
class x985_outr_Foreach_kernel(
  list_x1110_rd_x898: List[Bool],
  list_x760_reg: List[StandardInterface],
  list_x581_ncol: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Pipelined, 2, isFSM = false   , latency = 0.0.toInt, myName = "x985_outr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x985_outr_Foreach_iiCtr"))
  
  abstract class x985_outr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x760_reg = Flipped(new StandardInterface(ModuleParams.getParams("x760_reg_p").asInstanceOf[MemParams] ))
      val in_x1110_rd_x898 = Input(Bool())
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x898_reg = Flipped(new StandardInterface(ModuleParams.getParams("x898_reg_p").asInstanceOf[MemParams] ))
      val in_x1109_rd_x847 = Input(Bool())
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x847_reg = Flipped(new StandardInterface(ModuleParams.getParams("x847_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x1107_rd_x684 = Input(Bool())
      val in_x848_reg = Flipped(new StandardInterface(ModuleParams.getParams("x848_reg_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x1108_rd_x759 = Input(Bool())
      val in_x899_reg = Flipped(new StandardInterface(ModuleParams.getParams("x899_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x760_reg = {io.in_x760_reg} ; io.in_x760_reg := DontCare
    def x1110_rd_x898 = {io.in_x1110_rd_x898} 
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x898_reg = {io.in_x898_reg} ; io.in_x898_reg := DontCare
    def x1109_rd_x847 = {io.in_x1109_rd_x847} 
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x847_reg = {io.in_x847_reg} ; io.in_x847_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x1107_rd_x684 = {io.in_x1107_rd_x684} 
    def x848_reg = {io.in_x848_reg} ; io.in_x848_reg := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x581_ncol = {io.in_x581_ncol} 
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x1108_rd_x759 = {io.in_x1108_rd_x759} 
    def x899_reg = {io.in_x899_reg} ; io.in_x899_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x985_outr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    x760_reg.connectLedger(module.io.in_x760_reg)
    module.io.in_x1110_rd_x898 <> x1110_rd_x898
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x898_reg.connectLedger(module.io.in_x898_reg)
    module.io.in_x1109_rd_x847 <> x1109_rd_x847
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x847_reg.connectLedger(module.io.in_x847_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    module.io.in_x1107_rd_x684 <> x1107_rd_x684
    x848_reg.connectLedger(module.io.in_x848_reg)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    module.io.in_x581_ncol <> x581_ncol
    x759_reg.connectLedger(module.io.in_x759_reg)
    module.io.in_x1108_rd_x759 <> x1108_rd_x759
    x899_reg.connectLedger(module.io.in_x899_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x1110_rd_x898 = list_x1110_rd_x898(0)
  val x1109_rd_x847 = list_x1110_rd_x898(1)
  val x1107_rd_x684 = list_x1110_rd_x898(2)
  val x1108_rd_x759 = list_x1110_rd_x898(3)
  val x760_reg = list_x760_reg(0)
  val x597_tab_sram_0 = list_x760_reg(1)
  val x898_reg = list_x760_reg(2)
  val x598_tab_sram_1 = list_x760_reg(3)
  val x847_reg = list_x760_reg(4)
  val x679_pivot_col_0 = list_x760_reg(5)
  val x674_pivot_element_0 = list_x760_reg(6)
  val x848_reg = list_x760_reg(7)
  val x685_reg = list_x760_reg(8)
  val x680_pivot_row_0 = list_x760_reg(9)
  val x759_reg = list_x760_reg(10)
  val x899_reg = list_x760_reg(11)
  val x684_reg = list_x760_reg(12)
  val x581_ncol = list_x581_ncol(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x985_outr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x985_outr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x985_outr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b916 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b916.suggestName("b916")
      val b916_chain = Module(new RegChainPass(2, 32, myName = "b916_chain")); b916_chain.io <> DontCare
      b916_chain.chain_pass(b916, io.sigsOut.smDoneIn.head)
      val b916_chain_read_1 = b916_chain.read(1).FP(true,32,0)
      val b917 = ~io.sigsIn.cchainOutputs.head.oobs(0); b917.suggestName("b917")
      val b917_chain = Module(new RegChainPass(2, 1, myName = "b917_chain")); b917_chain.io <> DontCare
      b917_chain.chain_pass(b917, io.sigsOut.smDoneIn.head)
      val b917_chain_read_1: Bool = b917_chain.read(1).apply(0)
      val x918_reg = (new x918_reg).m.io.asInstanceOf[NBufInterface]
      val x919_reg = (new x919_reg).m.io.asInstanceOf[NBufInterface]
      val x920_reg = (new x920_reg).m.io.asInstanceOf[NBufInterface]
      val x927_inr_UnitPipe = new x927_inr_UnitPipe_kernel(List(b917), List(b916), List(x680_pivot_row_0), List(x920_reg,x919_reg,x918_reg) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x927_inr_UnitPipe.sm.io.ctrDone := risingEdge(x927_inr_UnitPipe.sm.io.ctrInc)
      b916_chain.connectStageCtrl((x927_inr_UnitPipe.done).DS(1.toInt, rr, x927_inr_UnitPipe.sm.io.backpressure), x927_inr_UnitPipe.baseEn, 0)
      b917_chain.connectStageCtrl((x927_inr_UnitPipe.done).DS(1.toInt, rr, x927_inr_UnitPipe.sm.io.backpressure), x927_inr_UnitPipe.baseEn, 0)
      x927_inr_UnitPipe.backpressure := true.B | x927_inr_UnitPipe.sm.io.doneLatch
      x927_inr_UnitPipe.forwardpressure := true.B | x927_inr_UnitPipe.sm.io.doneLatch
      x927_inr_UnitPipe.sm.io.enableOut.zip(x927_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x927_inr_UnitPipe.sm.io.break := false.B
      x927_inr_UnitPipe.mask := true.B & b917
      x927_inr_UnitPipe.configure("x927_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x927_inr_UnitPipe.kernel()
      val x1111_rd_x918 = Wire(Bool()).suggestName("""x1111_rd_x918""")
      val x1111_rd_x918_banks = List[UInt]()
      val x1111_rd_x918_ofs = List[UInt]()
      val x1111_rd_x918_en = List[Bool](true.B)
      x1111_rd_x918.toSeq.zip(x918_reg.connectRPort(1111, x1111_rd_x918_banks, x1111_rd_x918_ofs, io.sigsIn.backpressure, x1111_rd_x918_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1112_rd_x920 = Wire(Bool()).suggestName("""x1112_rd_x920""")
      val x1112_rd_x920_banks = List[UInt]()
      val x1112_rd_x920_ofs = List[UInt]()
      val x1112_rd_x920_en = List[Bool](true.B)
      x1112_rd_x920.toSeq.zip(x920_reg.connectRPort(1112, x1112_rd_x920_banks, x1112_rd_x920_ofs, io.sigsIn.backpressure, x1112_rd_x920_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x984_outr_Switch_obj = new x984_outr_Switch_kernel(List(b916_chain_read_1), List(x920_reg,x919_reg,x918_reg), List(x581_ncol), List(x760_reg,x597_tab_sram_0,x898_reg,x598_tab_sram_1,x847_reg,x679_pivot_col_0,x674_pivot_element_0,x848_reg,x685_reg,x680_pivot_row_0,x759_reg,x899_reg,x684_reg), List(x1111_rd_x918,x1112_rd_x920) , Some(me), List(), 1, 2, 1, List(1), List(32), breakpoints, rr)
      val x982_outr_SwitchCase_switch_sel_reg = RegInit(false.B)
      x982_outr_SwitchCase_switch_sel_reg := Mux(risingEdge(x984_outr_Switch_obj.en), x1111_rd_x918, x982_outr_SwitchCase_switch_sel_reg)
      x984_outr_Switch_obj.sm.io.selectsIn(0) := x1111_rd_x918
      val x983_inr_SwitchCase_switch_sel_reg = RegInit(false.B)
      x983_inr_SwitchCase_switch_sel_reg := Mux(risingEdge(x984_outr_Switch_obj.en), x1112_rd_x920, x983_inr_SwitchCase_switch_sel_reg)
      x984_outr_Switch_obj.sm.io.selectsIn(1) := x1112_rd_x920
      b916_chain.connectStageCtrl((x984_outr_Switch_obj.done).DS(1.toInt, rr, x984_outr_Switch_obj.sm.io.backpressure), x984_outr_Switch_obj.baseEn, 1)
      b917_chain.connectStageCtrl((x984_outr_Switch_obj.done).DS(1.toInt, rr, x984_outr_Switch_obj.sm.io.backpressure), x984_outr_Switch_obj.baseEn, 1)
      x984_outr_Switch_obj.backpressure := true.B | x984_outr_Switch_obj.sm.io.doneLatch
      x984_outr_Switch_obj.forwardpressure := true.B | x984_outr_Switch_obj.sm.io.doneLatch
      x984_outr_Switch_obj.sm.io.enableOut.zip(x984_outr_Switch_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x984_outr_Switch_obj.sm.io.break := false.B
      x984_outr_Switch_obj.mask := true.B & true.B
      x984_outr_Switch_obj.configure("x984_outr_Switch_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x984_outr_Switch_obj.kernel()
    }
    val module = Module(new x985_outr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x985_outr_Foreach **/
