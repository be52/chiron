package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x824 -> x844 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x824_inr_Foreach **/
class x824_inr_Foreach_kernel(
  list_x1095_rd_x757: List[Bool],
  list_x597_tab_sram_0: List[StandardInterface],
  list_x581_ncol: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Pipelined, false   , latency = 39.0.toInt, myName = "x824_inr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(2.0.toInt, 2 + _root_.utils.math.log2Up(2.0.toInt), "x824_inr_Foreach_iiCtr"))
  
  abstract class x824_inr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x1095_rd_x757 = Input(Bool())
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x757_reg = Flipped(new StandardInterface(ModuleParams.getParams("x757_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x758_reg = Flipped(new StandardInterface(ModuleParams.getParams("x758_reg_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x675_pivot_row_min_ratio_0 = Flipped(new StandardInterface(ModuleParams.getParams("x675_pivot_row_min_ratio_0_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x673_no_pivot_row_count_0 = Flipped(new StandardInterface(ModuleParams.getParams("x673_no_pivot_row_count_0_p").asInstanceOf[MemParams] ))
      val in_x1094_rd_x684 = Input(Bool())
      val in_x581_ncol = Input(UInt(64.W))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x1095_rd_x757 = {io.in_x1095_rd_x757} 
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x757_reg = {io.in_x757_reg} ; io.in_x757_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x758_reg = {io.in_x758_reg} ; io.in_x758_reg := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x675_pivot_row_min_ratio_0 = {io.in_x675_pivot_row_min_ratio_0} ; io.in_x675_pivot_row_min_ratio_0 := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x673_no_pivot_row_count_0 = {io.in_x673_no_pivot_row_count_0} ; io.in_x673_no_pivot_row_count_0 := DontCare
    def x1094_rd_x684 = {io.in_x1094_rd_x684} 
    def x581_ncol = {io.in_x581_ncol} 
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x824_inr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    module.io.in_x1095_rd_x757 <> x1095_rd_x757
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x757_reg.connectLedger(module.io.in_x757_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x758_reg.connectLedger(module.io.in_x758_reg)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x675_pivot_row_min_ratio_0.connectLedger(module.io.in_x675_pivot_row_min_ratio_0)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    x673_no_pivot_row_count_0.connectLedger(module.io.in_x673_no_pivot_row_count_0)
    module.io.in_x1094_rd_x684 <> x1094_rd_x684
    module.io.in_x581_ncol <> x581_ncol
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x1095_rd_x757 = list_x1095_rd_x757(0)
  val x1094_rd_x684 = list_x1095_rd_x757(1)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x598_tab_sram_1 = list_x597_tab_sram_0(1)
  val x757_reg = list_x597_tab_sram_0(2)
  val x679_pivot_col_0 = list_x597_tab_sram_0(3)
  val x674_pivot_element_0 = list_x597_tab_sram_0(4)
  val x758_reg = list_x597_tab_sram_0(5)
  val x685_reg = list_x597_tab_sram_0(6)
  val x675_pivot_row_min_ratio_0 = list_x597_tab_sram_0(7)
  val x680_pivot_row_0 = list_x597_tab_sram_0(8)
  val x673_no_pivot_row_count_0 = list_x597_tab_sram_0(9)
  val x684_reg = list_x597_tab_sram_0(10)
  val x581_ncol = list_x581_ncol(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x824_inr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x824_inr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x824_inr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b780 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b780.suggestName("b780")
      val b781 = ~io.sigsIn.cchainOutputs.head.oobs(0); b781.suggestName("b781")
      val x782_rd_x581 = Wire(new FixedPoint(true, 32, 0))
      x782_rd_x581.r := x581_ncol.r
      val x783_sub = Wire(new FixedPoint(true, 32, 0)).suggestName("""x783_sub""")
      x783_sub.r := Math.sub(x782_rd_x581,1L.FP(true, 32, 0),Some(1.0), true.B, Truncate, Wrapping, "x783_sub").r
      val x784_rd_x758 = Wire(Bool()).suggestName("""x784_rd_x758""")
      val x784_rd_x758_banks = List[UInt]()
      val x784_rd_x758_ofs = List[UInt]()
      val x784_rd_x758_en = List[Bool](true.B)
      x784_rd_x758.toSeq.zip(x758_reg.connectRPort(784, x784_rd_x758_banks, x784_rd_x758_ofs, io.sigsIn.backpressure, x784_rd_x758_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x785_rd_x685 = Wire(Bool()).suggestName("""x785_rd_x685""")
      val x785_rd_x685_banks = List[UInt]()
      val x785_rd_x685_ofs = List[UInt]()
      val x785_rd_x685_en = List[Bool](true.B)
      x785_rd_x685.toSeq.zip(x685_reg.connectRPort(785, x785_rd_x685_banks, x785_rd_x685_ofs, io.sigsIn.backpressure, x785_rd_x685_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x788_mul = Wire(new FixedPoint(true, 32, 0)).suggestName("""x788_mul""")
      x788_mul.r := (Math.mul(b780, 10L.FP(true, 32, 0), Some(6.0), true.B, Truncate, Wrapping, "x788_mul")).r
      val x1159 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1159_x783_sub_D5") 
      x1159.r := getRetimed(x783_sub.r, 5.toInt, io.sigsIn.backpressure)
      val x789_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x789_sum""")
      x789_sum.r := Math.add(x788_mul,x1159,Some(1.0), true.B, Truncate, Wrapping, "x789_sum").r
      val x1160 = Wire(Bool()).suggestName("x1160_b781_D7") 
      x1160.r := getRetimed(b781.r, 7.toInt, io.sigsIn.backpressure)
      val x1161 = Wire(Bool()).suggestName("x1161_x784_rd_x758_D7") 
      x1161.r := getRetimed(x784_rd_x758.r, 7.toInt, io.sigsIn.backpressure)
      val x1162 = Wire(Bool()).suggestName("x1162_x785_rd_x685_D7") 
      x1162.r := getRetimed(x785_rd_x685.r, 7.toInt, io.sigsIn.backpressure)
      val x791_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x791_rd""")
      val x791_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x791_rd_ofs = List[UInt](x789_sum.r)
      val x791_rd_en = List[Bool](true.B)
      x791_rd.toSeq.zip(x598_tab_sram_1.connectRPort(791, x791_rd_banks, x791_rd_ofs, io.sigsIn.backpressure, x791_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(7.0.toInt, rr, io.sigsIn.backpressure) && x1162 & x1161 & x1160), true)).foreach{case (a,b) => a := b}
      val x792_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x792_elem_0""")
      x792_elem_0.r := x791_rd(0).r
      val x793_rd_x679 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x793_rd_x679""")
      val x793_rd_x679_banks = List[UInt]()
      val x793_rd_x679_ofs = List[UInt]()
      val x793_rd_x679_en = List[Bool](true.B)
      x793_rd_x679.toSeq.zip(x679_pivot_col_0.connectRPort(793, x793_rd_x679_banks, x793_rd_x679_ofs, io.sigsIn.backpressure, x793_rd_x679_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1163 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1163_x793_rd_x679_D6") 
      x1163.r := getRetimed(x793_rd_x679.r, 6.toInt, io.sigsIn.backpressure)
      val x796_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x796_sum""")
      x796_sum.r := Math.add(x788_mul,x1163,Some(1.0), true.B, Truncate, Wrapping, "x796_sum").r
      val x798_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x798_rd""")
      val x798_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x798_rd_ofs = List[UInt](x796_sum.r)
      val x798_rd_en = List[Bool](true.B)
      x798_rd.toSeq.zip(x597_tab_sram_0.connectRPort(798, x798_rd_banks, x798_rd_ofs, io.sigsIn.backpressure, x798_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(7.0.toInt, rr, io.sigsIn.backpressure) && x1162 & x1161 & x1160), true)).foreach{case (a,b) => a := b}
      val x799_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x799_elem_0""")
      x799_elem_0.r := x798_rd(0).r
      val x800 = Wire(new FloatingPoint(24, 8)).suggestName("""x800""")
      x800.r := Math.fdiv(x792_elem_0, x799_elem_0, Some(28.0), true.B,"x800").r
      val x1164 = Wire(Bool()).suggestName("x1164_b781_D37") 
      x1164.r := getRetimed(b781.r, 37.toInt, io.sigsIn.backpressure)
      val x1165 = Wire(Bool()).suggestName("x1165_x784_rd_x758_D37") 
      x1165.r := getRetimed(x784_rd_x758.r, 37.toInt, io.sigsIn.backpressure)
      val x1166 = Wire(Bool()).suggestName("x1166_x785_rd_x685_D37") 
      x1166.r := getRetimed(x785_rd_x685.r, 37.toInt, io.sigsIn.backpressure)
      val x801_wr_x674_banks = List[UInt]()
      val x801_wr_x674_ofs = List[UInt]()
      val x801_wr_x674_en = List[Bool](true.B)
      val x801_wr_x674_data = List[UInt](x800.r)
      x674_pivot_element_0.connectWPort(801, x801_wr_x674_banks, x801_wr_x674_ofs, x801_wr_x674_data, x801_wr_x674_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(37.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1166 & x1165 & x1164))
      val x802 = Wire(Bool()).suggestName("""x802""")
      x802.r := Math.flt(0.0.FlP(24, 8), x799_elem_0, Some(0.0), true.B,"x802").r
      val x803 = Wire(Bool()).suggestName("""x803""")
      x803 := ~x802
      val x804_rd_x674 = Wire(new FloatingPoint(24, 8)).suggestName("""x804_rd_x674""")
      val x804_rd_x674_banks = List[UInt]()
      val x804_rd_x674_ofs = List[UInt]()
      val x804_rd_x674_en = List[Bool](true.B)
      x804_rd_x674.toSeq.zip(x674_pivot_element_0.connectRPort(804, x804_rd_x674_banks, x804_rd_x674_ofs, io.sigsIn.backpressure, x804_rd_x674_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(38.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x805_rd_x675 = Wire(new FloatingPoint(24, 8)).suggestName("""x805_rd_x675""")
      val x805_rd_x675_banks = List[UInt]()
      val x805_rd_x675_ofs = List[UInt]()
      val x805_rd_x675_en = List[Bool](true.B)
      x805_rd_x675.toSeq.zip(x675_pivot_row_min_ratio_0.connectRPort(805, x805_rd_x675_banks, x805_rd_x675_ofs, io.sigsIn.backpressure, x805_rd_x675_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(38.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x806 = Wire(Bool()).suggestName("""x806""")
      x806.r := Math.flt(x804_rd_x674, x805_rd_x675, Some(0.0), true.B,"x806").r
      val x808_rd_x757 = Wire(Bool()).suggestName("""x808_rd_x757""")
      val x808_rd_x757_banks = List[UInt]()
      val x808_rd_x757_ofs = List[UInt]()
      val x808_rd_x757_en = List[Bool](true.B)
      x808_rd_x757.toSeq.zip(x757_reg.connectRPort(808, x808_rd_x757_banks, x808_rd_x757_ofs, io.sigsIn.backpressure, x808_rd_x757_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x809_rd_x684 = Wire(Bool()).suggestName("""x809_rd_x684""")
      val x809_rd_x684_banks = List[UInt]()
      val x809_rd_x684_ofs = List[UInt]()
      val x809_rd_x684_en = List[Bool](true.B)
      x809_rd_x684.toSeq.zip(x684_reg.connectRPort(809, x809_rd_x684_banks, x809_rd_x684_ofs, io.sigsIn.backpressure, x809_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1167 = Wire(Bool()).suggestName("x1167_x808_rd_x757_D38") 
      x1167.r := getRetimed(x808_rd_x757.r, 38.toInt, io.sigsIn.backpressure)
      val x1168 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1168_b780_D38") 
      x1168.r := getRetimed(b780.r, 38.toInt, io.sigsIn.backpressure)
      val x1169 = Wire(Bool()).suggestName("x1169_x802_D29") 
      x1169.r := getRetimed(x802.r, 29.toInt, io.sigsIn.backpressure)
      val x1170 = Wire(Bool()).suggestName("x1170_x809_rd_x684_D38") 
      x1170.r := getRetimed(x809_rd_x684.r, 38.toInt, io.sigsIn.backpressure)
      val x810_wr_x680_banks = List[UInt]()
      val x810_wr_x680_ofs = List[UInt]()
      val x810_wr_x680_en = List[Bool](true.B)
      val x810_wr_x680_data = List[UInt](x1168.r)
      x680_pivot_row_0.connectWPort(810, x810_wr_x680_banks, x810_wr_x680_ofs, x810_wr_x680_data, x810_wr_x680_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(38.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1170 & x1167 & x1169 & x806))
      val x811_rd_x674 = Wire(new FloatingPoint(24, 8)).suggestName("""x811_rd_x674""")
      val x811_rd_x674_banks = List[UInt]()
      val x811_rd_x674_ofs = List[UInt]()
      val x811_rd_x674_en = List[Bool](true.B)
      x811_rd_x674.toSeq.zip(x674_pivot_element_0.connectRPort(811, x811_rd_x674_banks, x811_rd_x674_ofs, io.sigsIn.backpressure, x811_rd_x674_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(38.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x812_wr_x675_banks = List[UInt]()
      val x812_wr_x675_ofs = List[UInt]()
      val x812_wr_x675_en = List[Bool](true.B)
      val x812_wr_x675_data = List[UInt](x811_rd_x674.r)
      x675_pivot_row_min_ratio_0.connectWPort(812, x812_wr_x675_banks, x812_wr_x675_ofs, x812_wr_x675_data, x812_wr_x675_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(38.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1170 & x1167 & x1169 & x806))
      val x817_rd_x673 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x817_rd_x673""")
      val x817_rd_x673_banks = List[UInt]()
      val x817_rd_x673_ofs = List[UInt]()
      val x817_rd_x673_en = List[Bool](true.B)
      x817_rd_x673.toSeq.zip(x673_no_pivot_row_count_0.connectRPort(817, x817_rd_x673_banks, x817_rd_x673_ofs, io.sigsIn.backpressure, x817_rd_x673_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(8.2.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x818_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x818_sum""")
      x818_sum.r := Math.add(x817_rd_x673,1L.FP(true, 32, 0),Some(1.0), true.B, Truncate, Wrapping, "x818_sum").r
      val x819_rd_x757 = Wire(Bool()).suggestName("""x819_rd_x757""")
      val x819_rd_x757_banks = List[UInt]()
      val x819_rd_x757_ofs = List[UInt]()
      val x819_rd_x757_en = List[Bool](true.B)
      x819_rd_x757.toSeq.zip(x757_reg.connectRPort(819, x819_rd_x757_banks, x819_rd_x757_ofs, io.sigsIn.backpressure, x819_rd_x757_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x820_rd_x684 = Wire(Bool()).suggestName("""x820_rd_x684""")
      val x820_rd_x684_banks = List[UInt]()
      val x820_rd_x684_ofs = List[UInt]()
      val x820_rd_x684_en = List[Bool](true.B)
      x820_rd_x684.toSeq.zip(x684_reg.connectRPort(820, x820_rd_x684_banks, x820_rd_x684_ofs, io.sigsIn.backpressure, x820_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1171 = Wire(Bool()).suggestName("x1171_x819_rd_x757_D9") 
      x1171.r := getRetimed(x819_rd_x757.r, 9.toInt, io.sigsIn.backpressure)
      val x1172 = Wire(Bool()).suggestName("x1172_x820_rd_x684_D9") 
      x1172.r := getRetimed(x820_rd_x684.r, 9.toInt, io.sigsIn.backpressure)
      val x821_wr_x673_banks = List[UInt]()
      val x821_wr_x673_ofs = List[UInt]()
      val x821_wr_x673_en = List[Bool](true.B)
      val x821_wr_x673_data = List[UInt](x818_sum.r)
      x673_no_pivot_row_count_0.connectWPort(821, x821_wr_x673_banks, x821_wr_x673_ofs, x821_wr_x673_data, x821_wr_x673_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(9.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1172 & x1171 & x803))
    }
    val module = Module(new x824_inr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x824_inr_Foreach **/
