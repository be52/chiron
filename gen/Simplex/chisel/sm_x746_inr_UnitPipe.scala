package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x746 -> x756 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x746_inr_UnitPipe **/
class x746_inr_UnitPipe_kernel(
  list_x730_reg: List[StandardInterface],
  list_x580_nrow: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 12.2.toInt, myName = "x746_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x746_inr_UnitPipe_iiCtr"))
  
  abstract class x746_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x730_reg = Flipped(new StandardInterface(ModuleParams.getParams("x730_reg_p").asInstanceOf[MemParams] ))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x729_reg = Flipped(new StandardInterface(ModuleParams.getParams("x729_reg_p").asInstanceOf[MemParams] ))
      val in_x682_reg = Flipped(new StandardInterface(ModuleParams.getParams("x682_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x730_reg = {io.in_x730_reg} ; io.in_x730_reg := DontCare
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x729_reg = {io.in_x729_reg} ; io.in_x729_reg := DontCare
    def x682_reg = {io.in_x682_reg} ; io.in_x682_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x580_nrow = {io.in_x580_nrow} 
  }
  def connectWires0(module: x746_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x730_reg.connectLedger(module.io.in_x730_reg)
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x729_reg.connectLedger(module.io.in_x729_reg)
    x682_reg.connectLedger(module.io.in_x682_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    module.io.in_x580_nrow <> x580_nrow
  }
  val x730_reg = list_x730_reg(0)
  val x597_tab_sram_0 = list_x730_reg(1)
  val x729_reg = list_x730_reg(2)
  val x682_reg = list_x730_reg(3)
  val x679_pivot_col_0 = list_x730_reg(4)
  val x580_nrow = list_x580_nrow(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x746_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x746_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x746_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x731_rd_x580 = Wire(new FixedPoint(true, 32, 0))
      x731_rd_x580.r := x580_nrow.r
      val x732_sub = Wire(new FixedPoint(true, 32, 0)).suggestName("""x732_sub""")
      x732_sub.r := Math.sub(x731_rd_x580,1L.FP(true, 32, 0),Some(1.0), true.B, Truncate, Wrapping, "x732_sub").r
      val x733_rd_x679 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x733_rd_x679""")
      val x733_rd_x679_banks = List[UInt]()
      val x733_rd_x679_ofs = List[UInt]()
      val x733_rd_x679_en = List[Bool](true.B)
      x733_rd_x679.toSeq.zip(x679_pivot_col_0.connectRPort(733, x733_rd_x679_banks, x733_rd_x679_ofs, io.sigsIn.backpressure, x733_rd_x679_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x734_rd_x682 = Wire(Bool()).suggestName("""x734_rd_x682""")
      val x734_rd_x682_banks = List[UInt]()
      val x734_rd_x682_ofs = List[UInt]()
      val x734_rd_x682_en = List[Bool](true.B)
      x734_rd_x682.toSeq.zip(x682_reg.connectRPort(734, x734_rd_x682_banks, x734_rd_x682_ofs, io.sigsIn.backpressure, x734_rd_x682_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1157 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1157_x733_rd_x679_D1") 
      x1157.r := getRetimed(x733_rd_x679.r, 1.toInt, io.sigsIn.backpressure)
      val x1124 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1124""")
      x1124.r := Math.fma(x732_sub,10L.FP(true, 32, 0),x1157,Some(6.0), true.B, "x1124").toFixed(x1124, "cast_x1124").r
      val x1158 = Wire(Bool()).suggestName("x1158_x734_rd_x682_D7") 
      x1158.r := getRetimed(x734_rd_x682.r, 7.toInt, io.sigsIn.backpressure)
      val x740_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x740_rd""")
      val x740_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x740_rd_ofs = List[UInt](x1124.r)
      val x740_rd_en = List[Bool](true.B)
      x740_rd.toSeq.zip(x597_tab_sram_0.connectRPort(740, x740_rd_banks, x740_rd_ofs, io.sigsIn.backpressure, x740_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(7.0.toInt, rr, io.sigsIn.backpressure) && x1158), true)).foreach{case (a,b) => a := b}
      val x741_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x741_elem_0""")
      x741_elem_0.r := x740_rd(0).r
      val x742 = Wire(Bool()).suggestName("""x742""")
      x742.r := Math.flte(0.0.FlP(24, 8), x741_elem_0, Some(2.0), true.B,"x742").r
      val x743 = Wire(Bool()).suggestName("""x743""")
      x743 := ~x742
      val x744_wr_x729_banks = List[UInt]()
      val x744_wr_x729_ofs = List[UInt]()
      val x744_wr_x729_en = List[Bool](true.B)
      val x744_wr_x729_data = List[UInt](x742.r)
      x729_reg.connectWPort(744, x744_wr_x729_banks, x744_wr_x729_ofs, x744_wr_x729_data, x744_wr_x729_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(11.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x745_wr_x730_banks = List[UInt]()
      val x745_wr_x730_ofs = List[UInt]()
      val x745_wr_x730_en = List[Bool](true.B)
      val x745_wr_x730_data = List[UInt](x743.r)
      x730_reg.connectWPort(745, x745_wr_x730_banks, x745_wr_x730_ofs, x745_wr_x730_data, x745_wr_x730_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(11.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x746_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x746_inr_UnitPipe **/
