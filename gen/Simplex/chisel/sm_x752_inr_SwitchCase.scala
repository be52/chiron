package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x752 -> x754 -> x756 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x752_inr_SwitchCase **/
class x752_inr_SwitchCase_kernel(
  list_x729_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.0.toInt, myName = "x752_inr_SwitchCase_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x752_inr_SwitchCase_iiCtr"))
  
  abstract class x752_inr_SwitchCase_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x729_reg = Flipped(new StandardInterface(ModuleParams.getParams("x729_reg_p").asInstanceOf[MemParams] ))
      val in_x682_reg = Flipped(new StandardInterface(ModuleParams.getParams("x682_reg_p").asInstanceOf[MemParams] ))
      val in_x671_no_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x671_no_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x729_reg = {io.in_x729_reg} ; io.in_x729_reg := DontCare
    def x682_reg = {io.in_x682_reg} ; io.in_x682_reg := DontCare
    def x671_no_pivot_col_0 = {io.in_x671_no_pivot_col_0} ; io.in_x671_no_pivot_col_0 := DontCare
  }
  def connectWires0(module: x752_inr_SwitchCase_module)(implicit stack: List[KernelHash]): Unit = {
    x729_reg.connectLedger(module.io.in_x729_reg)
    x682_reg.connectLedger(module.io.in_x682_reg)
    x671_no_pivot_col_0.connectLedger(module.io.in_x671_no_pivot_col_0)
  }
  val x729_reg = list_x729_reg(0)
  val x682_reg = list_x729_reg(1)
  val x671_no_pivot_col_0 = list_x729_reg(2)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x752_inr_SwitchCase_obj")
    implicit val stack = ControllerStack.stack.toList
    class x752_inr_SwitchCase_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x752_inr_SwitchCase_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      // Controller Stack: Stack(x754, x756, x1006, x1020, x1137)
      val x749_rd_x729 = Wire(Bool()).suggestName("""x749_rd_x729""")
      val x749_rd_x729_banks = List[UInt]()
      val x749_rd_x729_ofs = List[UInt]()
      val x749_rd_x729_en = List[Bool](true.B)
      x749_rd_x729.toSeq.zip(x729_reg.connectRPort(749, x749_rd_x729_banks, x749_rd_x729_ofs, io.sigsIn.backpressure, x749_rd_x729_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x750_rd_x682 = Wire(Bool()).suggestName("""x750_rd_x682""")
      val x750_rd_x682_banks = List[UInt]()
      val x750_rd_x682_ofs = List[UInt]()
      val x750_rd_x682_en = List[Bool](true.B)
      x750_rd_x682.toSeq.zip(x682_reg.connectRPort(750, x750_rd_x682_banks, x750_rd_x682_ofs, io.sigsIn.backpressure, x750_rd_x682_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x751_wr_x671_banks = List[UInt]()
      val x751_wr_x671_ofs = List[UInt]()
      val x751_wr_x671_en = List[Bool](true.B)
      val x751_wr_x671_data = List[UInt](true.B.r)
      x671_no_pivot_col_0.connectWPort(751, x751_wr_x671_banks, x751_wr_x671_ofs, x751_wr_x671_data, x751_wr_x671_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x750_rd_x682 & x749_rd_x729))
    }
    val module = Module(new x752_inr_SwitchCase_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END SwitchCase x752_inr_SwitchCase **/
