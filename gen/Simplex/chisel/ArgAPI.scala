package accel
object api {
  
// ArgIns
  val NCONS_arg = 2
  val NCOL_arg = 1
  val NROW_arg = 0
  val NCOEFS_arg = 3
  
// DRAM Ptrs:
  val TAB_DRAM_OUT_ptr = 5
  val TAB_DRAM_ptr = 4
  
// ArgIOs
  
// ArgOuts
  
// Instrumentation Counters
  val numArgOuts_breakpts = 1
}
