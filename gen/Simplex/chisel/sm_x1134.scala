package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1134 -> x1137 **/
/** BEGIN None x1134 **/
class x1134_kernel(
  list_x592_tab_dram: List[FixedPoint],
  list_x597_tab_sram_0: List[StandardInterface],
  list_x600: List[DecoupledIO[AppCommandDense]],
  list_x580_nrow: List[UInt],
  list_x602: List[DecoupledIO[AppLoadData]],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(ForkJoin, 3, isFSM = false   , latency = 0.0.toInt, myName = "x1134_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1134_iiCtr"))
  
  abstract class x1134_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x602 = Flipped(Decoupled(new AppLoadData(ModuleParams.getParams("x602_p").asInstanceOf[(Int, Int)] )))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x672_no_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x672_no_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x668_iter_0 = Flipped(new StandardInterface(ModuleParams.getParams("x668_iter_0_p").asInstanceOf[MemParams] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x600 = Decoupled(new AppCommandDense(ModuleParams.getParams("x600_p").asInstanceOf[(Int,Int)] ))
      val in_x671_no_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x671_no_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x592_tab_dram = Input(new FixedPoint(true, 64, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(3, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(3, 1))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x602 = {io.in_x602} 
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x672_no_pivot_row_0 = {io.in_x672_no_pivot_row_0} ; io.in_x672_no_pivot_row_0 := DontCare
    def x668_iter_0 = {io.in_x668_iter_0} ; io.in_x668_iter_0 := DontCare
    def x580_nrow = {io.in_x580_nrow} 
    def x600 = {io.in_x600} 
    def x671_no_pivot_col_0 = {io.in_x671_no_pivot_col_0} ; io.in_x671_no_pivot_col_0 := DontCare
    def x581_ncol = {io.in_x581_ncol} 
    def x592_tab_dram = {io.in_x592_tab_dram} 
  }
  def connectWires0(module: x1134_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    module.io.in_x602 <> x602
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x672_no_pivot_row_0.connectLedger(module.io.in_x672_no_pivot_row_0)
    x668_iter_0.connectLedger(module.io.in_x668_iter_0)
    module.io.in_x580_nrow <> x580_nrow
    module.io.in_x600 <> x600
    x671_no_pivot_col_0.connectLedger(module.io.in_x671_no_pivot_col_0)
    module.io.in_x581_ncol <> x581_ncol
    module.io.in_x592_tab_dram <> x592_tab_dram
  }
  val x592_tab_dram = list_x592_tab_dram(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x598_tab_sram_1 = list_x597_tab_sram_0(1)
  val x672_no_pivot_row_0 = list_x597_tab_sram_0(2)
  val x668_iter_0 = list_x597_tab_sram_0(3)
  val x671_no_pivot_col_0 = list_x597_tab_sram_0(4)
  val x600 = list_x600(0)
  val x580_nrow = list_x580_nrow(0)
  val x581_ncol = list_x580_nrow(1)
  val x602 = list_x602(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1134")
    implicit val stack = ControllerStack.stack.toList
    class x1134_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1134_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x667_outr_UnitPipe = new x667_outr_UnitPipe_kernel(List(x592_tab_dram), List(x597_tab_sram_0,x598_tab_sram_1), List(x600), List(x580_nrow,x581_ncol), List(x602) , Some(me), List(), 0, 2, 2, List(1), List(32), breakpoints, rr)
      x667_outr_UnitPipe.backpressure := true.B | x667_outr_UnitPipe.sm.io.doneLatch
      x667_outr_UnitPipe.forwardpressure := true.B | x667_outr_UnitPipe.sm.io.doneLatch
      x667_outr_UnitPipe.sm.io.enableOut.zip(x667_outr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x667_outr_UnitPipe.sm.io.break := false.B
      x667_outr_UnitPipe.mask := true.B & true.B
      x667_outr_UnitPipe.configure("x667_outr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x667_outr_UnitPipe.kernel()
      val x670_inr_UnitPipe = new x670_inr_UnitPipe_kernel(List(x668_iter_0) , Some(me), List(), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x670_inr_UnitPipe.sm.io.ctrDone := risingEdge(x670_inr_UnitPipe.sm.io.ctrInc)
      x670_inr_UnitPipe.backpressure := true.B | x670_inr_UnitPipe.sm.io.doneLatch
      x670_inr_UnitPipe.forwardpressure := true.B | x670_inr_UnitPipe.sm.io.doneLatch
      x670_inr_UnitPipe.sm.io.enableOut.zip(x670_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x670_inr_UnitPipe.sm.io.break := false.B
      x670_inr_UnitPipe.mask := true.B & true.B
      x670_inr_UnitPipe.configure("x670_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x670_inr_UnitPipe.kernel()
      val x678_inr_UnitPipe = new x678_inr_UnitPipe_kernel(List(x671_no_pivot_col_0,x672_no_pivot_row_0) , Some(me), List(), 2, 1, 1, List(1), List(32), breakpoints, rr)
      x678_inr_UnitPipe.sm.io.ctrDone := risingEdge(x678_inr_UnitPipe.sm.io.ctrInc)
      x678_inr_UnitPipe.backpressure := true.B | x678_inr_UnitPipe.sm.io.doneLatch
      x678_inr_UnitPipe.forwardpressure := true.B | x678_inr_UnitPipe.sm.io.doneLatch
      x678_inr_UnitPipe.sm.io.enableOut.zip(x678_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x678_inr_UnitPipe.sm.io.break := false.B
      x678_inr_UnitPipe.mask := true.B & true.B
      x678_inr_UnitPipe.configure("x678_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x678_inr_UnitPipe.kernel()
    }
    val module = Module(new x1134_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END ParallelPipe x1134 **/
