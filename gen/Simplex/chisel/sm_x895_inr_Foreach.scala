package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x895 -> x897 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x895_inr_Foreach **/
class x895_inr_Foreach_kernel(
  list_x1103_rd_x845: List[Bool],
  list_x846_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Pipelined, false   , latency = 37.0.toInt, myName = "x895_inr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x895_inr_Foreach_iiCtr"))
  
  abstract class x895_inr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x846_reg = Flipped(new StandardInterface(ModuleParams.getParams("x846_reg_p").asInstanceOf[MemParams] ))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x1103_rd_x845 = Input(Bool())
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x1102_rd_x760 = Input(Bool())
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x1101_rd_x684 = Input(Bool())
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x846_reg = {io.in_x846_reg} ; io.in_x846_reg := DontCare
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x1103_rd_x845 = {io.in_x1103_rd_x845} 
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x1102_rd_x760 = {io.in_x1102_rd_x760} 
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x1101_rd_x684 = {io.in_x1101_rd_x684} 
  }
  def connectWires0(module: x895_inr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    x846_reg.connectLedger(module.io.in_x846_reg)
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    module.io.in_x1103_rd_x845 <> x1103_rd_x845
    x685_reg.connectLedger(module.io.in_x685_reg)
    module.io.in_x1102_rd_x760 <> x1102_rd_x760
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    x759_reg.connectLedger(module.io.in_x759_reg)
    module.io.in_x1101_rd_x684 <> x1101_rd_x684
  }
  val x1103_rd_x845 = list_x1103_rd_x845(0)
  val x1102_rd_x760 = list_x1103_rd_x845(1)
  val x1101_rd_x684 = list_x1103_rd_x845(2)
  val x846_reg = list_x846_reg(0)
  val x597_tab_sram_0 = list_x846_reg(1)
  val x598_tab_sram_1 = list_x846_reg(2)
  val x674_pivot_element_0 = list_x846_reg(3)
  val x685_reg = list_x846_reg(4)
  val x680_pivot_row_0 = list_x846_reg(5)
  val x759_reg = list_x846_reg(6)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x895_inr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x895_inr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x895_inr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b878 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b878.suggestName("b878")
      val b879 = ~io.sigsIn.cchainOutputs.head.oobs(0); b879.suggestName("b879")
      val x880_rd_x680 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x880_rd_x680""")
      val x880_rd_x680_banks = List[UInt]()
      val x880_rd_x680_ofs = List[UInt]()
      val x880_rd_x680_en = List[Bool](true.B)
      x880_rd_x680.toSeq.zip(x680_pivot_row_0.connectRPort(880, x880_rd_x680_banks, x880_rd_x680_ofs, io.sigsIn.backpressure, x880_rd_x680_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x881_rd_x846 = Wire(Bool()).suggestName("""x881_rd_x846""")
      val x881_rd_x846_banks = List[UInt]()
      val x881_rd_x846_ofs = List[UInt]()
      val x881_rd_x846_en = List[Bool](true.B)
      x881_rd_x846.toSeq.zip(x846_reg.connectRPort(881, x881_rd_x846_banks, x881_rd_x846_ofs, io.sigsIn.backpressure, x881_rd_x846_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x882_rd_x759 = Wire(Bool()).suggestName("""x882_rd_x759""")
      val x882_rd_x759_banks = List[UInt]()
      val x882_rd_x759_ofs = List[UInt]()
      val x882_rd_x759_en = List[Bool](true.B)
      x882_rd_x759.toSeq.zip(x759_reg.connectRPort(882, x882_rd_x759_banks, x882_rd_x759_ofs, io.sigsIn.backpressure, x882_rd_x759_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x883_rd_x685 = Wire(Bool()).suggestName("""x883_rd_x685""")
      val x883_rd_x685_banks = List[UInt]()
      val x883_rd_x685_ofs = List[UInt]()
      val x883_rd_x685_en = List[Bool](true.B)
      x883_rd_x685.toSeq.zip(x685_reg.connectRPort(883, x883_rd_x685_banks, x883_rd_x685_ofs, io.sigsIn.backpressure, x883_rd_x685_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1126 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1126""")
      x1126.r := Math.fma(x880_rd_x680,10L.FP(true, 32, 0),b878,Some(6.0), true.B, "x1126").toFixed(x1126, "cast_x1126").r
      val x1179 = Wire(Bool()).suggestName("x1179_x881_rd_x846_D6") 
      x1179.r := getRetimed(x881_rd_x846.r, 6.toInt, io.sigsIn.backpressure)
      val x1180 = Wire(Bool()).suggestName("x1180_b879_D6") 
      x1180.r := getRetimed(b879.r, 6.toInt, io.sigsIn.backpressure)
      val x1181 = Wire(Bool()).suggestName("x1181_x883_rd_x685_D6") 
      x1181.r := getRetimed(x883_rd_x685.r, 6.toInt, io.sigsIn.backpressure)
      val x1182 = Wire(Bool()).suggestName("x1182_x882_rd_x759_D6") 
      x1182.r := getRetimed(x882_rd_x759.r, 6.toInt, io.sigsIn.backpressure)
      val x889_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x889_rd""")
      val x889_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x889_rd_ofs = List[UInt](x1126.r)
      val x889_rd_en = List[Bool](true.B)
      x889_rd.toSeq.zip(x597_tab_sram_0.connectRPort(889, x889_rd_banks, x889_rd_ofs, io.sigsIn.backpressure, x889_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(6.0.toInt, rr, io.sigsIn.backpressure) && x1181 & x1182 & x1179 & x1180), true)).foreach{case (a,b) => a := b}
      val x890_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x890_elem_0""")
      x890_elem_0.r := x889_rd(0).r
      val x891_rd_x674 = Wire(new FloatingPoint(24, 8)).suggestName("""x891_rd_x674""")
      val x891_rd_x674_banks = List[UInt]()
      val x891_rd_x674_ofs = List[UInt]()
      val x891_rd_x674_en = List[Bool](true.B)
      x891_rd_x674.toSeq.zip(x674_pivot_element_0.connectRPort(891, x891_rd_x674_banks, x891_rd_x674_ofs, io.sigsIn.backpressure, x891_rd_x674_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1183 = Wire(new FloatingPoint(24, 8)).suggestName("x1183_x891_rd_x674_D8") 
      x1183.r := getRetimed(x891_rd_x674.r, 8.toInt, io.sigsIn.backpressure)
      val x892 = Wire(new FloatingPoint(24, 8)).suggestName("""x892""")
      x892.r := Math.fdiv(x890_elem_0, x1183, Some(28.0), true.B,"x892").r
      val x1184 = Wire(Bool()).suggestName("x1184_x881_rd_x846_D36") 
      x1184.r := getRetimed(x881_rd_x846.r, 36.toInt, io.sigsIn.backpressure)
      val x1185 = Wire(Bool()).suggestName("x1185_b879_D36") 
      x1185.r := getRetimed(b879.r, 36.toInt, io.sigsIn.backpressure)
      val x1186 = Wire(Bool()).suggestName("x1186_x883_rd_x685_D36") 
      x1186.r := getRetimed(x883_rd_x685.r, 36.toInt, io.sigsIn.backpressure)
      val x1187 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1187_x1126_D30") 
      x1187.r := getRetimed(x1126.r, 30.toInt, io.sigsIn.backpressure)
      val x1188 = Wire(Bool()).suggestName("x1188_x882_rd_x759_D36") 
      x1188.r := getRetimed(x882_rd_x759.r, 36.toInt, io.sigsIn.backpressure)
      val x893_wr_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x893_wr_ofs = List[UInt](x1187.r)
      val x893_wr_en = List[Bool](true.B)
      val x893_wr_data = List[UInt](x892.r)
      x597_tab_sram_0.connectWPort(893, x893_wr_banks, x893_wr_ofs, x893_wr_data, x893_wr_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(36.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1186 & x1188 & x1184 & x1185))
      val x894_wr_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x894_wr_ofs = List[UInt](x1187.r)
      val x894_wr_en = List[Bool](true.B)
      val x894_wr_data = List[UInt](x892.r)
      x598_tab_sram_1.connectWPort(894, x894_wr_banks, x894_wr_ofs, x894_wr_data, x894_wr_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(36.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1186 & x1188 & x1184 & x1185))
    }
    val module = Module(new x895_inr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x895_inr_Foreach **/
