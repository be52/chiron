package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x842 -> x844 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x842_inr_Switch **/
class x842_inr_Switch_kernel(
  list_x1096_rd_x825: List[Bool],
  list_x825_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Fork, false   ,cases = 2, latency = 1.0.toInt, myName = "x842_inr_Switch_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x842_inr_Switch_iiCtr"))
  
  abstract class x842_inr_Switch_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x825_reg = Flipped(new StandardInterface(ModuleParams.getParams("x825_reg_p").asInstanceOf[MemParams] ))
      val in_x757_reg = Flipped(new StandardInterface(ModuleParams.getParams("x757_reg_p").asInstanceOf[MemParams] ))
      val in_x1096_rd_x825 = Input(Bool())
      val in_x672_no_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x672_no_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_x1097_rd_x826 = Input(Bool())
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x825_reg = {io.in_x825_reg} ; io.in_x825_reg := DontCare
    def x757_reg = {io.in_x757_reg} ; io.in_x757_reg := DontCare
    def x1096_rd_x825 = {io.in_x1096_rd_x825} 
    def x672_no_pivot_row_0 = {io.in_x672_no_pivot_row_0} ; io.in_x672_no_pivot_row_0 := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
    def x1097_rd_x826 = {io.in_x1097_rd_x826} 
  }
  def connectWires0(module: x842_inr_Switch_module)(implicit stack: List[KernelHash]): Unit = {
    x825_reg.connectLedger(module.io.in_x825_reg)
    x757_reg.connectLedger(module.io.in_x757_reg)
    module.io.in_x1096_rd_x825 <> x1096_rd_x825
    x672_no_pivot_row_0.connectLedger(module.io.in_x672_no_pivot_row_0)
    x684_reg.connectLedger(module.io.in_x684_reg)
    module.io.in_x1097_rd_x826 <> x1097_rd_x826
  }
  val x1096_rd_x825 = list_x1096_rd_x825(0)
  val x1097_rd_x826 = list_x1096_rd_x825(1)
  val x825_reg = list_x825_reg(0)
  val x757_reg = list_x825_reg(1)
  val x672_no_pivot_row_0 = list_x825_reg(2)
  val x684_reg = list_x825_reg(3)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x842_inr_Switch_obj")
    implicit val stack = ControllerStack.stack.toList
    class x842_inr_Switch_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x842_inr_Switch_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x840_inr_SwitchCase_obj = new x840_inr_SwitchCase_kernel(List(x825_reg,x757_reg,x672_no_pivot_row_0,x684_reg) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x840_inr_SwitchCase_obj.baseEn := io.sigsIn.smSelectsOut(x840_inr_SwitchCase_obj.childId)
      x840_inr_SwitchCase_obj.sm.io.ctrDone := risingEdge(x840_inr_SwitchCase_obj.sm.io.ctrInc)
      x840_inr_SwitchCase_obj.backpressure := true.B | x840_inr_SwitchCase_obj.sm.io.doneLatch
      x840_inr_SwitchCase_obj.forwardpressure := true.B | x840_inr_SwitchCase_obj.sm.io.doneLatch
      x840_inr_SwitchCase_obj.sm.io.enableOut.zip(x840_inr_SwitchCase_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x840_inr_SwitchCase_obj.sm.io.break := false.B
      x840_inr_SwitchCase_obj.mask := true.B & true.B
      x840_inr_SwitchCase_obj.configure("x840_inr_SwitchCase_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = true)
      x840_inr_SwitchCase_obj.kernel()
      val x841_inr_SwitchCase_obj = new x841_inr_SwitchCase_kernel(  Some(me), List(), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x841_inr_SwitchCase_obj.baseEn := io.sigsIn.smSelectsOut(x841_inr_SwitchCase_obj.childId)
      x841_inr_SwitchCase_obj.sm.io.ctrDone := risingEdge(x841_inr_SwitchCase_obj.sm.io.ctrInc)
      x841_inr_SwitchCase_obj.backpressure := true.B | x841_inr_SwitchCase_obj.sm.io.doneLatch
      x841_inr_SwitchCase_obj.forwardpressure := true.B | x841_inr_SwitchCase_obj.sm.io.doneLatch
      x841_inr_SwitchCase_obj.sm.io.enableOut.zip(x841_inr_SwitchCase_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x841_inr_SwitchCase_obj.sm.io.break := false.B
      x841_inr_SwitchCase_obj.mask := true.B & true.B
      x841_inr_SwitchCase_obj.configure("x841_inr_SwitchCase_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = true)
      x841_inr_SwitchCase_obj.kernel()
    }
    val module = Module(new x842_inr_Switch_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END Switch x842_inr_Switch **/
