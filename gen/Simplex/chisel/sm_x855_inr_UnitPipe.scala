package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x855 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x855_inr_UnitPipe **/
class x855_inr_UnitPipe_kernel(
  list_b47: List[FixedPoint],
  list_x846_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.4.toInt, myName = "x855_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x855_inr_UnitPipe_iiCtr"))
  
  abstract class x855_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x846_reg = Flipped(new StandardInterface(ModuleParams.getParams("x846_reg_p").asInstanceOf[MemParams] ))
      val in_x847_reg = Flipped(new StandardInterface(ModuleParams.getParams("x847_reg_p").asInstanceOf[MemParams] ))
      val in_x848_reg = Flipped(new StandardInterface(ModuleParams.getParams("x848_reg_p").asInstanceOf[MemParams] ))
      val in_x845_reg = Flipped(new StandardInterface(ModuleParams.getParams("x845_reg_p").asInstanceOf[MemParams] ))
      val in_b47 = Input(new FixedPoint(true, 32, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x846_reg = {io.in_x846_reg} ; io.in_x846_reg := DontCare
    def x847_reg = {io.in_x847_reg} ; io.in_x847_reg := DontCare
    def x848_reg = {io.in_x848_reg} ; io.in_x848_reg := DontCare
    def x845_reg = {io.in_x845_reg} ; io.in_x845_reg := DontCare
    def b47 = {io.in_b47} 
  }
  def connectWires0(module: x855_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x846_reg.connectLedger(module.io.in_x846_reg)
    x847_reg.connectLedger(module.io.in_x847_reg)
    x848_reg.connectLedger(module.io.in_x848_reg)
    x845_reg.connectLedger(module.io.in_x845_reg)
    module.io.in_b47 <> b47
  }
  val b47 = list_b47(0)
  val x846_reg = list_x846_reg(0)
  val x847_reg = list_x846_reg(1)
  val x848_reg = list_x846_reg(2)
  val x845_reg = list_x846_reg(3)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x855_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x855_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x855_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x849 = Wire(Bool()).suggestName("""x849""")
      x849.r := Math.eql(b47, 2L.FP(true, 32, 0), Some(0.2), true.B,"x849").r
      val x850 = Wire(Bool()).suggestName("""x850""")
      x850 := ~x849
      val x851_wr_x846_banks = List[UInt]()
      val x851_wr_x846_ofs = List[UInt]()
      val x851_wr_x846_en = List[Bool](true.B)
      val x851_wr_x846_data = List[UInt](x849.r)
      x846_reg.connectWPort(851, x851_wr_x846_banks, x851_wr_x846_ofs, x851_wr_x846_data, x851_wr_x846_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x852_wr_x845_banks = List[UInt]()
      val x852_wr_x845_ofs = List[UInt]()
      val x852_wr_x845_en = List[Bool](true.B)
      val x852_wr_x845_data = List[UInt](x849.r)
      x845_reg.connectWPort(852, x852_wr_x845_banks, x852_wr_x845_ofs, x852_wr_x845_data, x852_wr_x845_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.2.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x853_wr_x848_banks = List[UInt]()
      val x853_wr_x848_ofs = List[UInt]()
      val x853_wr_x848_en = List[Bool](true.B)
      val x853_wr_x848_data = List[UInt](x850.r)
      x848_reg.connectWPort(853, x853_wr_x848_banks, x853_wr_x848_ofs, x853_wr_x848_data, x853_wr_x848_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
      val x854_wr_x847_banks = List[UInt]()
      val x854_wr_x847_ofs = List[UInt]()
      val x854_wr_x847_en = List[Bool](true.B)
      val x854_wr_x847_data = List[UInt](x850.r)
      x847_reg.connectWPort(854, x854_wr_x847_banks, x854_wr_x847_ofs, x854_wr_x847_data, x854_wr_x847_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.4.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B))
    }
    val module = Module(new x855_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x855_inr_UnitPipe **/
