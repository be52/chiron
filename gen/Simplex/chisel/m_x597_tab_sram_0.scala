package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

class x597_tab_sram_0 {
  val w0 = Access(663, 2, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val w1 = Access(893, 0, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val w2 = Access(978, 1, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r6 = Access(1070, 3, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r1 = Access(798, 0, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r3 = Access(974, 5, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r2 = Access(889, 7, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r0 = Access(941, 4, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r4 = Access(868, 1, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r5 = Access(712, 6, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val r7 = Access(740, 2, 0, List(0), List(0), None, PortInfo(Some(0), 1, 7, List(1), 32, List(List(RG(0)))))
  val m = Module(new BankedSRAM( 
    List[Int](10,10),
     32, 
    List[Int](1),
    List[Int](1),
    List[Int](1,1),
    List(w0,w1,w2),
    List(r0,r1,r2,r3,r4,r5,r6,r7),
    BankedMemory, 
    None, 
    true, 
    0,
    11, 
    myName = "x597_tab_sram_0"
  ))
  m.io.asInstanceOf[StandardInterface] <> DontCare
  m.io.reset := false.B
  ModuleParams.addParams("x597_tab_sram_0_p", m.io.p)
}
