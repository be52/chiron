package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1074 -> x1075 -> x1080 -> x1081 -> x1137 **/
/** BEGIN None x1074_inr_Foreach **/
class x1074_inr_Foreach_kernel(
  list_b1027: List[FixedPoint],
  list_x597_tab_sram_0: List[StandardInterface],
  list_x1023: List[DecoupledIO[AppStoreData]],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Pipelined, false   , latency = 9.0.toInt, myName = "x1074_inr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1074_inr_Foreach_iiCtr"))
  
  abstract class x1074_inr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_b1027 = Input(new FixedPoint(true, 32, 0))
      val in_x1029_reg = Flipped(new StandardInterface(ModuleParams.getParams("x1029_reg_p").asInstanceOf[MemParams] ))
      val in_x1023 = Decoupled(new AppStoreData(ModuleParams.getParams("x1023_p").asInstanceOf[(Int,Int)] ))
      val in_x1030_reg = Flipped(new StandardInterface(ModuleParams.getParams("x1030_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def b1027 = {io.in_b1027} 
    def x1029_reg = {io.in_x1029_reg} ; io.in_x1029_reg := DontCare
    def x1023 = {io.in_x1023} 
    def x1030_reg = {io.in_x1030_reg} ; io.in_x1030_reg := DontCare
  }
  def connectWires0(module: x1074_inr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    module.io.in_b1027 <> b1027
    x1029_reg.connectLedger(module.io.in_x1029_reg)
    module.io.in_x1023 <> x1023
    x1030_reg.connectLedger(module.io.in_x1030_reg)
  }
  val b1027 = list_b1027(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x1029_reg = list_x597_tab_sram_0(1)
  val x1030_reg = list_x597_tab_sram_0(2)
  val x1023 = list_x1023(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1074_inr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x1074_inr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1074_inr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b1057 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b1057.suggestName("b1057")
      val b1058 = ~io.sigsIn.cchainOutputs.head.oobs(0); b1058.suggestName("b1058")
      val x1059_rd_x1029 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1059_rd_x1029""")
      val x1059_rd_x1029_banks = List[UInt]()
      val x1059_rd_x1029_ofs = List[UInt]()
      val x1059_rd_x1029_en = List[Bool](true.B)
      x1059_rd_x1029.toSeq.zip(x1029_reg.connectRPort(1059, x1059_rd_x1029_banks, x1059_rd_x1029_ofs, io.sigsIn.backpressure, x1059_rd_x1029_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1060 = Wire(Bool()).suggestName("""x1060""")
      val ensig0 = Wire(Bool())
      ensig0 := x1023.ready
      x1060.r := Math.lte(x1059_rd_x1029, b1057, Some(0.4), ensig0,"x1060").r
      val x1061_rd_x1030 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1061_rd_x1030""")
      val x1061_rd_x1030_banks = List[UInt]()
      val x1061_rd_x1030_ofs = List[UInt]()
      val x1061_rd_x1030_en = List[Bool](true.B)
      x1061_rd_x1030.toSeq.zip(x1030_reg.connectRPort(1061, x1061_rd_x1030_banks, x1061_rd_x1030_ofs, io.sigsIn.backpressure, x1061_rd_x1030_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1062 = Wire(Bool()).suggestName("""x1062""")
      x1062.r := Math.lt(b1057, x1061_rd_x1030, Some(0.4), ensig0,"x1062").r
      val x1063 = Wire(Bool()).suggestName("""x1063""")
      x1063 := x1060 & x1062
      val x1064_sub = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1064_sub""")
      x1064_sub.r := Math.sub(b1057,x1059_rd_x1029,Some(1.0), ensig0, Truncate, Wrapping, "x1064_sub").r
      val x1229 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1229_b1027_D1") 
      x1229.r := getRetimed(b1027.r, 1.toInt, io.sigsIn.backpressure)
      val x1133 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1133""")
      x1133.r := Math.fma(x1229,10L.FP(true, 32, 0),x1064_sub,Some(6.0), ensig0, "x1133").toFixed(x1133, "cast_x1133").r
      val x1230 = Wire(Bool()).suggestName("x1230_b1058_D7") 
      x1230.r := getRetimed(b1058.r, 7.toInt, io.sigsIn.backpressure)
      val x1231 = Wire(Bool()).suggestName("x1231_x1063_D7") 
      x1231.r := getRetimed(x1063.r, 7.toInt, io.sigsIn.backpressure)
      val x1070_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x1070_rd""")
      val x1070_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x1070_rd_ofs = List[UInt](x1133.r)
      val x1070_rd_en = List[Bool](true.B)
      x1070_rd.toSeq.zip(x597_tab_sram_0.connectRPort(1070, x1070_rd_banks, x1070_rd_ofs, io.sigsIn.backpressure, x1070_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(7.0.toInt, rr, io.sigsIn.backpressure) && x1231 & true.B & x1230), true)).foreach{case (a,b) => a := b}
      val x1071_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x1071_elem_0""")
      x1071_elem_0.r := x1070_rd(0).r
      val x1232 = Wire(Bool()).suggestName("x1232_x1063_D9") 
      x1232.r := getRetimed(x1063.r, 9.toInt, io.sigsIn.backpressure)
      val x1072_tuple = Wire(UInt(33.W)).suggestName("""x1072_tuple""")
      x1072_tuple.r := Cat(x1232,x1071_elem_0.r)
      val x1233 = Wire(Bool()).suggestName("x1233_b1058_D9") 
      x1233.r := getRetimed(b1058.r, 9.toInt, io.sigsIn.backpressure)
      x1023.valid := (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(9.0.toInt.toInt, rr, io.sigsIn.backpressure) & x1233 & io.sigsIn.backpressure
      x1023.bits.wdata(0) := x1072_tuple(31,0)
      x1023.bits.wstrb := x1072_tuple(32,32)
    }
    val module = Module(new x1074_inr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x1074_inr_Foreach **/
