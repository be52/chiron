package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x1002_outr_SwitchCase **/
class x1002_outr_SwitchCase_kernel(
  list_b47: List[FixedPoint],
  list_x760_reg: List[StandardInterface],
  list_x580_nrow: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 2, isFSM = false   , latency = 0.0.toInt, myName = "x1002_outr_SwitchCase_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1002_outr_SwitchCase_iiCtr"))
  
  abstract class x1002_outr_SwitchCase_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x760_reg = Flipped(new StandardInterface(ModuleParams.getParams("x760_reg_p").asInstanceOf[MemParams] ))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x668_iter_0 = Flipped(new StandardInterface(ModuleParams.getParams("x668_iter_0_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_b47 = Input(new FixedPoint(true, 32, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x760_reg = {io.in_x760_reg} ; io.in_x760_reg := DontCare
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x668_iter_0 = {io.in_x668_iter_0} ; io.in_x668_iter_0 := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x580_nrow = {io.in_x580_nrow} 
    def x581_ncol = {io.in_x581_ncol} 
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
    def b47 = {io.in_b47} 
  }
  def connectWires0(module: x1002_outr_SwitchCase_module)(implicit stack: List[KernelHash]): Unit = {
    x760_reg.connectLedger(module.io.in_x760_reg)
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x668_iter_0.connectLedger(module.io.in_x668_iter_0)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    module.io.in_x580_nrow <> x580_nrow
    module.io.in_x581_ncol <> x581_ncol
    x759_reg.connectLedger(module.io.in_x759_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
    module.io.in_b47 <> b47
  }
  val b47 = list_b47(0)
  val x760_reg = list_x760_reg(0)
  val x597_tab_sram_0 = list_x760_reg(1)
  val x598_tab_sram_1 = list_x760_reg(2)
  val x679_pivot_col_0 = list_x760_reg(3)
  val x674_pivot_element_0 = list_x760_reg(4)
  val x668_iter_0 = list_x760_reg(5)
  val x685_reg = list_x760_reg(6)
  val x680_pivot_row_0 = list_x760_reg(7)
  val x759_reg = list_x760_reg(8)
  val x684_reg = list_x760_reg(9)
  val x580_nrow = list_x580_nrow(0)
  val x581_ncol = list_x580_nrow(1)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1002_outr_SwitchCase_obj")
    implicit val stack = ControllerStack.stack.toList
    class x1002_outr_SwitchCase_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1002_outr_SwitchCase_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      // Controller Stack: Stack(x1003, x1005, x1006, x1020, x1137)
      val x845_reg = (new x845_reg).m.io.asInstanceOf[StandardInterface]
      val x846_reg = (new x846_reg).m.io.asInstanceOf[StandardInterface]
      val x847_reg = (new x847_reg).m.io.asInstanceOf[StandardInterface]
      val x848_reg = (new x848_reg).m.io.asInstanceOf[StandardInterface]
      val x855_inr_UnitPipe = new x855_inr_UnitPipe_kernel(List(b47), List(x846_reg,x847_reg,x848_reg,x845_reg) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x855_inr_UnitPipe.sm.io.ctrDone := risingEdge(x855_inr_UnitPipe.sm.io.ctrInc)
      x855_inr_UnitPipe.backpressure := true.B | x855_inr_UnitPipe.sm.io.doneLatch
      x855_inr_UnitPipe.forwardpressure := true.B | x855_inr_UnitPipe.sm.io.doneLatch
      x855_inr_UnitPipe.sm.io.enableOut.zip(x855_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x855_inr_UnitPipe.sm.io.break := false.B
      x855_inr_UnitPipe.mask := true.B & true.B
      x855_inr_UnitPipe.configure("x855_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x855_inr_UnitPipe.kernel()
      val x1098_rd_x845 = Wire(Bool()).suggestName("""x1098_rd_x845""")
      val x1098_rd_x845_banks = List[UInt]()
      val x1098_rd_x845_ofs = List[UInt]()
      val x1098_rd_x845_en = List[Bool](true.B)
      x1098_rd_x845.toSeq.zip(x845_reg.connectRPort(1098, x1098_rd_x845_banks, x1098_rd_x845_ofs, io.sigsIn.backpressure, x1098_rd_x845_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1099_rd_x847 = Wire(Bool()).suggestName("""x1099_rd_x847""")
      val x1099_rd_x847_banks = List[UInt]()
      val x1099_rd_x847_ofs = List[UInt]()
      val x1099_rd_x847_en = List[Bool](true.B)
      x1099_rd_x847.toSeq.zip(x847_reg.connectRPort(1099, x1099_rd_x847_banks, x1099_rd_x847_ofs, io.sigsIn.backpressure, x1099_rd_x847_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1000_outr_Switch_obj = new x1000_outr_Switch_kernel(List(x1099_rd_x847,x1098_rd_x845), List(b47), List(x760_reg,x846_reg,x597_tab_sram_0,x598_tab_sram_1,x847_reg,x679_pivot_col_0,x674_pivot_element_0,x848_reg,x668_iter_0,x685_reg,x680_pivot_row_0,x845_reg,x759_reg,x684_reg), List(x580_nrow,x581_ncol) , Some(me), List(), 1, 2, 1, List(1), List(32), breakpoints, rr)
      val x897_outr_SwitchCase_switch_sel_reg = RegInit(false.B)
      x897_outr_SwitchCase_switch_sel_reg := Mux(risingEdge(x1000_outr_Switch_obj.en), x1098_rd_x845, x897_outr_SwitchCase_switch_sel_reg)
      x1000_outr_Switch_obj.sm.io.selectsIn(0) := x1098_rd_x845
      val x999_outr_SwitchCase_switch_sel_reg = RegInit(false.B)
      x999_outr_SwitchCase_switch_sel_reg := Mux(risingEdge(x1000_outr_Switch_obj.en), x1099_rd_x847, x999_outr_SwitchCase_switch_sel_reg)
      x1000_outr_Switch_obj.sm.io.selectsIn(1) := x1099_rd_x847
      x1000_outr_Switch_obj.backpressure := true.B | x1000_outr_Switch_obj.sm.io.doneLatch
      x1000_outr_Switch_obj.forwardpressure := true.B | x1000_outr_Switch_obj.sm.io.doneLatch
      x1000_outr_Switch_obj.sm.io.enableOut.zip(x1000_outr_Switch_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x1000_outr_Switch_obj.sm.io.break := false.B
      x1000_outr_Switch_obj.mask := true.B & true.B
      x1000_outr_Switch_obj.configure("x1000_outr_Switch_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1000_outr_Switch_obj.kernel()
    }
    val module = Module(new x1002_outr_SwitchCase_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END SwitchCase x1002_outr_SwitchCase **/
