package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1135 -> x1136 -> x997 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x1135 **/
class x1135_kernel(
  list_x1110_rd_x898: List[Bool],
  list_x911_ctrchain: List[CounterChainInterface],
  list_x760_reg: List[StandardInterface],
  list_x581_ncol: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(ForkJoin, 2, isFSM = false   , latency = 0.0.toInt, myName = "x1135_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1135_iiCtr"))
  
  abstract class x1135_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x760_reg = Flipped(new StandardInterface(ModuleParams.getParams("x760_reg_p").asInstanceOf[MemParams] ))
      val in_x1110_rd_x898 = Input(Bool())
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x898_reg = Flipped(new StandardInterface(ModuleParams.getParams("x898_reg_p").asInstanceOf[MemParams] ))
      val in_x1109_rd_x847 = Input(Bool())
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x847_reg = Flipped(new StandardInterface(ModuleParams.getParams("x847_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x911_ctrchain = Flipped(new CounterChainInterface(ModuleParams.getParams("x911_ctrchain_p").asInstanceOf[(List[Int],List[Int])] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x1107_rd_x684 = Input(Bool())
      val in_x848_reg = Flipped(new StandardInterface(ModuleParams.getParams("x848_reg_p").asInstanceOf[MemParams] ))
      val in_x668_iter_0 = Flipped(new StandardInterface(ModuleParams.getParams("x668_iter_0_p").asInstanceOf[MemParams] ))
      val in_x685_reg = Flipped(new StandardInterface(ModuleParams.getParams("x685_reg_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x1108_rd_x759 = Input(Bool())
      val in_x899_reg = Flipped(new StandardInterface(ModuleParams.getParams("x899_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(2, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(2, 1))
      val rr = Input(Bool())
    })
    def x760_reg = {io.in_x760_reg} ; io.in_x760_reg := DontCare
    def x1110_rd_x898 = {io.in_x1110_rd_x898} 
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x898_reg = {io.in_x898_reg} ; io.in_x898_reg := DontCare
    def x1109_rd_x847 = {io.in_x1109_rd_x847} 
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x847_reg = {io.in_x847_reg} ; io.in_x847_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x911_ctrchain = {io.in_x911_ctrchain} ; io.in_x911_ctrchain := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x1107_rd_x684 = {io.in_x1107_rd_x684} 
    def x848_reg = {io.in_x848_reg} ; io.in_x848_reg := DontCare
    def x668_iter_0 = {io.in_x668_iter_0} ; io.in_x668_iter_0 := DontCare
    def x685_reg = {io.in_x685_reg} ; io.in_x685_reg := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x581_ncol = {io.in_x581_ncol} 
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x1108_rd_x759 = {io.in_x1108_rd_x759} 
    def x899_reg = {io.in_x899_reg} ; io.in_x899_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x1135_module)(implicit stack: List[KernelHash]): Unit = {
    x760_reg.connectLedger(module.io.in_x760_reg)
    module.io.in_x1110_rd_x898 <> x1110_rd_x898
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x898_reg.connectLedger(module.io.in_x898_reg)
    module.io.in_x1109_rd_x847 <> x1109_rd_x847
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x847_reg.connectLedger(module.io.in_x847_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    module.io.in_x911_ctrchain.input <> x911_ctrchain.input; module.io.in_x911_ctrchain.output <> x911_ctrchain.output
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    module.io.in_x1107_rd_x684 <> x1107_rd_x684
    x848_reg.connectLedger(module.io.in_x848_reg)
    x668_iter_0.connectLedger(module.io.in_x668_iter_0)
    x685_reg.connectLedger(module.io.in_x685_reg)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    module.io.in_x581_ncol <> x581_ncol
    x759_reg.connectLedger(module.io.in_x759_reg)
    module.io.in_x1108_rd_x759 <> x1108_rd_x759
    x899_reg.connectLedger(module.io.in_x899_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x1110_rd_x898 = list_x1110_rd_x898(0)
  val x1109_rd_x847 = list_x1110_rd_x898(1)
  val x1107_rd_x684 = list_x1110_rd_x898(2)
  val x1108_rd_x759 = list_x1110_rd_x898(3)
  val x911_ctrchain = list_x911_ctrchain(0)
  val x760_reg = list_x760_reg(0)
  val x597_tab_sram_0 = list_x760_reg(1)
  val x898_reg = list_x760_reg(2)
  val x598_tab_sram_1 = list_x760_reg(3)
  val x847_reg = list_x760_reg(4)
  val x679_pivot_col_0 = list_x760_reg(5)
  val x674_pivot_element_0 = list_x760_reg(6)
  val x848_reg = list_x760_reg(7)
  val x668_iter_0 = list_x760_reg(8)
  val x685_reg = list_x760_reg(9)
  val x680_pivot_row_0 = list_x760_reg(10)
  val x759_reg = list_x760_reg(11)
  val x899_reg = list_x760_reg(12)
  val x684_reg = list_x760_reg(13)
  val x581_ncol = list_x581_ncol(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1135")
    implicit val stack = ControllerStack.stack.toList
    class x1135_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1135_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x985_outr_Foreach = new x985_outr_Foreach_kernel(List(x1110_rd_x898,x1109_rd_x847,x1107_rd_x684,x1108_rd_x759), List(x760_reg,x597_tab_sram_0,x898_reg,x598_tab_sram_1,x847_reg,x679_pivot_col_0,x674_pivot_element_0,x848_reg,x685_reg,x680_pivot_row_0,x759_reg,x899_reg,x684_reg), List(x581_ncol) , Some(me), List(x911_ctrchain), 0, 2, 1, List(1), List(32), breakpoints, rr)
      x985_outr_Foreach.sm.io.ctrDone := (x985_outr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x985_outr_Foreach.backpressure := true.B | x985_outr_Foreach.sm.io.doneLatch
      x985_outr_Foreach.forwardpressure := true.B | x985_outr_Foreach.sm.io.doneLatch
      x985_outr_Foreach.sm.io.enableOut.zip(x985_outr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x985_outr_Foreach.sm.io.break := false.B
      x985_outr_Foreach.mask := ~x985_outr_Foreach.cchain.head.output.noop & x1107_rd_x684 & x1108_rd_x759 & x1109_rd_x847 & x1110_rd_x898
      x985_outr_Foreach.configure("x985_outr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x985_outr_Foreach.kernel()
      val x993_inr_UnitPipe = new x993_inr_UnitPipe_kernel(List(x898_reg,x847_reg,x668_iter_0,x759_reg,x684_reg) , Some(me), List(), 1, 1, 1, List(1), List(32), breakpoints, rr)
      x993_inr_UnitPipe.sm.io.ctrDone := risingEdge(x993_inr_UnitPipe.sm.io.ctrInc)
      x993_inr_UnitPipe.backpressure := true.B | x993_inr_UnitPipe.sm.io.doneLatch
      x993_inr_UnitPipe.forwardpressure := true.B | x993_inr_UnitPipe.sm.io.doneLatch
      x993_inr_UnitPipe.sm.io.enableOut.zip(x993_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x993_inr_UnitPipe.sm.io.break := false.B
      x993_inr_UnitPipe.mask := true.B & true.B
      x993_inr_UnitPipe.configure("x993_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x993_inr_UnitPipe.kernel()
    }
    val module = Module(new x1135_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END ParallelPipe x1135 **/
