package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x728 -> x756 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x728_inr_Foreach **/
class x728_inr_Foreach_kernel(
  list_x1088_rd_x683: List[Bool],
  list_x597_tab_sram_0: List[StandardInterface],
  list_x580_nrow: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Pipelined, false   , latency = 11.0.toInt, myName = "x728_inr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(4.0.toInt, 2 + _root_.utils.math.log2Up(4.0.toInt), "x728_inr_Foreach_iiCtr"))
  
  abstract class x728_inr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x682_reg = Flipped(new StandardInterface(ModuleParams.getParams("x682_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x1088_rd_x683 = Input(Bool())
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x682_reg = {io.in_x682_reg} ; io.in_x682_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x580_nrow = {io.in_x580_nrow} 
    def x1088_rd_x683 = {io.in_x1088_rd_x683} 
  }
  def connectWires0(module: x728_inr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x682_reg.connectLedger(module.io.in_x682_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    module.io.in_x580_nrow <> x580_nrow
    module.io.in_x1088_rd_x683 <> x1088_rd_x683
  }
  val x1088_rd_x683 = list_x1088_rd_x683(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x598_tab_sram_1 = list_x597_tab_sram_0(1)
  val x682_reg = list_x597_tab_sram_0(2)
  val x679_pivot_col_0 = list_x597_tab_sram_0(3)
  val x580_nrow = list_x580_nrow(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x728_inr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x728_inr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x728_inr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b702 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b702.suggestName("b702")
      val b703 = ~io.sigsIn.cchainOutputs.head.oobs(0); b703.suggestName("b703")
      val x704_rd_x580 = Wire(new FixedPoint(true, 32, 0))
      x704_rd_x580.r := x580_nrow.r
      val x705_sub = Wire(new FixedPoint(true, 32, 0)).suggestName("""x705_sub""")
      x705_sub.r := Math.sub(x704_rd_x580,1L.FP(true, 32, 0),Some(1.0), true.B, Truncate, Wrapping, "x705_sub").r
      val x706_rd_x682 = Wire(Bool()).suggestName("""x706_rd_x682""")
      val x706_rd_x682_banks = List[UInt]()
      val x706_rd_x682_ofs = List[UInt]()
      val x706_rd_x682_en = List[Bool](true.B)
      x706_rd_x682.toSeq.zip(x682_reg.connectRPort(706, x706_rd_x682_banks, x706_rd_x682_ofs, io.sigsIn.backpressure, x706_rd_x682_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x709_mul = Wire(new FixedPoint(true, 32, 0)).suggestName("""x709_mul""")
      x709_mul.r := (Math.mul(x705_sub, 10L.FP(true, 32, 0), Some(6.0), true.B, Truncate, Wrapping, "x709_mul")).r
      val x1152 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1152_b702_D7") 
      x1152.r := getRetimed(b702.r, 7.toInt, io.sigsIn.backpressure)
      val x710_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x710_sum""")
      x710_sum.r := Math.add(x709_mul,x1152,Some(1.0), true.B, Truncate, Wrapping, "x710_sum").r
      val x1153 = Wire(Bool()).suggestName("x1153_b703_D8") 
      x1153.r := getRetimed(b703.r, 8.toInt, io.sigsIn.backpressure)
      val x1154 = Wire(Bool()).suggestName("x1154_x706_rd_x682_D8") 
      x1154.r := getRetimed(x706_rd_x682.r, 8.toInt, io.sigsIn.backpressure)
      val x712_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x712_rd""")
      val x712_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x712_rd_ofs = List[UInt](x710_sum.r)
      val x712_rd_en = List[Bool](true.B)
      x712_rd.toSeq.zip(x597_tab_sram_0.connectRPort(712, x712_rd_banks, x712_rd_ofs, io.sigsIn.backpressure, x712_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(8.0.toInt, rr, io.sigsIn.backpressure) && x1154 & x1153), true)).foreach{case (a,b) => a := b}
      val x713_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x713_elem_0""")
      x713_elem_0.r := x712_rd(0).r
      val x714_rd_x679 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x714_rd_x679""")
      val x714_rd_x679_banks = List[UInt]()
      val x714_rd_x679_ofs = List[UInt]()
      val x714_rd_x679_en = List[Bool](true.B)
      x714_rd_x679.toSeq.zip(x679_pivot_col_0.connectRPort(714, x714_rd_x679_banks, x714_rd_x679_ofs, io.sigsIn.backpressure, x714_rd_x679_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(7.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x717_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x717_sum""")
      x717_sum.r := Math.add(x709_mul,x714_rd_x679,Some(1.0), true.B, Truncate, Wrapping, "x717_sum").r
      val x719_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x719_rd""")
      val x719_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x719_rd_ofs = List[UInt](x717_sum.r)
      val x719_rd_en = List[Bool](true.B)
      x719_rd.toSeq.zip(x598_tab_sram_1.connectRPort(719, x719_rd_banks, x719_rd_ofs, io.sigsIn.backpressure, x719_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(8.0.toInt, rr, io.sigsIn.backpressure) && x1154 & x1153), true)).foreach{case (a,b) => a := b}
      val x720_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x720_elem_0""")
      x720_elem_0.r := x719_rd(0).r
      val x721 = Wire(Bool()).suggestName("""x721""")
      x721.r := Math.flt(x713_elem_0, x720_elem_0, Some(0.0), true.B,"x721").r
      val x723_rd_x682 = Wire(Bool()).suggestName("""x723_rd_x682""")
      val x723_rd_x682_banks = List[UInt]()
      val x723_rd_x682_ofs = List[UInt]()
      val x723_rd_x682_en = List[Bool](true.B)
      x723_rd_x682.toSeq.zip(x682_reg.connectRPort(723, x723_rd_x682_banks, x723_rd_x682_ofs, io.sigsIn.backpressure, x723_rd_x682_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1155 = Wire(Bool()).suggestName("x1155_x723_rd_x682_D10") 
      x1155.r := getRetimed(x723_rd_x682.r, 10.toInt, io.sigsIn.backpressure)
      val x1156 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1156_b702_D10") 
      x1156.r := getRetimed(b702.r, 10.toInt, io.sigsIn.backpressure)
      val x724_wr_x679_banks = List[UInt]()
      val x724_wr_x679_ofs = List[UInt]()
      val x724_wr_x679_en = List[Bool](true.B)
      val x724_wr_x679_data = List[UInt](x1156.r)
      x679_pivot_col_0.connectWPort(724, x724_wr_x679_banks, x724_wr_x679_ofs, x724_wr_x679_data, x724_wr_x679_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(10.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1155 & x721))
    }
    val module = Module(new x728_inr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x728_inr_Foreach **/
