package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1020 -> x1137 **/
/** BEGIN None x1020_outr_FSM **/
class x1020_outr_FSM_kernel(
  list_x597_tab_sram_0: List[StandardInterface],
  list_x582_ncons: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Sequenced, 3, isFSM = true ,stateWidth = 32  , latency = 0.0.toInt, myName = "x1020_outr_FSM_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1020_outr_FSM_iiCtr"))
  
  abstract class x1020_outr_FSM_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x582_ncons = Input(UInt(64.W))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x598_tab_sram_1 = Flipped(new StandardInterface(ModuleParams.getParams("x598_tab_sram_1_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x672_no_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x672_no_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x668_iter_0 = Flipped(new StandardInterface(ModuleParams.getParams("x668_iter_0_p").asInstanceOf[MemParams] ))
      val in_x675_pivot_row_min_ratio_0 = Flipped(new StandardInterface(ModuleParams.getParams("x675_pivot_row_min_ratio_0_p").asInstanceOf[MemParams] ))
      val in_x680_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x680_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x673_no_pivot_row_count_0 = Flipped(new StandardInterface(ModuleParams.getParams("x673_no_pivot_row_count_0_p").asInstanceOf[MemParams] ))
      val in_x671_no_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x671_no_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(3, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(3, 1))
      val rr = Input(Bool())
    })
    def x582_ncons = {io.in_x582_ncons} 
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x598_tab_sram_1 = {io.in_x598_tab_sram_1} ; io.in_x598_tab_sram_1 := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x672_no_pivot_row_0 = {io.in_x672_no_pivot_row_0} ; io.in_x672_no_pivot_row_0 := DontCare
    def x668_iter_0 = {io.in_x668_iter_0} ; io.in_x668_iter_0 := DontCare
    def x675_pivot_row_min_ratio_0 = {io.in_x675_pivot_row_min_ratio_0} ; io.in_x675_pivot_row_min_ratio_0 := DontCare
    def x680_pivot_row_0 = {io.in_x680_pivot_row_0} ; io.in_x680_pivot_row_0 := DontCare
    def x580_nrow = {io.in_x580_nrow} 
    def x673_no_pivot_row_count_0 = {io.in_x673_no_pivot_row_count_0} ; io.in_x673_no_pivot_row_count_0 := DontCare
    def x671_no_pivot_col_0 = {io.in_x671_no_pivot_col_0} ; io.in_x671_no_pivot_col_0 := DontCare
    def x581_ncol = {io.in_x581_ncol} 
  }
  def connectWires0(module: x1020_outr_FSM_module)(implicit stack: List[KernelHash]): Unit = {
    module.io.in_x582_ncons <> x582_ncons
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x598_tab_sram_1.connectLedger(module.io.in_x598_tab_sram_1)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x672_no_pivot_row_0.connectLedger(module.io.in_x672_no_pivot_row_0)
    x668_iter_0.connectLedger(module.io.in_x668_iter_0)
    x675_pivot_row_min_ratio_0.connectLedger(module.io.in_x675_pivot_row_min_ratio_0)
    x680_pivot_row_0.connectLedger(module.io.in_x680_pivot_row_0)
    module.io.in_x580_nrow <> x580_nrow
    x673_no_pivot_row_count_0.connectLedger(module.io.in_x673_no_pivot_row_count_0)
    x671_no_pivot_col_0.connectLedger(module.io.in_x671_no_pivot_col_0)
    module.io.in_x581_ncol <> x581_ncol
  }
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x598_tab_sram_1 = list_x597_tab_sram_0(1)
  val x679_pivot_col_0 = list_x597_tab_sram_0(2)
  val x674_pivot_element_0 = list_x597_tab_sram_0(3)
  val x672_no_pivot_row_0 = list_x597_tab_sram_0(4)
  val x668_iter_0 = list_x597_tab_sram_0(5)
  val x675_pivot_row_min_ratio_0 = list_x597_tab_sram_0(6)
  val x680_pivot_row_0 = list_x597_tab_sram_0(7)
  val x673_no_pivot_row_count_0 = list_x597_tab_sram_0(8)
  val x671_no_pivot_col_0 = list_x597_tab_sram_0(9)
  val x582_ncons = list_x582_ncons(0)
  val x580_nrow = list_x582_ncons(1)
  val x581_ncol = list_x582_ncons(2)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1020_outr_FSM")
    implicit val stack = ControllerStack.stack.toList
    class x1020_outr_FSM_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1020_outr_FSM_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b47 = Wire(new FixedPoint(true, 32, 0)).suggestName("""b47""")
      b47.r := io.sigsIn.smState.r
      val x681 = Wire(Bool()).suggestName("""x681""")
      x681.r := Math.neq(b47, 4L.FP(true, 32, 0), Some(0.2), true.B,"x681").r
      val x682_reg = (new x682_reg).m.io.asInstanceOf[StandardInterface]
      val x683_reg = (new x683_reg).m.io.asInstanceOf[StandardInterface]
      val x684_reg = (new x684_reg).m.io.asInstanceOf[StandardInterface]
      val x685_reg = (new x685_reg).m.io.asInstanceOf[StandardInterface]
      val x692_inr_UnitPipe = new x692_inr_UnitPipe_kernel(List(b47), List(x683_reg,x682_reg,x685_reg,x684_reg) , Some(me), List(), 0, 1, 1, List(1), List(32), breakpoints, rr)
      x692_inr_UnitPipe.sm.io.ctrDone := risingEdge(x692_inr_UnitPipe.sm.io.ctrInc)
      x692_inr_UnitPipe.backpressure := true.B | x692_inr_UnitPipe.sm.io.doneLatch
      x692_inr_UnitPipe.forwardpressure := true.B | x692_inr_UnitPipe.sm.io.doneLatch
      x692_inr_UnitPipe.sm.io.enableOut.zip(x692_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x692_inr_UnitPipe.sm.io.break := false.B
      x692_inr_UnitPipe.mask := true.B & true.B
      x692_inr_UnitPipe.configure("x692_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x692_inr_UnitPipe.kernel()
      val x1085_rd_x682 = Wire(Bool()).suggestName("""x1085_rd_x682""")
      val x1085_rd_x682_banks = List[UInt]()
      val x1085_rd_x682_ofs = List[UInt]()
      val x1085_rd_x682_en = List[Bool](true.B)
      x1085_rd_x682.toSeq.zip(x682_reg.connectRPort(1085, x1085_rd_x682_banks, x1085_rd_x682_ofs, io.sigsIn.backpressure, x1085_rd_x682_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1086_rd_x684 = Wire(Bool()).suggestName("""x1086_rd_x684""")
      val x1086_rd_x684_banks = List[UInt]()
      val x1086_rd_x684_ofs = List[UInt]()
      val x1086_rd_x684_en = List[Bool](true.B)
      x1086_rd_x684.toSeq.zip(x684_reg.connectRPort(1086, x1086_rd_x684_banks, x1086_rd_x684_ofs, io.sigsIn.backpressure, x1086_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1006_outr_Switch_obj = new x1006_outr_Switch_kernel(List(x1086_rd_x684,x1085_rd_x682), List(b47), List(x683_reg,x597_tab_sram_0,x598_tab_sram_1,x682_reg,x679_pivot_col_0,x674_pivot_element_0,x672_no_pivot_row_0,x668_iter_0,x685_reg,x675_pivot_row_min_ratio_0,x680_pivot_row_0,x673_no_pivot_row_count_0,x671_no_pivot_col_0,x684_reg), List(x582_ncons,x580_nrow,x581_ncol) , Some(me), List(), 1, 2, 1, List(1), List(32), breakpoints, rr)
      val x756_outr_SwitchCase_switch_sel_reg = RegInit(false.B)
      x756_outr_SwitchCase_switch_sel_reg := Mux(risingEdge(x1006_outr_Switch_obj.en), x1085_rd_x682, x756_outr_SwitchCase_switch_sel_reg)
      x1006_outr_Switch_obj.sm.io.selectsIn(0) := x1085_rd_x682
      val x1005_outr_SwitchCase_switch_sel_reg = RegInit(false.B)
      x1005_outr_SwitchCase_switch_sel_reg := Mux(risingEdge(x1006_outr_Switch_obj.en), x1086_rd_x684, x1005_outr_SwitchCase_switch_sel_reg)
      x1006_outr_Switch_obj.sm.io.selectsIn(1) := x1086_rd_x684
      x1006_outr_Switch_obj.backpressure := true.B | x1006_outr_Switch_obj.sm.io.doneLatch
      x1006_outr_Switch_obj.forwardpressure := true.B | x1006_outr_Switch_obj.sm.io.doneLatch
      x1006_outr_Switch_obj.sm.io.enableOut.zip(x1006_outr_Switch_obj.smEnableOuts).foreach{case (l,r) => r := l}
      x1006_outr_Switch_obj.sm.io.break := false.B
      x1006_outr_Switch_obj.mask := true.B & true.B
      x1006_outr_Switch_obj.configure("x1006_outr_Switch_obj", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1006_outr_Switch_obj.kernel()
      val x1007_reg = (new x1007_reg).m.io.asInstanceOf[StandardInterface]
      val x1018_inr_UnitPipe = new x1018_inr_UnitPipe_kernel(List(b47), List(x1007_reg,x672_no_pivot_row_0,x668_iter_0,x671_no_pivot_col_0) , Some(me), List(), 2, 1, 1, List(1), List(32), breakpoints, rr)
      x1018_inr_UnitPipe.sm.io.ctrDone := risingEdge(x1018_inr_UnitPipe.sm.io.ctrInc)
      x1018_inr_UnitPipe.backpressure := true.B | x1018_inr_UnitPipe.sm.io.doneLatch
      x1018_inr_UnitPipe.forwardpressure := true.B | x1018_inr_UnitPipe.sm.io.doneLatch
      x1018_inr_UnitPipe.sm.io.enableOut.zip(x1018_inr_UnitPipe.smEnableOuts).foreach{case (l,r) => r := l}
      x1018_inr_UnitPipe.sm.io.break := false.B
      x1018_inr_UnitPipe.mask := true.B & true.B
      x1018_inr_UnitPipe.configure("x1018_inr_UnitPipe", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1018_inr_UnitPipe.kernel()
      val x1019_rd_x1007 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1019_rd_x1007""")
      val x1019_rd_x1007_banks = List[UInt]()
      val x1019_rd_x1007_ofs = List[UInt]()
      val x1019_rd_x1007_en = List[Bool](true.B)
      x1019_rd_x1007.toSeq.zip(x1007_reg.connectRPort(1019, x1019_rd_x1007_banks, x1019_rd_x1007_ofs, io.sigsIn.backpressure, x1019_rd_x1007_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      io.sigsOut.smNextState := x1019_rd_x1007.r.asSInt 
      io.sigsOut.smInitState := 0L.FP(true, 32, 0).r.asSInt
      io.sigsOut.smDoneCondition := ~x681
    }
    val module = Module(new x1020_outr_FSM_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END StateMachine x1020_outr_FSM **/
