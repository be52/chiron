package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x627 -> x667 -> x1134 -> x1137 **/
/** BEGIN None x627_inr_Foreach **/
class x627_inr_Foreach_kernel(
  list_x592_tab_dram: List[FixedPoint],
  list_x601_fifo: List[FIFOInterface],
  list_x600: List[DecoupledIO[AppCommandDense]],
  list_x581_ncol: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 10.8.toInt, myName = "x627_inr_Foreach_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(10.8.toInt, 2 + _root_.utils.math.log2Up(10.8.toInt), "x627_inr_Foreach_iiCtr"))
  
  abstract class x627_inr_Foreach_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x601_fifo = Flipped(new FIFOInterface(ModuleParams.getParams("x601_fifo_p").asInstanceOf[MemParams] ))
      val in_x600 = Decoupled(new AppCommandDense(ModuleParams.getParams("x600_p").asInstanceOf[(Int,Int)] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x592_tab_dram = Input(new FixedPoint(true, 64, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x601_fifo = {io.in_x601_fifo} ; io.in_x601_fifo := DontCare
    def x600 = {io.in_x600} 
    def x581_ncol = {io.in_x581_ncol} 
    def x592_tab_dram = {io.in_x592_tab_dram} 
  }
  def connectWires0(module: x627_inr_Foreach_module)(implicit stack: List[KernelHash]): Unit = {
    x601_fifo.connectLedger(module.io.in_x601_fifo)
    module.io.in_x600 <> x600
    module.io.in_x581_ncol <> x581_ncol
    module.io.in_x592_tab_dram <> x592_tab_dram
  }
  val x592_tab_dram = list_x592_tab_dram(0)
  val x601_fifo = list_x601_fifo(0)
  val x600 = list_x600(0)
  val x581_ncol = list_x581_ncol(0)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x627_inr_Foreach")
    implicit val stack = ControllerStack.stack.toList
    class x627_inr_Foreach_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x627_inr_Foreach_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val b605 = io.sigsIn.cchainOutputs.head.counts(0).FP(true, 32, 0); b605.suggestName("b605")
      val b606 = ~io.sigsIn.cchainOutputs.head.oobs(0); b606.suggestName("b606")
      val x607_rd_x581 = Wire(new FixedPoint(true, 32, 0))
      x607_rd_x581.r := x581_ncol.r
      val x608_mul = Wire(new FixedPoint(true, 32, 0)).suggestName("""x608_mul""")
      val ensig0 = Wire(Bool())
      ensig0 := (~x601_fifo.full | ~(x601_fifo.accessActivesOut(0))) & x600.ready
      x608_mul.r := (Math.mul(b605, x607_rd_x581, Some(6.0), ensig0, Truncate, Wrapping, "x608_mul")).r
      val x609 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x609""")
      x609.r := Math.arith_right_shift(x608_mul, 4, Some(0.2), ensig0,"x609").r
      val x610 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x610""")
      x610.r := Math.arith_left_shift(x609, 4, Some(0.2), ensig0,"x610").r
      val x1121 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1121""")
      x1121.r := Math.arith_left_shift(x609, 6, Some(0.2), ensig0,"x1121").r
      val x612_sub = Wire(new FixedPoint(true, 32, 0)).suggestName("""x612_sub""")
      x612_sub.r := Math.sub(x608_mul,x610,Some(1.0), ensig0, Truncate, Wrapping, "x612_sub").r
      val x1138 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1138_x607_rd_x581_D7") 
      x1138.r := getRetimed(x607_rd_x581.r, 7.toInt, io.sigsIn.backpressure)
      val x614_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x614_sum""")
      x614_sum.r := Math.add(x612_sub,x1138,Some(1.0), ensig0, Truncate, Wrapping, "x614_sum").r
      val x615_sum = Wire(new FixedPoint(true, 32, 0)).suggestName("""x615_sum""")
      x615_sum.r := Math.add(x614_sum,15L.FP(true, 32, 0),Some(1.0), ensig0, Truncate, Wrapping, "x615_sum").r
      val x616 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x616""")
      x616.r := Math.arith_right_shift(x615_sum, 4, Some(0.2), ensig0,"x616").r
      val x617 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x617""")
      x617.r := Math.arith_left_shift(x616, 4, Some(0.2), ensig0,"x617").r
      val x1122 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1122""")
      x1122.r := Math.arith_left_shift(x616, 6, Some(0.2), ensig0,"x1122").r
      val x619 = Wire(new FixedPoint(true, 64, 0)).suggestName("""x619""")
      x619.r := Math.fix2fix(x1121, true, 64, 0, Some(0.0), ensig0, Truncate, Wrapping, "x619").r
      val x620 = x592_tab_dram
      val x1139 = Wire(new FixedPoint(true, 64, 0)).suggestName("x1139_x620_D6") 
      x1139.r := getRetimed(x620.r, 6.toInt, io.sigsIn.backpressure)
      val x621_sum = Wire(new FixedPoint(true, 64, 0)).suggestName("""x621_sum""")
      x621_sum.r := Math.add(x619,x1139,Some(2.0), ensig0, Truncate, Wrapping, "x621_sum").r
      val x1140 = Wire(new FixedPoint(true, 64, 0)).suggestName("x1140_x621_sum_D1") 
      x1140.r := getRetimed(x621_sum.r, 1.toInt, io.sigsIn.backpressure)
      val x622_tuple = Wire(UInt(97.W)).suggestName("""x622_tuple""")
      x622_tuple.r := Cat(true.B,x1122.r,x1140.r)
      val x623 = true.B
      val x1141 = Wire(Bool()).suggestName("x1141_x623_D9") 
      x1141.r := getRetimed(x623.r, 9.toInt, io.sigsIn.backpressure)
      val x1142 = Wire(Bool()).suggestName("x1142_b606_D9") 
      x1142.r := getRetimed(b606.r, 9.toInt, io.sigsIn.backpressure)
      x600.valid := (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(9.8.toInt.toInt, rr, io.sigsIn.backpressure) & x1141&x1142 & io.sigsIn.backpressure
      x600.bits.addr := x622_tuple(63,0)
      x600.bits.size := x622_tuple(95,64)
      val x1143 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1143_x612_sub_D2") 
      x1143.r := getRetimed(x612_sub.r, 2.toInt, io.sigsIn.backpressure)
      val x1144 = Wire(new FixedPoint(true, 32, 0)).suggestName("x1144_x614_sum_D1") 
      x1144.r := getRetimed(x614_sum.r, 1.toInt, io.sigsIn.backpressure)
      val x625_tuple = Wire(UInt(96.W)).suggestName("""x625_tuple""")
      x625_tuple.r := Cat(x1144.r,x1143.r,x617.r)
      val x626_enq_x601_banks = List[UInt]()
      val x626_enq_x601_ofs = List[UInt]()
      val x626_enq_x601_en = List[Bool](true.B)
      val x626_enq_x601_data = List[UInt](x625_tuple.r)
      x601_fifo.connectWPort(626, x626_enq_x601_banks, x626_enq_x601_ofs, x626_enq_x601_data, x626_enq_x601_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(9.8.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && true.B & x1142))
      x601_fifo.connectAccessActivesIn(0, ((true.B & x1142)))
    }
    val module = Module(new x627_inr_Foreach_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnrolledForeach x627_inr_Foreach **/
