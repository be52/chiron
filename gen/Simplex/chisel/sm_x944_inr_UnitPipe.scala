package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x944 -> x982 -> x984 -> x985 -> x1135 -> x1136 -> x997 -> x999 -> x1000 -> x1002 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x944_inr_UnitPipe **/
class x944_inr_UnitPipe_kernel(
  list_b916: List[FixedPoint],
  list_x918_reg: List[NBufInterface],
  list_x597_tab_sram_0: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 9.0.toInt, myName = "x944_inr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x944_inr_UnitPipe_iiCtr"))
  
  abstract class x944_inr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x898_reg = Flipped(new StandardInterface(ModuleParams.getParams("x898_reg_p").asInstanceOf[MemParams] ))
      val in_b916 = Input(new FixedPoint(true, 32, 0))
      val in_x847_reg = Flipped(new StandardInterface(ModuleParams.getParams("x847_reg_p").asInstanceOf[MemParams] ))
      val in_x679_pivot_col_0 = Flipped(new StandardInterface(ModuleParams.getParams("x679_pivot_col_0_p").asInstanceOf[MemParams] ))
      val in_x674_pivot_element_0 = Flipped(new StandardInterface(ModuleParams.getParams("x674_pivot_element_0_p").asInstanceOf[MemParams] ))
      val in_x759_reg = Flipped(new StandardInterface(ModuleParams.getParams("x759_reg_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_x918_reg = Flipped(new NBufInterface(ModuleParams.getParams("x918_reg_p").asInstanceOf[NBufParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x898_reg = {io.in_x898_reg} ; io.in_x898_reg := DontCare
    def b916 = {io.in_b916} 
    def x847_reg = {io.in_x847_reg} ; io.in_x847_reg := DontCare
    def x679_pivot_col_0 = {io.in_x679_pivot_col_0} ; io.in_x679_pivot_col_0 := DontCare
    def x674_pivot_element_0 = {io.in_x674_pivot_element_0} ; io.in_x674_pivot_element_0 := DontCare
    def x759_reg = {io.in_x759_reg} ; io.in_x759_reg := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
    def x918_reg = {io.in_x918_reg} ; io.in_x918_reg := DontCare
  }
  def connectWires0(module: x944_inr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    x898_reg.connectLedger(module.io.in_x898_reg)
    module.io.in_b916 <> b916
    x847_reg.connectLedger(module.io.in_x847_reg)
    x679_pivot_col_0.connectLedger(module.io.in_x679_pivot_col_0)
    x674_pivot_element_0.connectLedger(module.io.in_x674_pivot_element_0)
    x759_reg.connectLedger(module.io.in_x759_reg)
    x684_reg.connectLedger(module.io.in_x684_reg)
    x918_reg.connectLedger(module.io.in_x918_reg)
  }
  val b916 = list_b916(0)
  val x918_reg = list_x918_reg(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x898_reg = list_x597_tab_sram_0(1)
  val x847_reg = list_x597_tab_sram_0(2)
  val x679_pivot_col_0 = list_x597_tab_sram_0(3)
  val x674_pivot_element_0 = list_x597_tab_sram_0(4)
  val x759_reg = list_x597_tab_sram_0(5)
  val x684_reg = list_x597_tab_sram_0(6)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x944_inr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x944_inr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x944_inr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x930_rd_x679 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x930_rd_x679""")
      val x930_rd_x679_banks = List[UInt]()
      val x930_rd_x679_ofs = List[UInt]()
      val x930_rd_x679_en = List[Bool](true.B)
      x930_rd_x679.toSeq.zip(x679_pivot_col_0.connectRPort(930, x930_rd_x679_banks, x930_rd_x679_ofs, io.sigsIn.backpressure, x930_rd_x679_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x931_rd_x918 = Wire(Bool()).suggestName("""x931_rd_x918""")
      val x931_rd_x918_banks = List[UInt]()
      val x931_rd_x918_ofs = List[UInt]()
      val x931_rd_x918_en = List[Bool](true.B)
      x931_rd_x918.toSeq.zip(x918_reg.connectRPort(931, x931_rd_x918_banks, x931_rd_x918_ofs, io.sigsIn.backpressure, x931_rd_x918_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x932_rd_x898 = Wire(Bool()).suggestName("""x932_rd_x898""")
      val x932_rd_x898_banks = List[UInt]()
      val x932_rd_x898_ofs = List[UInt]()
      val x932_rd_x898_en = List[Bool](true.B)
      x932_rd_x898.toSeq.zip(x898_reg.connectRPort(932, x932_rd_x898_banks, x932_rd_x898_ofs, io.sigsIn.backpressure, x932_rd_x898_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x933_rd_x847 = Wire(Bool()).suggestName("""x933_rd_x847""")
      val x933_rd_x847_banks = List[UInt]()
      val x933_rd_x847_ofs = List[UInt]()
      val x933_rd_x847_en = List[Bool](true.B)
      x933_rd_x847.toSeq.zip(x847_reg.connectRPort(933, x933_rd_x847_banks, x933_rd_x847_ofs, io.sigsIn.backpressure, x933_rd_x847_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x934_rd_x759 = Wire(Bool()).suggestName("""x934_rd_x759""")
      val x934_rd_x759_banks = List[UInt]()
      val x934_rd_x759_ofs = List[UInt]()
      val x934_rd_x759_en = List[Bool](true.B)
      x934_rd_x759.toSeq.zip(x759_reg.connectRPort(934, x934_rd_x759_banks, x934_rd_x759_ofs, io.sigsIn.backpressure, x934_rd_x759_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x935_rd_x684 = Wire(Bool()).suggestName("""x935_rd_x684""")
      val x935_rd_x684_banks = List[UInt]()
      val x935_rd_x684_ofs = List[UInt]()
      val x935_rd_x684_en = List[Bool](true.B)
      x935_rd_x684.toSeq.zip(x684_reg.connectRPort(935, x935_rd_x684_banks, x935_rd_x684_ofs, io.sigsIn.backpressure, x935_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x1127 = Wire(new FixedPoint(true, 32, 0)).suggestName("""x1127""")
      x1127.r := Math.fma(b916,10L.FP(true, 32, 0),x930_rd_x679,Some(6.0), true.B, "x1127").toFixed(x1127, "cast_x1127").r
      val x1189 = Wire(Bool()).suggestName("x1189_x934_rd_x759_D6") 
      x1189.r := getRetimed(x934_rd_x759.r, 6.toInt, io.sigsIn.backpressure)
      val x1190 = Wire(Bool()).suggestName("x1190_x933_rd_x847_D6") 
      x1190.r := getRetimed(x933_rd_x847.r, 6.toInt, io.sigsIn.backpressure)
      val x1191 = Wire(Bool()).suggestName("x1191_x932_rd_x898_D6") 
      x1191.r := getRetimed(x932_rd_x898.r, 6.toInt, io.sigsIn.backpressure)
      val x1192 = Wire(Bool()).suggestName("x1192_x931_rd_x918_D6") 
      x1192.r := getRetimed(x931_rd_x918.r, 6.toInt, io.sigsIn.backpressure)
      val x1193 = Wire(Bool()).suggestName("x1193_x935_rd_x684_D6") 
      x1193.r := getRetimed(x935_rd_x684.r, 6.toInt, io.sigsIn.backpressure)
      val x941_rd = Wire(Vec(1, new FloatingPoint(24, 8))).suggestName("""x941_rd""")
      val x941_rd_banks = List[UInt](0L.FP(true, 32, 0).r)
      val x941_rd_ofs = List[UInt](x1127.r)
      val x941_rd_en = List[Bool](true.B)
      x941_rd.toSeq.zip(x597_tab_sram_0.connectRPort(941, x941_rd_banks, x941_rd_ofs, io.sigsIn.backpressure, x941_rd_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(6.0.toInt, rr, io.sigsIn.backpressure) && x1192 & x1193 & x1190 & x1189 & x1191), true)).foreach{case (a,b) => a := b}
      val x942_elem_0 = Wire(new FloatingPoint(24, 8)).suggestName("""x942_elem_0""")
      x942_elem_0.r := x941_rd(0).r
      val x1194 = Wire(Bool()).suggestName("x1194_x934_rd_x759_D8") 
      x1194.r := getRetimed(x934_rd_x759.r, 8.toInt, io.sigsIn.backpressure)
      val x1195 = Wire(Bool()).suggestName("x1195_x933_rd_x847_D8") 
      x1195.r := getRetimed(x933_rd_x847.r, 8.toInt, io.sigsIn.backpressure)
      val x1196 = Wire(Bool()).suggestName("x1196_x932_rd_x898_D8") 
      x1196.r := getRetimed(x932_rd_x898.r, 8.toInt, io.sigsIn.backpressure)
      val x1197 = Wire(Bool()).suggestName("x1197_x931_rd_x918_D8") 
      x1197.r := getRetimed(x931_rd_x918.r, 8.toInt, io.sigsIn.backpressure)
      val x1198 = Wire(Bool()).suggestName("x1198_x935_rd_x684_D8") 
      x1198.r := getRetimed(x935_rd_x684.r, 8.toInt, io.sigsIn.backpressure)
      val x943_wr_x674_banks = List[UInt]()
      val x943_wr_x674_ofs = List[UInt]()
      val x943_wr_x674_en = List[Bool](true.B)
      val x943_wr_x674_data = List[UInt](x942_elem_0.r)
      x674_pivot_element_0.connectWPort(943, x943_wr_x674_banks, x943_wr_x674_ofs, x943_wr_x674_data, x943_wr_x674_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(8.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x1195 & x1198 & x1196 & x1194 & x1197))
    }
    val module = Module(new x944_inr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x944_inr_UnitPipe **/
