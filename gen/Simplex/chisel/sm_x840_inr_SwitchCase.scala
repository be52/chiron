package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x840 -> x842 -> x844 -> x1003 -> x1005 -> x1006 -> x1020 -> x1137 **/
/** BEGIN None x840_inr_SwitchCase **/
class x840_inr_SwitchCase_kernel(
  list_x825_reg: List[StandardInterface],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new InnerControl(Sequenced, false   , latency = 1.0.toInt, myName = "x840_inr_SwitchCase_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x840_inr_SwitchCase_iiCtr"))
  
  abstract class x840_inr_SwitchCase_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x825_reg = Flipped(new StandardInterface(ModuleParams.getParams("x825_reg_p").asInstanceOf[MemParams] ))
      val in_x757_reg = Flipped(new StandardInterface(ModuleParams.getParams("x757_reg_p").asInstanceOf[MemParams] ))
      val in_x672_no_pivot_row_0 = Flipped(new StandardInterface(ModuleParams.getParams("x672_no_pivot_row_0_p").asInstanceOf[MemParams] ))
      val in_x684_reg = Flipped(new StandardInterface(ModuleParams.getParams("x684_reg_p").asInstanceOf[MemParams] ))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x825_reg = {io.in_x825_reg} ; io.in_x825_reg := DontCare
    def x757_reg = {io.in_x757_reg} ; io.in_x757_reg := DontCare
    def x672_no_pivot_row_0 = {io.in_x672_no_pivot_row_0} ; io.in_x672_no_pivot_row_0 := DontCare
    def x684_reg = {io.in_x684_reg} ; io.in_x684_reg := DontCare
  }
  def connectWires0(module: x840_inr_SwitchCase_module)(implicit stack: List[KernelHash]): Unit = {
    x825_reg.connectLedger(module.io.in_x825_reg)
    x757_reg.connectLedger(module.io.in_x757_reg)
    x672_no_pivot_row_0.connectLedger(module.io.in_x672_no_pivot_row_0)
    x684_reg.connectLedger(module.io.in_x684_reg)
  }
  val x825_reg = list_x825_reg(0)
  val x757_reg = list_x825_reg(1)
  val x672_no_pivot_row_0 = list_x825_reg(2)
  val x684_reg = list_x825_reg(3)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x840_inr_SwitchCase_obj")
    implicit val stack = ControllerStack.stack.toList
    class x840_inr_SwitchCase_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x840_inr_SwitchCase_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      // Controller Stack: Stack(x842, x844, x1003, x1005, x1006, x1020, x1137)
      val x836_rd_x825 = Wire(Bool()).suggestName("""x836_rd_x825""")
      val x836_rd_x825_banks = List[UInt]()
      val x836_rd_x825_ofs = List[UInt]()
      val x836_rd_x825_en = List[Bool](true.B)
      x836_rd_x825.toSeq.zip(x825_reg.connectRPort(836, x836_rd_x825_banks, x836_rd_x825_ofs, io.sigsIn.backpressure, x836_rd_x825_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x837_rd_x757 = Wire(Bool()).suggestName("""x837_rd_x757""")
      val x837_rd_x757_banks = List[UInt]()
      val x837_rd_x757_ofs = List[UInt]()
      val x837_rd_x757_en = List[Bool](true.B)
      x837_rd_x757.toSeq.zip(x757_reg.connectRPort(837, x837_rd_x757_banks, x837_rd_x757_ofs, io.sigsIn.backpressure, x837_rd_x757_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x838_rd_x684 = Wire(Bool()).suggestName("""x838_rd_x684""")
      val x838_rd_x684_banks = List[UInt]()
      val x838_rd_x684_ofs = List[UInt]()
      val x838_rd_x684_en = List[Bool](true.B)
      x838_rd_x684.toSeq.zip(x684_reg.connectRPort(838, x838_rd_x684_banks, x838_rd_x684_ofs, io.sigsIn.backpressure, x838_rd_x684_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) && true.B), true)).foreach{case (a,b) => a := b}
      val x839_wr_x672_banks = List[UInt]()
      val x839_wr_x672_ofs = List[UInt]()
      val x839_wr_x672_en = List[Bool](true.B)
      val x839_wr_x672_data = List[UInt](true.B.r)
      x672_no_pivot_row_0.connectWPort(839, x839_wr_x672_banks, x839_wr_x672_ofs, x839_wr_x672_data, x839_wr_x672_en.map(_ && ~io.sigsIn.break && (io.sigsIn.datapathEn & io.sigsIn.iiDone).DS(0.0.toInt, rr, io.sigsIn.backpressure) & ~io.sigsIn.break && io.sigsIn.backpressure && x838_rd_x684 & x837_rd_x757 & x836_rd_x825))
    }
    val module = Module(new x840_inr_SwitchCase_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END SwitchCase x840_inr_SwitchCase **/
