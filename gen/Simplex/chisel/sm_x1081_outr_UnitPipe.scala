package accel
import fringe._
import fringe.templates.memory._
import fringe.templates._
import fringe.Ledger._
import fringe.utils._
import fringe.utils.implicits._
import fringe.templates.math._
import fringe.templates.counters._
import fringe.templates.vector._
import fringe.templates.axi4._
import fringe.SpatialBlocks._
import fringe.templates.memory._
import fringe.templates.memory.implicits._
import fringe.templates.retiming._
import emul.ResidualGenerator._
import fringe.templates.euresys._
import api._
import chisel3._
import chisel3.util._
import Args._
import scala.collection.immutable._

/** Hierarchy: x1081 -> x1137 **/
/** BEGIN None x1081_outr_UnitPipe **/
class x1081_outr_UnitPipe_kernel(
  list_x596_tab_dram_out: List[FixedPoint],
  list_x1024: List[DecoupledIO[Bool]],
  list_x597_tab_sram_0: List[StandardInterface],
  list_x1023: List[DecoupledIO[AppStoreData]],
  list_x1022: List[DecoupledIO[AppCommandDense]],
  list_x580_nrow: List[UInt],
  parent: Option[Kernel], cchain: List[CounterChainInterface], childId: Int, nMyChildren: Int, ctrcopies: Int, ctrPars: List[Int], ctrWidths: List[Int], breakpoints: Vec[Bool], rr: Bool
) extends Kernel(parent, cchain, childId, nMyChildren, ctrcopies, ctrPars, ctrWidths) {
  
  val me = this
  val sm = Module(new OuterControl(Streaming, 1, isFSM = false   , latency = 0.0.toInt, myName = "x1081_outr_UnitPipe_sm")); sm.io <> DontCare
  val iiCtr = Module(new IICounter(1.0.toInt, 2 + _root_.utils.math.log2Up(1.0.toInt), "x1081_outr_UnitPipe_iiCtr"))
  
  abstract class x1081_outr_UnitPipe_module(depth: Int)(implicit stack: List[KernelHash]) extends Module {
    val io = IO(new Bundle {
      val in_x1024 = Flipped(Decoupled(Bool()))
      val in_x597_tab_sram_0 = Flipped(new StandardInterface(ModuleParams.getParams("x597_tab_sram_0_p").asInstanceOf[MemParams] ))
      val in_x1022 = Decoupled(new AppCommandDense(ModuleParams.getParams("x1022_p").asInstanceOf[(Int,Int)] ))
      val in_x580_nrow = Input(UInt(64.W))
      val in_x1023 = Decoupled(new AppStoreData(ModuleParams.getParams("x1023_p").asInstanceOf[(Int,Int)] ))
      val in_x581_ncol = Input(UInt(64.W))
      val in_x596_tab_dram_out = Input(new FixedPoint(true, 64, 0))
      val in_breakpoints = Vec(api.numArgOuts_breakpts, Output(Bool()))
      val sigsIn = Input(new InputKernelSignals(1, 1, List(1), List(32)))
      val sigsOut = Output(new OutputKernelSignals(1, 1))
      val rr = Input(Bool())
    })
    def x1024 = {io.in_x1024} 
    def x597_tab_sram_0 = {io.in_x597_tab_sram_0} ; io.in_x597_tab_sram_0 := DontCare
    def x1022 = {io.in_x1022} 
    def x580_nrow = {io.in_x580_nrow} 
    def x1023 = {io.in_x1023} 
    def x581_ncol = {io.in_x581_ncol} 
    def x596_tab_dram_out = {io.in_x596_tab_dram_out} 
  }
  def connectWires0(module: x1081_outr_UnitPipe_module)(implicit stack: List[KernelHash]): Unit = {
    module.io.in_x1024 <> x1024
    x597_tab_sram_0.connectLedger(module.io.in_x597_tab_sram_0)
    module.io.in_x1022 <> x1022
    module.io.in_x580_nrow <> x580_nrow
    module.io.in_x1023 <> x1023
    module.io.in_x581_ncol <> x581_ncol
    module.io.in_x596_tab_dram_out <> x596_tab_dram_out
  }
  val x596_tab_dram_out = list_x596_tab_dram_out(0)
  val x1024 = list_x1024(0)
  val x597_tab_sram_0 = list_x597_tab_sram_0(0)
  val x1023 = list_x1023(0)
  val x1022 = list_x1022(0)
  val x580_nrow = list_x580_nrow(0)
  val x581_ncol = list_x580_nrow(1)
  def kernel(): Unit = {
    Ledger.enter(this.hashCode, "x1081_outr_UnitPipe")
    implicit val stack = ControllerStack.stack.toList
    class x1081_outr_UnitPipe_concrete(depth: Int)(implicit stack: List[KernelHash]) extends x1081_outr_UnitPipe_module(depth) {
      io.sigsOut := DontCare
      val breakpoints = io.in_breakpoints; breakpoints := DontCare
      val rr = io.rr
      val x1119_rd_x580 = Wire(new FixedPoint(true, 32, 0))
      x1119_rd_x580.r := x580_nrow.r
      val x1025_ctr = new CtrObject(Left(Some(0)), Right(x1119_rd_x580), Left(Some(1)), 1, 32, false)
      val x1026_ctrchain = (new CChainObject(List[CtrObject](x1025_ctr), "x1026_ctrchain")).cchain.io 
      x1026_ctrchain.setup.isStream := false.B
      ModuleParams.addParams("x1026_ctrchain_p", (x1026_ctrchain.par, x1026_ctrchain.widths))
      val x1080_outr_Foreach = new x1080_outr_Foreach_kernel(List(x596_tab_dram_out), List(x1024), List(x597_tab_sram_0), List(x581_ncol), List(x1023), List(x1022) , Some(me), List(x1026_ctrchain), 0, 2, 1, List(1), List(32), breakpoints, rr)
      x1080_outr_Foreach.sm.io.ctrDone := (x1080_outr_Foreach.cchain.head.output.done).DS(1.toInt, rr, io.sigsIn.backpressure)
      x1080_outr_Foreach.backpressure := true.B | x1080_outr_Foreach.sm.io.doneLatch
      x1080_outr_Foreach.forwardpressure := true.B | x1080_outr_Foreach.sm.io.doneLatch
      x1080_outr_Foreach.sm.io.enableOut.zip(x1080_outr_Foreach.smEnableOuts).foreach{case (l,r) => r := l}
      x1080_outr_Foreach.sm.io.break := false.B
      x1080_outr_Foreach.mask := ~x1080_outr_Foreach.cchain.head.output.noop & true.B
      x1080_outr_Foreach.configure("x1080_outr_Foreach", Some(io.sigsIn), Some(io.sigsOut), isSwitchCase = false)
      x1080_outr_Foreach.kernel()
    }
    val module = Module(new x1081_outr_UnitPipe_concrete(sm.p.depth)); module.io := DontCare
    connectWires0(module)
    Ledger.connectBreakpoints(breakpoints, module.io.in_breakpoints)
    module.io.rr := rr
    module.io.sigsIn := me.sigsIn
    me.sigsOut := module.io.sigsOut
    Ledger.exit()
  }
}
/** END UnitPipe x1081_outr_UnitPipe **/
