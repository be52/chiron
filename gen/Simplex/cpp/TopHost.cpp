#include "structs.hpp"
#include <stdint.h>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <string> 
#include <sstream> 
#include <stdarg.h>
#include <signal.h>
#include <sys/wait.h>
#include <pwd.h>
#include <map>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "FringeContext.h"
#include "functions.hpp"
#include "ArgAPI.hpp"
#include "Fixed.hpp"
#include <vector>
using std::vector;

#ifndef ZYNQ
typedef __int128 int128_t;
#endif

void printHelp(); 

using namespace std;

void Application(int numThreads, vector<string> * args) {
  // Create an execution context.
  FringeContext *c1 = new FringeContext("./verilog/accel.bit.bin");
  c1->load();
  vector<string>* x568 = args;
  string x569;
  try {
    x569 = (*x568).at(0);
  }
  catch (std::out_of_range& e) {
    printHelp();
  }
  std::ifstream x570_file (x569);
  assert(x570_file.good() && "File x569 does not exist"); 
  std::vector<string>* x571 = new std::vector<string>; 
  if (x570_file.is_open()) {
    while ( x570_file.good() ) {
      string x571_line;
      getline (x570_file, x571_line);
      string x571_delim = string(",");
      size_t x571_pos = 0;
      while (x571_line.find(x571_delim) != std::string::npos | x571_line.length() > 0) {
        if (x571_line.find(x571_delim) != std::string::npos) {
          x571_pos = x571_line.find(x571_delim);
        } else {
          x571_pos = x571_line.length();
        }
        string x571_token = x571_line.substr(0, x571_pos);
        x571_line.erase(0, x571_pos + x571_delim.length());
        x571->push_back(x571_token);
      }
    }
  }
  x570_file.clear();
  x570_file.seekg(0, x570_file.beg);
  std::vector<string>* x572 = new std::vector<string>; 
  if (x570_file.is_open()) {
    while ( x570_file.good() ) {
      string x572_line;
      getline (x570_file, x572_line);
      string x572_delim = string("\n");
      size_t x572_pos = 0;
      while (x572_line.find(x572_delim) != std::string::npos | x572_line.length() > 0) {
        if (x572_line.find(x572_delim) != std::string::npos) {
          x572_pos = x572_line.find(x572_delim);
        } else {
          x572_pos = x572_line.length();
        }
        string x572_token = x572_line.substr(0, x572_pos);
        x572_line.erase(0, x572_pos + x572_delim.length());
        x572->push_back(x572_token);
      }
    }
  }
  x570_file.clear();
  x570_file.seekg(0, x570_file.beg);
  x570_file.close();
  vector<float>* x576 = new vector<float>((*x571).size());
  for (int b6 = 0; b6 < (*x571).size(); b6++) { 
    string x574 = (*x571)[b6];
    float x575 = std::stof(x574);
    (*x576)[b6] = x575;
  }
  int32_t x577 = (*x572).size();
  int32_t x578 = (*x571).size();
  int32_t x579 = x578 / x577;
  int32_t x580 = 0;
  int32_t x581 = 0;
  int32_t x582 = 0;
  int32_t x583 = 0;
  c1->setArg(NROW_arg, x577, false);
  x580 = x577;
  c1->setArg(NCOL_arg, x579, false);
  x581 = x579;
  int32_t x586 = x577 - 1;
  c1->setArg(NCONS_arg, x586, false);
  x582 = x586;
  int32_t x588 = x579 - x577;
  c1->setArg(NCOEFS_arg, x588, false);
  x583 = x588;
  int32_t x590 = x580;
  int32_t x591 = x581;
  uint64_t x592 = c1->malloc(sizeof(float) * x590*x591);
  c1->setArg(TAB_DRAM_ptr, x592, false);
  printf("Allocate mem of size x590*x591 at %p\n", (void*)x592);
  c1->memcpy(x592, &(*x576)[0], (*x576).size() * sizeof(float));
  int32_t x594 = x580;
  int32_t x595 = x581;
  uint64_t x596 = c1->malloc(sizeof(float) * x594*x595);
  c1->setArg(TAB_DRAM_OUT_ptr, x596, false);
  printf("Allocate mem of size x594*x595 at %p\n", (void*)x596);
// Register ArgIns and ArgIOs in case some are unused
c1->setNumArgIns(4 + 2 + 0);
c1->setNumArgIOs(0);
c1->setNumArgOuts(0);
c1->setNumArgOutInstrs(0);
c1->setNumEarlyExits(0);
c1->flushCache(1024);
time_t tstart = time(0);
c1->run();
time_t tend = time(0);
double elapsed = difftime(tend, tstart);
std::cout << "Kernel done, test run time = " << elapsed << " ms" << std::endl;
c1->flushCache(1024);
delete c1;
}

void printHelp() {
fprintf(stderr, "Help for app: Simplex\n");
fprintf(stderr, "  -- Args:    <0: file>\n");
exit(1);
}

int main(int argc, char *argv[]) {
vector<string> *args = new vector<string>(argc-1);
for (int i=1; i<argc; i++) {
(*args)[i-1] = std::string(argv[i]);
if (std::string(argv[i]) == "--help" | std::string(argv[i]) == "-h") {printHelp();}
}
int numThreads = 1;
char *env_threads = getenv("DELITE_NUM_THREADS");
if (env_threads != NULL) { numThreads = atoi(env_threads); } else {
  fprintf(stderr, "[WARNING]: DELITE_NUM_THREADS undefined, defaulting to 1\n");
}
fprintf(stderr, "Executing with %d thread(s)\n", numThreads);
Application(numThreads, args);
return 0;
}
