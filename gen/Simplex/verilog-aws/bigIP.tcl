
## Integer Multiplier
create_ip -name mult_gen -vendor xilinx.com -library ip -version 12.0 -module_name mul_32_32_32_6_Unsigned_Use_Mults
set_property -dict [list CONFIG.MultType {Parallel_Multiplier} CONFIG.PortAType {Unsigned}  CONFIG.PortAWidth {32} CONFIG.PortBType {Unsigned} CONFIG.PortBWidth {32} CONFIG.Multiplier_Construction {Use_Mults} CONFIG.OptGoal {Speed} CONFIG.Use_Custom_Output_Width {true} CONFIG.OutputWidthHigh {32} CONFIG.PipeStages {6} CONFIG.ClockEnable {true}] [get_ips mul_32_32_32_6_Unsigned_Use_Mults]
set_property -dict [list CONFIG.clk_intf.FREQ_HZ $CLOCK_FREQ_HZ] [get_ips mul_32_32_32_6_Unsigned_Use_Mults]
set_property generate_synth_checkpoint false [get_files mul_32_32_32_6_Unsigned_Use_Mults.xci]
generate_target {all} [get_ips mul_32_32_32_6_Unsigned_Use_Mults]



## flt
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name CompareLT_8_24
set_property -dict [list CONFIG.Operation_Type {Compare} CONFIG.Has_ACLKEN {true}  CONFIG.C_Compare_Operation {Less_Than} CONFIG.A_Precision_Type {Custom} CONFIG.Flow_Control {NonBlocking} CONFIG.C_A_Exponent_Width {8} CONFIG.C_A_Fraction_Width {24} CONFIG.Result_Precision_Type {Custom} CONFIG.C_Result_Exponent_Width {1} CONFIG.C_Result_Fraction_Width {0} CONFIG.C_Mult_Usage {No_Usage} CONFIG.Has_RESULT_TREADY {false} CONFIG.C_Latency {2} CONFIG.C_Rate {1}] [get_ips CompareLT_8_24]
set_property generate_synth_checkpoint false [get_files CompareLT_8_24.xci]
generate_target {all} [get_ips CompareLT_8_24]



## fle
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name CompareLE_8_24
set_property -dict [list CONFIG.Operation_Type {Compare} CONFIG.Has_ACLKEN {true}  CONFIG.C_Compare_Operation {Less_Than_Or_Equal} CONFIG.A_Precision_Type {Custom} CONFIG.Flow_Control {NonBlocking} CONFIG.C_A_Exponent_Width {8} CONFIG.C_A_Fraction_Width {24} CONFIG.Result_Precision_Type {Custom} CONFIG.C_Result_Exponent_Width {1} CONFIG.C_Result_Fraction_Width {0} CONFIG.C_Mult_Usage {No_Usage} CONFIG.Has_RESULT_TREADY {false} CONFIG.C_Latency {2} CONFIG.C_Rate {1}] [get_ips CompareLE_8_24]
set_property generate_synth_checkpoint false [get_files CompareLE_8_24.xci]
generate_target {all} [get_ips CompareLE_8_24]



## fdiv
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name Divide_8_24_8_24
set_property -dict [list CONFIG.Operation_Type {Divide} CONFIG.Has_ACLKEN {true}  CONFIG.A_Precision_Type {Custom} CONFIG.Flow_Control {NonBlocking} CONFIG.C_A_Exponent_Width {8} CONFIG.C_A_Fraction_Width {24} CONFIG.Result_Precision_Type {Custom} CONFIG.C_Result_Exponent_Width {8} CONFIG.C_Result_Fraction_Width {24} CONFIG.C_Mult_Usage {No_Usage} CONFIG.Has_RESULT_TREADY {false} CONFIG.C_Latency {28} CONFIG.C_Rate {1}] [get_ips Divide_8_24_8_24]
set_property generate_synth_checkpoint false [get_files Divide_8_24_8_24.xci]
generate_target {all} [get_ips Divide_8_24_8_24]



## fmul
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name Multiply_8_24_8_24
set_property -dict [list CONFIG.Operation_Type {Multiply} CONFIG.Has_ACLKEN {true}  CONFIG.A_Precision_Type {Custom} CONFIG.C_Mult_Usage {Full_Usage} CONFIG.Flow_Control {NonBlocking} CONFIG.C_A_Exponent_Width {8} CONFIG.C_A_Fraction_Width {24} CONFIG.Result_Precision_Type {Custom} CONFIG.C_Result_Exponent_Width {8} CONFIG.C_Result_Fraction_Width {24} CONFIG.Has_RESULT_TREADY {false} CONFIG.C_Latency {8} CONFIG.C_Rate {1}] [get_ips Multiply_8_24_8_24]
set_property generate_synth_checkpoint false [get_files Multiply_8_24_8_24.xci]
generate_target {all} [get_ips Multiply_8_24_8_24]



## fsub
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name Subtract_8_24_8_24
set_property -dict [list CONFIG.Add_Sub_Value {Subtract} CONFIG.Has_ACLKEN {true}  CONFIG.A_Precision_Type {Custom} CONFIG.Flow_Control {NonBlocking} CONFIG.Has_RESULT_TREADY {false} CONFIG.C_A_Exponent_Width {8} CONFIG.C_A_Fraction_Width {24} CONFIG.Result_Precision_Type {Custom} CONFIG.C_Result_Exponent_Width {8} CONFIG.C_Result_Fraction_Width {24} CONFIG.C_Mult_Usage {No_Usage} CONFIG.C_Latency {12}] [get_ips Subtract_8_24_8_24]
set_property generate_synth_checkpoint false [get_files Subtract_8_24_8_24.xci]
generate_target {all} [get_ips Subtract_8_24_8_24]


