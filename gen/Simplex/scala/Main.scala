import emul._
import emul.implicits._

object Main {
  def main(args: Array[String]): Unit = {
    OOB.open()
    object Block1Chunker0 { // 49 nodes, 49 weight
      def gen(): Map[String, Any] = {
        val x687 = args
        if (args.contains("--help") || args.contains("-h")) { Main.printHelp() }
        val x688 = if (TRUE) System.out.print("------------------------- Host -------------------------\n")
        // Commandline argument #FixedPoint(BigDecimal("0"),FixFormat(true,32,0)) (file)
        val x689 = try{ x687.apply(FixedPoint(BigDecimal("0"),FixFormat(true,32,0))) }
        catch {case _:Throwable =>
          println("Missing argument " + FixedPoint(BigDecimal("0"),FixFormat(true,32,0)) + " ('file')")
          Main.printHelp()
          sys.exit(-1)
        }
        val x690 = {
          val file = new java.io.File(x689)
          if (false) { // Will write to file
            val writer = new java.io.PrintWriter(file)
            writer.print("")
          }
          file
        }
        val x691 = {
          val scanner = new java.util.Scanner(x690)
          val tokens = new scala.collection.mutable.ArrayBuffer[String]() 
          scanner.useDelimiter("\\s*" + "," + "\\s*|\\s*\n\\s*")
          while (scanner.hasNext) {
            tokens += scanner.next.trim
          }
          scanner.close()
          tokens.toArray
        }
        val x692 = {
          val scanner = new java.util.Scanner(x690)
          val tokens = new scala.collection.mutable.ArrayBuffer[String]() 
          scanner.useDelimiter("\\s*" + "\n" + "\\s*|\\s*\n\\s*")
          while (scanner.hasNext) {
            tokens += scanner.next.trim
          }
          scanner.close()
          tokens.toArray
        }
        val x696 = Array.tabulate(x691.length){bbb => 
          val b7 = FixedPoint.fromInt(bbb)
          val x694 = x691.apply(b7)
          val x695 = FloatPoint(x694, FltFormat(23,8))
          x695
        }
        val x697 = FixedPoint.fromInt(x692.length)
        val x698 = FixedPoint.fromInt(x691.length)
        val x699_div = x698 / x697
        val x700 = "Starting Tableau: " + "\n"
        val x701 = if (TRUE) System.out.print(x700)
        val x710 = for (b17 <- FixedPoint(BigDecimal("0"),FixFormat(true,32,0)) until x697 by FixedPoint(BigDecimal("1"),FixFormat(true,32,0))) {
          val x708 = for (b18 <- FixedPoint(BigDecimal("0"),FixFormat(true,32,0)) until x699_div by FixedPoint(BigDecimal("1"),FixFormat(true,32,0))) {
            val x702_mul = b17 * x699_div
            val x703_sum = x702_mul + b18
            val x704 = x696.apply(x703_sum)
            val x705 = x704.toString
            val x706 = x705 + "\t"
            val x707 = if (TRUE) System.out.print(x706)
          }
          val x709 = if (TRUE) System.out.print("\n")
        }
        x711_nrow.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
        x712_ncol.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
        x713_ncons.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
        x714_ncoefs.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
        val x715_set_x711 = x711_nrow.set(x697)
        val x716_set_x712 = x712_ncol.set(x699_div)
        val x717_sub = x697 - FixedPoint(BigDecimal("1"),FixFormat(true,32,0))
        val x718_set_x713 = x713_ncons.set(x717_sub)
        val x719_sub = x699_div - x697
        val x720_set_x714 = x714_ncoefs.set(x719_sub)
        val x721_rd_x714 = x714_ncoefs.value
        val x722 = x721_rd_x714.toString
        val x723 = "Number of coefficients: " + x722 + ""
        val x724 = x723 + "\n"
        val x725 = if (TRUE) System.out.print(x724)
        val x726_rd_x713 = x713_ncons.value
        val x727 = x726_rd_x713.toString
        val x728 = "Number of constraints: " + x727 + ""
        val x729 = x728 + "\n"
        val x730 = if (TRUE) System.out.print(x729)
        val x731_rd_x711 = x711_nrow.value
        val x732_rd_x712 = x712_ncol.value
        x733_tab_dram.initMem(x731_rd_x711*x732_rd_x712 + 16,FloatPoint("-0.0", FltFormat(23,8)))
        val x734 = {
          for (i <- 0 until x696.length) {
            try {
              try {
                x733_tab_dram(i) = x696(i)
              }
              catch {case err: java.lang.ArrayIndexOutOfBoundsException => 
                System.out.println("[warn] Simplex.scala:33:11 Memory x696: Out of bounds read at address " + err.getMessage)
                FloatPoint.invalid(FltFormat(23,8))
              }
            }
            catch {case err: java.lang.ArrayIndexOutOfBoundsException => 
              System.out.println("[warn] Simplex.scala:33:11 Memory tab_dram: Out of bounds write at address " + err.getMessage)
            }
          }
        }
        val x735_rd_x711 = x711_nrow.value
        val x736_rd_x712 = x712_ncol.value
        x737_tab_dram_out.initMem(x735_rd_x711*x736_rd_x712 + 16,FloatPoint("-0.0", FltFormat(23,8)))
        val x738 = if (TRUE) System.out.print("\n")
        val x1374_outr_RootController = x1374_outr_RootController_kernel.run()
        val x1302 = if (TRUE) System.out.print("\n")
        val x1303 = if (TRUE) System.out.print("------------------------- Host -------------------------\n")
        val x1304_mul = x735_rd_x711 * x736_rd_x712
        val x1305 = new Array[FloatPoint](x1304_mul)
        val x1306 = {
          for (i <- 0 until x1305.length) {
            try {
              try {
                x1305(i) = x737_tab_dram_out(i)
              }
              catch {case err: java.lang.ArrayIndexOutOfBoundsException => 
                System.out.println("[warn] Simplex.scala:160:26 Memory tab_dram_out: Out of bounds read at address " + err.getMessage)
                FloatPoint.invalid(FltFormat(23,8))
              }
            }
            catch {case err: java.lang.ArrayIndexOutOfBoundsException => 
              System.out.println("[warn] Simplex.scala:160:26 Memory x1305: Out of bounds write at address " + err.getMessage)
            }
          }
        }
        val x1307 = "Ending Tableau: " + "\n"
        Map[String,Any]("x735_rd_x711" -> x735_rd_x711, "x736_rd_x712" -> x736_rd_x712, "x1305" -> x1305, "x1307" -> x1307)
      }
    }
    val block1chunk0: Map[String, Any] = Block1Chunker0.gen()
    object Block1Chunker1 { // 2 nodes, 2 weight
      def gen(): Map[String, Any] = {
        val x1308 = if (TRUE) System.out.print(block1chunk0("x1307").asInstanceOf[String])
        val x1317 = for (b262 <- FixedPoint(BigDecimal("0"),FixFormat(true,32,0)) until block1chunk0("x735_rd_x711").asInstanceOf[FixedPoint] by FixedPoint(BigDecimal("1"),FixFormat(true,32,0))) {
          val x1315 = for (b263 <- FixedPoint(BigDecimal("0"),FixFormat(true,32,0)) until block1chunk0("x736_rd_x712").asInstanceOf[FixedPoint] by FixedPoint(BigDecimal("1"),FixFormat(true,32,0))) {
            val x1309_mul = b262 * block1chunk0("x736_rd_x712").asInstanceOf[FixedPoint]
            val x1310_sum = x1309_mul + b263
            val x1311 = block1chunk0("x1305").asInstanceOf[Array[FloatPoint]].apply(x1310_sum)
            val x1312 = x1311.toString
            val x1313 = x1312 + "\t"
            val x1314 = if (TRUE) System.out.print(x1313)
          }
          val x1316 = if (TRUE) System.out.print("\n")
        }
        Map[String,Any]()
      }
    }
    val block1chunk1: Map[String, Any] = Block1Chunker1.gen()
    OOB.close()
  }
  def printHelp(): Unit = {
    System.out.print("Help for app: Simplex\n")
    System.out.print("  -- Args:    <0: file>\n");
    System.exit(0);
  }
}
