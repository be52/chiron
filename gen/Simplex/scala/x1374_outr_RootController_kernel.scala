import emul._
import emul.implicits._

/** BEGIN AccelScope x1374_outr_RootController **/
object x1374_outr_RootController_kernel {
  def run(
  ): Unit = {
    try {
      x812_iter_0.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
      x815_no_pivot_col_0.initMem(Bool(false,true))
      x816_no_pivot_row_0.initMem(Bool(false,true))
      x817_no_pivot_row_count_0.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
      x818_no_pivot_row_count_1.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
      x819_pivot_element_0.initMem(FloatPoint("-0.0", FltFormat(23,8)))
      x820_pivot_row_min_ratio_0.initMem(FloatPoint("-0.0", FltFormat(23,8)))
      val x1371 = x1371_kernel.run()
      x824_pivot_col_0.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
      x825_pivot_row_0.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
      val x1240_outr_FSM = x1240_outr_FSM_kernel.run()
      val x1301_outr_UnitPipe = x1301_outr_UnitPipe_kernel.run()
    }
    catch {
      case x: Exception if x.getMessage == "exit" =>  
      case t: Throwable => throw t
    }
  } 
}
/** END AccelScope x1374_outr_RootController **/
