import emul._
import emul.implicits._

object x1244 extends StreamIn[Bool]("defined at Simplex.scala:156:38", {str => 
  val x = Bool(str.toBoolean, true)
  x
})
