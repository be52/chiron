import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x1294_inr_Foreach **/
object x1294_inr_Foreach_kernel {
  def run(
    b1247: FixedPoint,
    x1276_ctrchain: Array[Counterlike]
  ): Unit = {
    x1276_ctrchain(0).foreach{case (is,vs) => 
      val b1277 = is(0)
      val b1278 = vs(0)
      val x1279_rd_x1249 = x1249_reg.value
      val x1280 = x1279_rd_x1249 <= b1277
      val x1281_rd_x1250 = x1250_reg.value
      val x1282 = b1277 < x1281_rd_x1250
      val x1283 = x1280 && x1282
      val x1284_sub = b1277 - x1279_rd_x1249
      val x1370 = (b1247 * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))) + x1284_sub
      val x1290_rd = x741_tab_sram_0.apply("MemoryDealiasing.scala:32:17", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1370), Seq(x1283 & Bool(true,true) & b1278))
      val x1291_elem_0 = x1290_rd.apply(0)
      val x1292_tuple: Struct3 = new Struct3(x1291_elem_0, x1283)
      val x1293 = {
        if (b1278) x1243.enqueue(x1292_tuple)
      }
    }
  } 
}
/** END UnrolledForeach x1294_inr_Foreach **/
