import emul._
import emul.implicits._

object x746 extends StreamIn[FloatPoint]("defined at Simplex.scala:49:16", {str => 
  val x = FloatPoint(str, FltFormat(23,8))
  x
})
