import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x809_inr_Foreach **/
object x809_inr_Foreach_kernel {
  def run(
    b776: Bool,
    x791_ctrchain: Array[Counterlike],
    b775: FixedPoint
  ): Unit = if (b776){
    x791_ctrchain(0).foreach{case (is,vs) => 
      val b792 = is(0)
      val b793 = vs(0)
      val x794_rd_x777 = x777_reg.value
      val x795 = x794_rd_x777 <= b792
      val x796_rd_x778 = x778_reg.value
      val x797 = b792 < x796_rd_x778
      val x798 = x795 && x797
      val x799_sub = b792 - x794_rd_x777
      val x800 = {
        val a0 = if (b793 & b776 && x746.nonEmpty) x746.dequeue() else FloatPoint.invalid(FltFormat(23,8))
        Array[FloatPoint](a0)
      }
      val x801_elem_0 = x800.apply(0)
      val x1359 = (b775 * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))) + x799_sub
      val x807_wr = x741_tab_sram_0.update("Simplex.scala:49:16", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1359), Seq(x798 & b793 & b776), Seq(x801_elem_0))
      val x808_wr = x742_tab_sram_1.update("Simplex.scala:49:16", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1359), Seq(x798 & b793 & b776), Seq(x801_elem_0))
    }
  } else null.asInstanceOf[Unit]
}
/** END UnrolledForeach x809_inr_Foreach **/
