import emul._
import emul.implicits._

/** BEGIN UnitPipe x823_inr_UnitPipe **/
object x823_inr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x821_wr_x815 = if (TRUE) x815_no_pivot_col_0.set(Bool(false,true))
    val x822_wr_x816 = if (TRUE) x816_no_pivot_row_0.set(Bool(false,true))
  } 
}
/** END UnitPipe x823_inr_UnitPipe **/
