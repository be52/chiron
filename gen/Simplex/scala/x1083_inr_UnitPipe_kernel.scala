import emul._
import emul.implicits._

/** BEGIN UnitPipe x1083_inr_UnitPipe **/
object x1083_inr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x1067 = "Step 4. Pivot the pivot row"
    val x1068 = x1067 + "\n"
    val x1069_rd_x1054 = x1054_reg.value
    val x1070_rd_x931 = x931_reg.value
    val x1071_rd_x829 = x829_reg.value
    val x1072 = if (x1071_rd_x829 & x1070_rd_x931 & x1069_rd_x1054) System.out.print(x1068)
    val x1073_rd_x824 = x824_pivot_col_0.value
    val x1074_rd_x825 = x825_pivot_row_0.value
    val x1362 = (x1074_rd_x825 * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))) + x1073_rd_x824
    val x1080_rd = x741_tab_sram_0.apply("Simplex.scala:129:36", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1362), Seq(x1071_rd_x829 & x1070_rd_x931 & x1069_rd_x1054))
    val x1081_elem_0 = x1080_rd.apply(0)
    val x1082_wr_x819 = if (x1071_rd_x829 & x1070_rd_x931 & x1069_rd_x1054) x819_pivot_element_0.set(x1081_elem_0)
  } 
}
/** END UnitPipe x1083_inr_UnitPipe **/
