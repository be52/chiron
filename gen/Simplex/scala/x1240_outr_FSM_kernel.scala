import emul._
import emul.implicits._

/** BEGIN StateMachine x1240_outr_FSM **/
object x1240_outr_FSM_kernel {
  def run(
  ): Unit = {
    var b73: FixedPoint = FixedPoint(BigDecimal("0"),FixFormat(true,32,0))
    def notDone() = {
      val x826 = b73 !== FixedPoint(BigDecimal("4"),FixFormat(true,32,0))
      x826
    }
    while( notDone() ){
      x827_reg.initMem(Bool(false,true))
      x828_reg.initMem(Bool(false,true))
      x829_reg.initMem(Bool(false,true))
      x830_reg.initMem(Bool(false,true))
      val x837_inr_UnitPipe = x837_inr_UnitPipe_kernel.run(b73)
      val x1321_rd_x827 = x827_reg.value
      val x1322_rd_x829 = x829_reg.value
      val x1226_outr_Switch = x1226_outr_Switch_kernel.run(x1322_rd_x829, b73, x1321_rd_x827)
      x1227_reg.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
      val x1238_inr_UnitPipe = x1238_inr_UnitPipe_kernel.run(b73)
      val x1239_rd_x1227 = x1227_reg.value
      b73 = x1239_rd_x1227
    }
  } 
}
/** END StateMachine x1240_outr_FSM **/
