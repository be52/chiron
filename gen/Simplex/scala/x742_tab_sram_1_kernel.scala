import emul._
import emul.implicits._

object x742_tab_sram_1 extends BankedMemory(
  name  = "tab_sram_1 (x742)",
  dims  = Seq(10,10),
  banks = Seq(1),
  data  = Array.fill(1){ Array.fill(100)(FloatPoint.invalid(FltFormat(23,8))) },
  invalid = FloatPoint.invalid(FltFormat(23,8)),
  saveInit = false
)
