import emul._
import emul.implicits._

/** BEGIN Switch x1226_outr_Switch **/
object x1226_outr_Switch_kernel {
  def run(
    x1322_rd_x829: Bool,
    b73: FixedPoint,
    x1321_rd_x827: Bool
  ): Unit = {
    if (x1321_rd_x827) {
      val x851_inr_UnitPipe = x851_inr_UnitPipe_kernel.run()
      val x1323_rd_x712 = x712_ncol.value
      val x853_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1323_rd_x712, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
      val x854_ctrchain = Array[Counterlike](x853_ctr)
      val x1324_rd_x828 = x828_reg.value
      val x882_inr_Foreach = x882_inr_Foreach_kernel.run(x1324_rd_x828, x854_ctrchain)
      x883_reg.initMem(Bool(false,true))
      x884_reg.initMem(Bool(false,true))
      val x900_inr_UnitPipe = x900_inr_UnitPipe_kernel.run()
      val x1325_rd_x883 = x883_reg.value
      val x1326_rd_x884 = x884_reg.value
      val x926_inr_Switch = x926_inr_Switch_kernel.run(x1325_rd_x883, x1326_rd_x884)
      ()
    }
    else if (x1322_rd_x829) {
      x929_reg.initMem(Bool(false,true))
      x930_reg.initMem(Bool(false,true))
      x931_reg.initMem(Bool(false,true))
      x932_reg.initMem(Bool(false,true))
      val x939_inr_UnitPipe = x939_inr_UnitPipe_kernel.run(b73)
      val x1327_rd_x929 = x929_reg.value
      val x1328_rd_x931 = x931_reg.value
      val x1223_outr_Switch = x1223_outr_Switch_kernel.run(x1327_rd_x929, x1328_rd_x931, b73)
      ()
    }
    else ()
  } 
}
/** END Switch x1226_outr_Switch **/
