import emul._
import emul.implicits._

/** BEGIN UnitPipe x1273_inr_UnitPipe **/
object x1273_inr_UnitPipe_kernel {
  def run(
    b1247: FixedPoint
  ): Unit = {
    val x1252_rd_x712 = x712_ncol.value
    val x1253_mul = b1247 * x1252_rd_x712
    val x1254 = x1253_mul >> FixedPoint(BigDecimal("4"),FixFormat(true,16,0))
    val x1255 = x1254 << FixedPoint(BigDecimal("4"),FixFormat(true,16,0))
    val x1368 = x1254 << FixedPoint(BigDecimal("6"),FixFormat(true,16,0))
    val x1257_sub = x1253_mul - x1255
    val x1259_sum = x1257_sub + x1252_rd_x712
    val x1260_sum = x1259_sum + FixedPoint(BigDecimal("15"),FixFormat(true,32,0))
    val x1261 = x1260_sum >> FixedPoint(BigDecimal("4"),FixFormat(true,16,0))
    val x1262 = x1261 << FixedPoint(BigDecimal("4"),FixFormat(true,16,0))
    val x1369 = x1261 << FixedPoint(BigDecimal("6"),FixFormat(true,16,0))
    val x1264 = x1368.toFixedPoint(FixFormat(true,64,0))
    val x1265 = FixedPoint.fromInt(0)
    val x1266_sum = x1264 + x1265
    val x1267_tuple: Struct1 = new Struct1(x1266_sum, x1369, Bool(false,true))
    val x1268 = true
    val x1269 = {
      if (x1268) x1242.enqueue(x1267_tuple)
    }
    val x1270_wr_x1249 = if (TRUE) x1249_reg.set(x1257_sub)
    val x1271_wr_x1250 = if (TRUE) x1250_reg.set(x1259_sum)
    val x1272_wr_x1251 = if (TRUE) x1251_reg.set(x1262)
  } 
}
/** END UnitPipe x1273_inr_UnitPipe **/
