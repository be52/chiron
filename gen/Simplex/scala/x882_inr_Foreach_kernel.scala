import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x882_inr_Foreach **/
object x882_inr_Foreach_kernel {
  def run(
    x1324_rd_x828: Bool,
    x854_ctrchain: Array[Counterlike]
  ): Unit = if (x1324_rd_x828){
    x854_ctrchain(0).foreach{case (is,vs) => 
      val b856 = is(0)
      val b857 = vs(0)
      val x858_rd_x711 = x711_nrow.value
      val x859_sub = x858_rd_x711 - FixedPoint(BigDecimal("1"),FixFormat(true,32,0))
      val x860_rd_x827 = x827_reg.value
      val x863_mul = x859_sub * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))
      val x864_sum = x863_mul + b856
      val x866_rd = x742_tab_sram_1.apply("Simplex.scala:88:25", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x864_sum), Seq(x860_rd_x827 & b857))
      val x867_elem_0 = x866_rd.apply(0)
      val x868_rd_x824 = x824_pivot_col_0.value
      val x871_sum = x863_mul + x868_rd_x824
      val x873_rd = x741_tab_sram_0.apply("Simplex.scala:88:49", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x871_sum), Seq(x860_rd_x827 & b857))
      val x874_elem_0 = x873_rd.apply(0)
      val x875 = x867_elem_0 < x874_elem_0
      val x877_rd_x827 = x827_reg.value
      val x878_wr_x824 = if (x877_rd_x827 & x875) x824_pivot_col_0.set(b856)
    }
  } else null.asInstanceOf[Unit]
}
/** END UnrolledForeach x882_inr_Foreach **/
