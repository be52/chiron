import emul._
import emul.implicits._

/** BEGIN UnitPipe x1301_outr_UnitPipe **/
object x1301_outr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x1355_rd_x711 = x711_nrow.value
    val x1245_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1355_rd_x711, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
    val x1246_ctrchain = Array[Counterlike](x1245_ctr)
    val x1300_outr_Foreach = x1300_outr_Foreach_kernel.run(x1246_ctrchain)
  } 
}
/** END UnitPipe x1301_outr_UnitPipe **/
