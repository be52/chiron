import emul._
import emul.implicits._

/** BEGIN Switch x1217_outr_Switch **/
object x1217_outr_Switch_kernel {
  def run(
    x1340_rd_x1110: Bool,
    x1341_rd_x1112: Bool
  ): Unit = {
    if (x1340_rd_x1110) {
      val x1342_rd_x711 = x711_nrow.value
      val x1130_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1342_rd_x711, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
      val x1131_ctrchain = Array[Counterlike](x1130_ctr)
      val x1343_rd_x829 = x829_reg.value
      val x1344_rd_x931 = x931_reg.value
      val x1345_rd_x1056 = x1056_reg.value
      val x1346_rd_x1110 = x1110_reg.value
      val x1372 = x1372_kernel.run(x1345_rd_x1056, x1343_rd_x829, x1131_ctrchain, x1344_rd_x931, x1346_rd_x1110)
      ()
    }
    else if (x1341_rd_x1112) {
      ()
    }
    else ()
  } 
}
/** END Switch x1217_outr_Switch **/
