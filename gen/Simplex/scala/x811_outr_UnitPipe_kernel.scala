import emul._
import emul.implicits._

/** BEGIN UnitPipe x811_outr_UnitPipe **/
object x811_outr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x1318_rd_x711 = x711_nrow.value
    val x747_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1318_rd_x711, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
    val x748_ctrchain = Array[Counterlike](x747_ctr)
    val x771_inr_Foreach = x771_inr_Foreach_kernel.run(x748_ctrchain)
    val x772 = x744.foreach{cmd => 
      for (i <- cmd.offset until cmd.offset+cmd.size by 4) {
        val data = {
          try {
            x733_tab_dram.apply(i / 4)
          }
          catch {case err: java.lang.ArrayIndexOutOfBoundsException => 
            System.out.println("[warn] Simplex.scala:49:16 Memory tab_dram: Out of bounds read at address " + err.getMessage)
            FloatPoint.invalid(FltFormat(23,8))
          }
        }
        x746.enqueue(data)
      }
    }
    x744.clear()
    val x1319_rd_x711 = x711_nrow.value
    val x773_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1319_rd_x711, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
    val x774_ctrchain = Array[Counterlike](x773_ctr)
    val x810_outr_Foreach = x810_outr_Foreach_kernel.run(x774_ctrchain)
  } 
}
/** END UnitPipe x811_outr_UnitPipe **/
