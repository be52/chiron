import emul._
import emul.implicits._

/** BEGIN UnitPipe x939_inr_UnitPipe **/
object x939_inr_UnitPipe_kernel {
  def run(
    b73: FixedPoint
  ): Unit = {
    val x933 = b73 === FixedPoint(BigDecimal("1"),FixFormat(true,32,0))
    val x934 = !x933
    val x935_wr_x929 = if (TRUE) x929_reg.set(x933)
    val x936_wr_x930 = if (TRUE) x930_reg.set(x933)
    val x937_wr_x932 = if (TRUE) x932_reg.set(x934)
    val x938_wr_x931 = if (TRUE) x931_reg.set(x934)
  } 
}
/** END UnitPipe x939_inr_UnitPipe **/
