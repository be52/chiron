import emul._
import emul.implicits._

/** BEGIN UnitPipe x1118_inr_UnitPipe **/
object x1118_inr_UnitPipe_kernel {
  def run(
    b73: FixedPoint
  ): Unit = {
    val x1113 = b73 === FixedPoint(BigDecimal("3"),FixFormat(true,32,0))
    val x1114 = !x1113
    val x1115_wr_x1111 = if (TRUE) x1111_reg.set(x1113)
    val x1116_wr_x1110 = if (TRUE) x1110_reg.set(x1113)
    val x1117_wr_x1112 = if (TRUE) x1112_reg.set(x1114)
  } 
}
/** END UnitPipe x1118_inr_UnitPipe **/
