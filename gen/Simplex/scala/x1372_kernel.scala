import emul._
import emul.implicits._

/** BEGIN ParallelPipe x1372 **/
object x1372_kernel {
  def run(
    x1345_rd_x1056: Bool,
    x1343_rd_x829: Bool,
    x1131_ctrchain: Array[Counterlike],
    x1344_rd_x931: Bool,
    x1346_rd_x1110: Bool
  ): Unit = {
    val x1128_inr_UnitPipe = x1128_inr_UnitPipe_kernel.run()
    val x1205_outr_Foreach = x1205_outr_Foreach_kernel.run(x1345_rd_x1056, x1343_rd_x829, x1131_ctrchain, x1344_rd_x931, x1346_rd_x1110)
    val x1213_inr_UnitPipe = x1213_inr_UnitPipe_kernel.run()
  } 
}
/** END ParallelPipe x1372 **/
