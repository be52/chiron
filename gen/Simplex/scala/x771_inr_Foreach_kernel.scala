import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x771_inr_Foreach **/
object x771_inr_Foreach_kernel {
  def run(
    x748_ctrchain: Array[Counterlike]
  ): Unit = {
    x748_ctrchain(0).foreach{case (is,vs) => 
      val b749 = is(0)
      val b750 = vs(0)
      val x751_rd_x712 = x712_ncol.value
      val x752_mul = b749 * x751_rd_x712
      val x753 = x752_mul >> FixedPoint(BigDecimal("4"),FixFormat(true,16,0))
      val x754 = x753 << FixedPoint(BigDecimal("4"),FixFormat(true,16,0))
      val x1357 = x753 << FixedPoint(BigDecimal("6"),FixFormat(true,16,0))
      val x756_sub = x752_mul - x754
      val x758_sum = x756_sub + x751_rd_x712
      val x759_sum = x758_sum + FixedPoint(BigDecimal("15"),FixFormat(true,32,0))
      val x760 = x759_sum >> FixedPoint(BigDecimal("4"),FixFormat(true,16,0))
      val x761 = x760 << FixedPoint(BigDecimal("4"),FixFormat(true,16,0))
      val x1358 = x760 << FixedPoint(BigDecimal("6"),FixFormat(true,16,0))
      val x763 = x1357.toFixedPoint(FixFormat(true,64,0))
      val x764 = FixedPoint.fromInt(0)
      val x765_sum = x763 + x764
      val x766_tuple: Struct1 = new Struct1(x765_sum, x1358, Bool(true,true))
      val x767 = true
      val x768 = {
        if (x767 & b750) x744.enqueue(x766_tuple)
      }
      val x769_tuple: Struct2 = new Struct2(x761, x756_sub, x758_sum)
      val x770_enq_x745 = {
        if (Bool(true,true) & b750) x745_fifo.enqueue(x769_tuple)
      }
    }
  } 
}
/** END UnrolledForeach x771_inr_Foreach **/
