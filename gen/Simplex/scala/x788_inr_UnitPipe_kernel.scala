import emul._
import emul.implicits._

/** BEGIN UnitPipe x788_inr_UnitPipe **/
object x788_inr_UnitPipe_kernel {
  def run(
    b776: Bool
  ): Unit = if (b776){
    val x780_deq_x745 = {
      val a0 = if (Bool(true,true) && x745_fifo.nonEmpty) x745_fifo.dequeue() else Struct2(FixedPoint.invalid(FixFormat(true,32,0)), FixedPoint.invalid(FixFormat(true,32,0)), FixedPoint.invalid(FixFormat(true,32,0)))
      Array[Struct2](a0)
    }
    val x781_elem_0 = x780_deq_x745.apply(0)
    val x782_apply = x781_elem_0.start
    val x783_wr_x777 = if (TRUE) x777_reg.set(x782_apply)
    val x784_apply = x781_elem_0.end
    val x785_wr_x778 = if (TRUE) x778_reg.set(x784_apply)
    val x786_apply = x781_elem_0.size
    val x787_wr_x779 = if (TRUE) x779_reg.set(x786_apply)
  } else null.asInstanceOf[Unit]
}
/** END UnitPipe x788_inr_UnitPipe **/
