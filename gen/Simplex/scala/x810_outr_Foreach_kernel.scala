import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x810_outr_Foreach **/
object x810_outr_Foreach_kernel {
  def run(
    x774_ctrchain: Array[Counterlike]
  ): Unit = {
    x774_ctrchain(0).foreach{case (is,vs) => 
      val b775 = is(0)
      val b776 = vs(0)
      x777_reg.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
      x778_reg.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
      x779_reg.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
      val x788_inr_UnitPipe = x788_inr_UnitPipe_kernel.run(b776)
      val x1320_rd_x779 = x779_reg.value
      val x790_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1320_rd_x779, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
      val x791_ctrchain = Array[Counterlike](x790_ctr)
      val x809_inr_Foreach = x809_inr_Foreach_kernel.run(b776, x791_ctrchain, b775)
    }
  } 
}
/** END UnrolledForeach x810_outr_Foreach **/
