import emul._
import emul.implicits._

/** BEGIN Switch x1220_outr_Switch **/
object x1220_outr_Switch_kernel {
  def run(
    x1334_rd_x1054: Bool,
    b73: FixedPoint,
    x1335_rd_x1056: Bool
  ): Unit = {
    if (x1334_rd_x1054) {
      val x1083_inr_UnitPipe = x1083_inr_UnitPipe_kernel.run()
      val x1336_rd_x712 = x712_ncol.value
      val x1085_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1336_rd_x712, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
      val x1086_ctrchain = Array[Counterlike](x1085_ctr)
      val x1337_rd_x830 = x830_reg.value
      val x1338_rd_x931 = x931_reg.value
      val x1339_rd_x1054 = x1054_reg.value
      val x1107_inr_Foreach = x1107_inr_Foreach_kernel.run(x1337_rd_x830, x1338_rd_x931, x1086_ctrchain, x1339_rd_x1054)
      ()
    }
    else if (x1335_rd_x1056) {
      x1110_reg.initMem(Bool(false,true))
      x1111_reg.initMem(Bool(false,true))
      x1112_reg.initMem(Bool(false,true))
      val x1118_inr_UnitPipe = x1118_inr_UnitPipe_kernel.run(b73)
      val x1340_rd_x1110 = x1110_reg.value
      val x1341_rd_x1112 = x1112_reg.value
      val x1217_outr_Switch = x1217_outr_Switch_kernel.run(x1340_rd_x1110, x1341_rd_x1112)
      ()
    }
    else ()
  } 
}
/** END Switch x1220_outr_Switch **/
