import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x1200_inr_Foreach **/
object x1200_inr_Foreach_kernel {
  def run(
    x1354_rd_x1138: Bool,
    b1136: FixedPoint,
    x1351_rd_x931: Bool,
    x1350_rd_x829: Bool,
    x1167_ctrchain: Array[Counterlike],
    x1353_rd_x1111: Bool,
    x1352_rd_x1057: Bool
  ): Unit = if (x1351_rd_x931 & x1353_rd_x1111 & x1354_rd_x1138 & x1350_rd_x829 & x1352_rd_x1057){
    x1167_ctrchain(0).foreach{case (is,vs) => 
      val b1173 = is(0)
      val b1174 = vs(0)
      val x1175_rd_x1139 = x1139_reg.value
      val x1176_rd_x1110 = x1110_reg.value
      val x1177_rd_x1056 = x1056_reg.value
      val x1178_rd_x932 = x932_reg.value
      val x1179_rd_x830 = x830_reg.value
      val x1365 = (b1136 * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))) + b1173
      val x1185_rd = x742_tab_sram_1.apply("Simplex.scala:142:42", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1365), Seq(x1179_rd_x830 & x1175_rd_x1139 & b1174 & x1177_rd_x1056 & x1176_rd_x1110 & x1178_rd_x932))
      val x1186_elem_0 = x1185_rd.apply(0)
      val x1187_rd_x819 = x819_pivot_element_0.value
      val x1188_rd_x825 = x825_pivot_row_0.value
      val x1366 = (x1188_rd_x825 * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))) + b1173
      val x1194_rd = x741_tab_sram_0.apply("Simplex.scala:142:73", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1366), Seq(x1179_rd_x830 & x1175_rd_x1139 & b1174 & x1177_rd_x1056 & x1176_rd_x1110 & x1178_rd_x932))
      val x1195_elem_0 = x1194_rd.apply(0)
      val x1196 = x1187_rd_x819 * x1195_elem_0
      val x1197 = x1186_elem_0 - x1196
      val x1198_wr = x741_tab_sram_0.update("Simplex.scala:142:32", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1365), Seq(x1179_rd_x830 & x1175_rd_x1139 & b1174 & x1177_rd_x1056 & x1176_rd_x1110 & x1178_rd_x932), Seq(x1197))
      val x1199_wr = x742_tab_sram_1.update("Simplex.scala:142:32", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1365), Seq(x1179_rd_x830 & x1175_rd_x1139 & b1174 & x1177_rd_x1056 & x1176_rd_x1110 & x1178_rd_x932), Seq(x1197))
    }
  } else null.asInstanceOf[Unit]
}
/** END UnrolledForeach x1200_inr_Foreach **/
