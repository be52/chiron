import emul._
import emul.implicits._

/** BEGIN UnitPipe x1238_inr_UnitPipe **/
object x1238_inr_UnitPipe_kernel {
  def run(
    b73: FixedPoint
  ): Unit = {
    val x1228_rd_x812 = x812_iter_0.value
    val x1229 = FixedPoint(BigDecimal("10"),FixFormat(true,32,0)) <= x1228_rd_x812
    val x1230_rd_x815 = x815_no_pivot_col_0.value
    val x1231 = x1229 || x1230_rd_x815
    val x1232_rd_x816 = x816_no_pivot_row_0.value
    val x1233 = x1231 || x1232_rd_x816
    val x1234_sum = b73 + FixedPoint(BigDecimal("1"),FixFormat(true,32,0))
    val x1367 = x1234_sum & FixedPoint(BigDecimal("3"),FixFormat(true,32,0))
    val x1236 = if (x1233) FixedPoint(BigDecimal("4"),FixFormat(true,32,0)) else x1367
    val x1237_wr_x1227 = if (TRUE) x1227_reg.set(x1236)
  } 
}
/** END UnitPipe x1238_inr_UnitPipe **/
