import emul._
import emul.implicits._

/** BEGIN UnitPipe x1064_inr_UnitPipe **/
object x1064_inr_UnitPipe_kernel {
  def run(
    b73: FixedPoint
  ): Unit = {
    val x1058 = b73 === FixedPoint(BigDecimal("2"),FixFormat(true,32,0))
    val x1059 = !x1058
    val x1060_wr_x1055 = if (TRUE) x1055_reg.set(x1058)
    val x1061_wr_x1054 = if (TRUE) x1054_reg.set(x1058)
    val x1062_wr_x1057 = if (TRUE) x1057_reg.set(x1059)
    val x1063_wr_x1056 = if (TRUE) x1056_reg.set(x1059)
  } 
}
/** END UnitPipe x1064_inr_UnitPipe **/
