import emul._
import emul.implicits._

/** BEGIN UnitPipe x1299_inr_UnitPipe **/
object x1299_inr_UnitPipe_kernel {
  def run(
    b1248: Bool
  ): Unit = if (b1248){
    val x1297 = {
      val a0 = if (TRUE && x1244.nonEmpty) x1244.dequeue() else Bool(false,false)
      Array[Bool](a0)
    }
  } else null.asInstanceOf[Unit]
}
/** END UnitPipe x1299_inr_UnitPipe **/
