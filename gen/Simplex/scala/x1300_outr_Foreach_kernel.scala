import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x1300_outr_Foreach **/
object x1300_outr_Foreach_kernel {
  def run(
    x1246_ctrchain: Array[Counterlike]
  ): Unit = {
    x1246_ctrchain(0).foreach{case (is,vs) => 
      val b1247 = is(0)
      val b1248 = vs(0)
      val x1295_outr_UnitPipe = x1295_outr_UnitPipe_kernel.run(b1247, b1248)
      val x1296 = x1242.foreach{cmd => 
        for (i <- cmd.offset until cmd.offset+cmd.size by 4) {
          val data = x1243.dequeue()
          try {
            if (data._2) x737_tab_dram_out(i / 4) = data._1
          }
          catch {case err: java.lang.ArrayIndexOutOfBoundsException => 
            System.out.println("[warn] Simplex.scala:156:38 Memory tab_dram_out: Out of bounds write at address " + err.getMessage)
          }
        }
        x1244.enqueue(true)
      }
      x1242.clear()
      val x1299_inr_UnitPipe = x1299_inr_UnitPipe_kernel.run(b1248)
    }
  } 
}
/** END UnrolledForeach x1300_outr_Foreach **/
