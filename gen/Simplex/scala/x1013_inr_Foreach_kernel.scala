import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x1013_inr_Foreach **/
object x1013_inr_Foreach_kernel {
  def run(
    x1330_rd_x829: Bool,
    x953_ctrchain: Array[Counterlike],
    x1331_rd_x930: Bool
  ): Unit = if (x1330_rd_x829 & x1331_rd_x930){
    x953_ctrchain(0).foreach{case (is,vs) => 
      val b956 = is(0)
      val b957 = vs(0)
      val x958_rd_x712 = x712_ncol.value
      val x959_sub = x958_rd_x712 - FixedPoint(BigDecimal("1"),FixFormat(true,32,0))
      val x960_rd_x929 = x929_reg.value
      val x961_rd_x830 = x830_reg.value
      val x964_mul = b956 * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))
      val x965_sum = x964_mul + x959_sub
      val x967_rd = x741_tab_sram_0.apply("Simplex.scala:107:38", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x965_sum), Seq(x961_rd_x830 & x960_rd_x929 & b957))
      val x968_elem_0 = x967_rd.apply(0)
      val x969_rd_x824 = x824_pivot_col_0.value
      val x972_sum = x964_mul + x969_rd_x824
      val x974_rd = x742_tab_sram_1.apply("Simplex.scala:107:60", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x972_sum), Seq(x961_rd_x830 & x960_rd_x929 & b957))
      val x975_elem_0 = x974_rd.apply(0)
      val x976 = x968_elem_0 / x975_elem_0
      val x977_wr_x819 = if (x961_rd_x830 & x960_rd_x929 & b957) x819_pivot_element_0.set(x976)
      val x978 = FloatPoint("-0.0", FltFormat(23,8)) < x975_elem_0
      val x979 = !x978
      val x980_rd_x819 = x819_pivot_element_0.value
      val x981_rd_x820 = x820_pivot_row_min_ratio_0.value
      val x982 = x980_rd_x819 < x981_rd_x820
      val x984_rd_x929 = x929_reg.value
      val x985_rd_x829 = x829_reg.value
      val x986_wr_x825 = if (x985_rd_x829 & x984_rd_x929 & x978 & x982) x825_pivot_row_0.set(b956)
      val x987_rd_x819 = x819_pivot_element_0.value
      val x988_wr_x820 = if (x985_rd_x829 & x984_rd_x929 & x978 & x982) x820_pivot_row_min_ratio_0.set(x987_rd_x819)
      val x993_rd_x817 = x817_no_pivot_row_count_0.value
      val x994_sum = x993_rd_x817 + FixedPoint(BigDecimal("1"),FixFormat(true,32,0))
      val x995_rd_x929 = x929_reg.value
      val x996_rd_x829 = x829_reg.value
      val x997_wr_x818 = if (x996_rd_x829 & x995_rd_x929 & x979) x818_no_pivot_row_count_1.set(x994_sum)
      val x998_wr_x817 = if (x996_rd_x829 & x995_rd_x929 & x979) x817_no_pivot_row_count_0.set(x994_sum)
      val x999 = b956.toString
      val x1000_rd_x818 = x818_no_pivot_row_count_1.value
      val x1001 = x1000_rd_x818.toString
      val x1002 = "Hello row " + x999 + "; no_pivot_row_count: " + x1001 + ""
      val x1003 = x1002 + "\n"
      val x1004 = if (x996_rd_x829 & x995_rd_x929 & x979) System.out.print(x1003)
      val x1008_rd_x819 = x819_pivot_element_0.value
      val x1009 = x1008_rd_x819.toString
      val x1010 = "row: " + x999 + "; ratio: " + x1009 + ""
      val x1011 = x1010 + "\n"
      val x1012 = if (x961_rd_x830 & x960_rd_x929 & b957) System.out.print(x1011)
    }
  } else null.asInstanceOf[Unit]
}
/** END UnrolledForeach x1013_inr_Foreach **/
