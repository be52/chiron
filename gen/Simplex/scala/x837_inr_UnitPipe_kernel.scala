import emul._
import emul.implicits._

/** BEGIN UnitPipe x837_inr_UnitPipe **/
object x837_inr_UnitPipe_kernel {
  def run(
    b73: FixedPoint
  ): Unit = {
    val x831 = b73 === FixedPoint(BigDecimal("0"),FixFormat(true,32,0))
    val x832 = !x831
    val x833_wr_x828 = if (TRUE) x828_reg.set(x831)
    val x834_wr_x827 = if (TRUE) x827_reg.set(x831)
    val x835_wr_x830 = if (TRUE) x830_reg.set(x832)
    val x836_wr_x829 = if (TRUE) x829_reg.set(x832)
  } 
}
/** END UnitPipe x837_inr_UnitPipe **/
