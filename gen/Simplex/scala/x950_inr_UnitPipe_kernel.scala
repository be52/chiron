import emul._
import emul.implicits._

/** BEGIN UnitPipe x950_inr_UnitPipe **/
object x950_inr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x942 = "Step 3. Choose pivot row"
    val x943 = x942 + "\n"
    val x944_rd_x929 = x929_reg.value
    val x945_rd_x829 = x829_reg.value
    val x946 = if (x945_rd_x829 & x944_rd_x929) System.out.print(x943)
    val x947_wr_x818 = if (x945_rd_x829 & x944_rd_x929) x818_no_pivot_row_count_1.set(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
    val x948_wr_x817 = if (x945_rd_x829 & x944_rd_x929) x817_no_pivot_row_count_0.set(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
    val x949_wr_x820 = if (x945_rd_x829 & x944_rd_x929) x820_pivot_row_min_ratio_0.set(FloatPoint(BigDecimal("99999.0000000000000000"), FltFormat(23,8)))
  } 
}
/** END UnitPipe x950_inr_UnitPipe **/
