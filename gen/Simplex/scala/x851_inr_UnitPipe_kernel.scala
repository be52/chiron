import emul._
import emul.implicits._

/** BEGIN UnitPipe x851_inr_UnitPipe **/
object x851_inr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x840_rd_x827 = x827_reg.value
    val x841 = if (x840_rd_x827) System.out.print("\n")
    val x842_rd_x812 = x812_iter_0.value
    val x843 = x842_rd_x812.toString
    val x844 = "------------------------- Iter " + x843 + " -------------------------"
    val x845 = x844 + "\n"
    val x846 = if (x840_rd_x827) System.out.print(x845)
    val x847 = "Step 2. Choose pivot column"
    val x848 = x847 + "\n"
    val x849 = if (x840_rd_x827) System.out.print(x848)
    val x850_wr_x824 = if (x840_rd_x827) x824_pivot_col_0.set(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
  } 
}
/** END UnitPipe x851_inr_UnitPipe **/
