import emul._
import emul.implicits._

/** BEGIN UnitPipe x1213_inr_UnitPipe **/
object x1213_inr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x1206_rd_x812 = x812_iter_0.value
    val x1207_sum = x1206_rd_x812 + FixedPoint(BigDecimal("1"),FixFormat(true,32,0))
    val x1208_rd_x1110 = x1110_reg.value
    val x1209_rd_x1056 = x1056_reg.value
    val x1210_rd_x931 = x931_reg.value
    val x1211_rd_x829 = x829_reg.value
    val x1212_wr_x812 = if (x1211_rd_x829 & x1210_rd_x931 & x1209_rd_x1056 & x1208_rd_x1110) x812_iter_0.set(x1207_sum)
  } 
}
/** END UnitPipe x1213_inr_UnitPipe **/
