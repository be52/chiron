import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x1205_outr_Foreach **/
object x1205_outr_Foreach_kernel {
  def run(
    x1345_rd_x1056: Bool,
    x1343_rd_x829: Bool,
    x1131_ctrchain: Array[Counterlike],
    x1344_rd_x931: Bool,
    x1346_rd_x1110: Bool
  ): Unit = if (x1343_rd_x829 & x1344_rd_x931 & x1345_rd_x1056 & x1346_rd_x1110){
    x1131_ctrchain(0).foreach{case (is,vs) => 
      val b1136 = is(0)
      val b1137 = vs(0)
      x1138_reg.initMem(Bool(false,true))
      x1139_reg.initMem(Bool(false,true))
      x1140_reg.initMem(Bool(false,true))
      val x1147_inr_UnitPipe = x1147_inr_UnitPipe_kernel.run(b1137, b1136)
      val x1347_rd_x1138 = x1138_reg.value
      val x1348_rd_x1140 = x1140_reg.value
      val x1204_outr_Switch = x1204_outr_Switch_kernel.run(x1347_rd_x1138, b1136, x1348_rd_x1140)
    }
  } else null.asInstanceOf[Unit]
}
/** END UnrolledForeach x1205_outr_Foreach **/
