import emul._
import emul.implicits._

/** BEGIN UnitPipe x1128_inr_UnitPipe **/
object x1128_inr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x1121 = "Step 5. Pivot everything else (elementar)"
    val x1122 = x1121 + "\n"
    val x1123_rd_x1110 = x1110_reg.value
    val x1124_rd_x1056 = x1056_reg.value
    val x1125_rd_x931 = x931_reg.value
    val x1126_rd_x829 = x829_reg.value
    val x1127 = if (x1126_rd_x829 & x1125_rd_x931 & x1124_rd_x1056 & x1123_rd_x1110) System.out.print(x1122)
  } 
}
/** END UnitPipe x1128_inr_UnitPipe **/
