import emul._
import emul.implicits._

/** BEGIN UnitPipe x1295_outr_UnitPipe **/
object x1295_outr_UnitPipe_kernel {
  def run(
    b1247: FixedPoint,
    b1248: Bool
  ): Unit = if (b1248){
    x1249_reg.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
    x1250_reg.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
    x1251_reg.initMem(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))
    val x1273_inr_UnitPipe = x1273_inr_UnitPipe_kernel.run(b1247)
    val x1356_rd_x1251 = x1251_reg.value
    val x1275_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1356_rd_x1251, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
    val x1276_ctrchain = Array[Counterlike](x1275_ctr)
    val x1294_inr_Foreach = x1294_inr_Foreach_kernel.run(b1247, x1276_ctrchain)
  } else null.asInstanceOf[Unit]
}
/** END UnitPipe x1295_outr_UnitPipe **/
