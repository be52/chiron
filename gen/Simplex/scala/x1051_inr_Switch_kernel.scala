import emul._
import emul.implicits._

/** BEGIN Switch x1051_inr_Switch **/
object x1051_inr_Switch_kernel {
  def run(
    x1333_rd_x1015: Bool,
    x1332_rd_x1014: Bool
  ): Unit = {
    if (x1332_rd_x1014) {
      val x1034_rd_x1014 = x1014_reg.value
      val x1035_rd_x929 = x929_reg.value
      val x1036_rd_x829 = x829_reg.value
      val x1037 = if (x1036_rd_x829 & x1035_rd_x929 & x1034_rd_x1014) System.out.print("No pivot row found!\n")
      val x1038_wr_x816 = if (x1036_rd_x829 & x1035_rd_x929 & x1034_rd_x1014) x816_no_pivot_row_0.set(Bool(true,true))
      ()
    }
    else if (x1333_rd_x1015) {
      val x1040_rd_x825 = x825_pivot_row_0.value
      val x1041 = x1040_rd_x825.toString
      val x1042_rd_x820 = x820_pivot_row_min_ratio_0.value
      val x1043 = x1042_rd_x820.toString
      val x1044 = "Pivot row: " + x1041 + "; pivot_row_min_ratio: " + x1043 + ""
      val x1045 = x1044 + "\n"
      val x1046_rd_x1015 = x1015_reg.value
      val x1047_rd_x929 = x929_reg.value
      val x1048_rd_x829 = x829_reg.value
      val x1049 = if (x1048_rd_x829 & x1047_rd_x929 & x1046_rd_x1015) System.out.print(x1045)
      ()
    }
    else ()
  } 
}
/** END Switch x1051_inr_Switch **/
