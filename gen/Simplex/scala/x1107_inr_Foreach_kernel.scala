import emul._
import emul.implicits._

/** BEGIN UnrolledForeach x1107_inr_Foreach **/
object x1107_inr_Foreach_kernel {
  def run(
    x1337_rd_x830: Bool,
    x1338_rd_x931: Bool,
    x1086_ctrchain: Array[Counterlike],
    x1339_rd_x1054: Bool
  ): Unit = if (x1337_rd_x830 & x1338_rd_x931 & x1339_rd_x1054){
    x1086_ctrchain(0).foreach{case (is,vs) => 
      val b1090 = is(0)
      val b1091 = vs(0)
      val x1092_rd_x825 = x825_pivot_row_0.value
      val x1093_rd_x1055 = x1055_reg.value
      val x1094_rd_x932 = x932_reg.value
      val x1095_rd_x829 = x829_reg.value
      val x1363 = (x1092_rd_x825 * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))) + b1090
      val x1101_rd = x741_tab_sram_0.apply("Simplex.scala:131:46", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1363), Seq(x1095_rd_x829 & x1094_rd_x932 & x1093_rd_x1055 & b1091))
      val x1102_elem_0 = x1101_rd.apply(0)
      val x1103_rd_x819 = x819_pivot_element_0.value
      val x1104 = x1102_elem_0 / x1103_rd_x819
      val x1105_wr = x741_tab_sram_0.update("Simplex.scala:131:36", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1363), Seq(x1095_rd_x829 & x1094_rd_x932 & x1093_rd_x1055 & b1091), Seq(x1104))
      val x1106_wr = x742_tab_sram_1.update("Simplex.scala:131:36", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1363), Seq(x1095_rd_x829 & x1094_rd_x932 & x1093_rd_x1055 & b1091), Seq(x1104))
    }
  } else null.asInstanceOf[Unit]
}
/** END UnrolledForeach x1107_inr_Foreach **/
