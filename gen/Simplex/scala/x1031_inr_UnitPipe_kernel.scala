import emul._
import emul.implicits._

/** BEGIN UnitPipe x1031_inr_UnitPipe **/
object x1031_inr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x1016_rd_x817 = x817_no_pivot_row_count_0.value
    val x1017 = x1016_rd_x817.toString
    val x1018_rd_x713 = x713_ncons.value
    val x1019 = x1018_rd_x713.toString
    val x1020 = "no_pivot_row_count: " + x1017 + "; ncons: " + x1019 + ""
    val x1021 = x1020 + "\n"
    val x1022_rd_x929 = x929_reg.value
    val x1023_rd_x829 = x829_reg.value
    val x1024 = if (x1023_rd_x829 & x1022_rd_x929) System.out.print(x1021)
    val x1025_rd_x818 = x818_no_pivot_row_count_1.value
    val x1027 = x1025_rd_x818 === x1018_rd_x713
    val x1028 = !x1027
    val x1029_wr_x1014 = if (TRUE) x1014_reg.set(x1027)
    val x1030_wr_x1015 = if (TRUE) x1015_reg.set(x1028)
  } 
}
/** END UnitPipe x1031_inr_UnitPipe **/
