import emul._
import emul.implicits._

/** BEGIN Switch x926_inr_Switch **/
object x926_inr_Switch_kernel {
  def run(
    x1325_rd_x883: Bool,
    x1326_rd_x884: Bool
  ): Unit = {
    if (x1325_rd_x883) {
      val x903_rd_x883 = x883_reg.value
      val x904_rd_x827 = x827_reg.value
      val x905 = if (x904_rd_x827 & x903_rd_x883) System.out.print("No pivot column found!\n")
      val x906_wr_x815 = if (x904_rd_x827 & x903_rd_x883) x815_no_pivot_col_0.set(Bool(true,true))
      ()
    }
    else if (x1326_rd_x884) {
      val x908_rd_x711 = x711_nrow.value
      val x909_sub = x908_rd_x711 - FixedPoint(BigDecimal("1"),FixFormat(true,32,0))
      val x910_rd_x824 = x824_pivot_col_0.value
      val x911_rd_x884 = x884_reg.value
      val x912_rd_x827 = x827_reg.value
      val x1361 = (x909_sub * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))) + x910_rd_x824
      val x918_rd = x741_tab_sram_0.apply("Simplex.scala:98:79", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1361), Seq(x912_rd_x827 & x911_rd_x884))
      val x919_elem_0 = x918_rd.apply(0)
      val x920 = x910_rd_x824.toString
      val x921 = x919_elem_0.toString
      val x922 = "Pivot column: " + x920 + "; constraint value: " + x921 + ""
      val x923 = x922 + "\n"
      val x924 = if (x912_rd_x827 & x911_rd_x884) System.out.print(x923)
      ()
    }
    else ()
  } 
}
/** END Switch x926_inr_Switch **/
