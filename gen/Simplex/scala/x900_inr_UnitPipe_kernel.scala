import emul._
import emul.implicits._

/** BEGIN UnitPipe x900_inr_UnitPipe **/
object x900_inr_UnitPipe_kernel {
  def run(
  ): Unit = {
    val x885_rd_x711 = x711_nrow.value
    val x886_sub = x885_rd_x711 - FixedPoint(BigDecimal("1"),FixFormat(true,32,0))
    val x887_rd_x824 = x824_pivot_col_0.value
    val x888_rd_x827 = x827_reg.value
    val x1360 = (x886_sub * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))) + x887_rd_x824
    val x894_rd = x741_tab_sram_0.apply("Simplex.scala:94:23", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1360), Seq(x888_rd_x827))
    val x895_elem_0 = x894_rd.apply(0)
    val x896 = FloatPoint("-0.0", FltFormat(23,8)) <= x895_elem_0
    val x897 = !x896
    val x898_wr_x883 = if (TRUE) x883_reg.set(x896)
    val x899_wr_x884 = if (TRUE) x884_reg.set(x897)
  } 
}
/** END UnitPipe x900_inr_UnitPipe **/
