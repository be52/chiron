import emul._
import emul.implicits._

/** BEGIN UnitPipe x1164_inr_UnitPipe **/
object x1164_inr_UnitPipe_kernel {
  def run(
    b1136: FixedPoint
  ): Unit = {
    val x1150_rd_x824 = x824_pivot_col_0.value
    val x1151_rd_x1138 = x1138_reg.value
    val x1152_rd_x1110 = x1110_reg.value
    val x1153_rd_x1056 = x1056_reg.value
    val x1154_rd_x931 = x931_reg.value
    val x1155_rd_x829 = x829_reg.value
    val x1364 = (b1136 * FixedPoint(BigDecimal("10"),FixFormat(true,32,0))) + x1150_rd_x824
    val x1161_rd = x741_tab_sram_0.apply("Simplex.scala:140:40", Seq(Seq(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)))), Seq(x1364), Seq(x1152_rd_x1110 & x1153_rd_x1056 & x1151_rd_x1138 & x1155_rd_x829 & x1154_rd_x931))
    val x1162_elem_0 = x1161_rd.apply(0)
    val x1163_wr_x819 = if (x1152_rd_x1110 & x1153_rd_x1056 & x1151_rd_x1138 & x1155_rd_x829 & x1154_rd_x931) x819_pivot_element_0.set(x1162_elem_0)
  } 
}
/** END UnitPipe x1164_inr_UnitPipe **/
