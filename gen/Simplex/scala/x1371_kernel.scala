import emul._
import emul.implicits._

/** BEGIN ParallelPipe x1371 **/
object x1371_kernel {
  def run(
  ): Unit = {
    val x740_inr_UnitPipe = x740_inr_UnitPipe_kernel.run()
    val x811_outr_UnitPipe = x811_outr_UnitPipe_kernel.run()
    val x814_inr_UnitPipe = x814_inr_UnitPipe_kernel.run()
    val x823_inr_UnitPipe = x823_inr_UnitPipe_kernel.run()
  } 
}
/** END ParallelPipe x1371 **/
