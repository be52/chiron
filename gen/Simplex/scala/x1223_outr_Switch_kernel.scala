import emul._
import emul.implicits._

/** BEGIN Switch x1223_outr_Switch **/
object x1223_outr_Switch_kernel {
  def run(
    x1327_rd_x929: Bool,
    x1328_rd_x931: Bool,
    b73: FixedPoint
  ): Unit = {
    if (x1327_rd_x929) {
      val x950_inr_UnitPipe = x950_inr_UnitPipe_kernel.run()
      val x1329_rd_x713 = x713_ncons.value
      val x952_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1329_rd_x713, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
      val x953_ctrchain = Array[Counterlike](x952_ctr)
      val x1330_rd_x829 = x829_reg.value
      val x1331_rd_x930 = x930_reg.value
      val x1013_inr_Foreach = x1013_inr_Foreach_kernel.run(x1330_rd_x829, x953_ctrchain, x1331_rd_x930)
      x1014_reg.initMem(Bool(false,true))
      x1015_reg.initMem(Bool(false,true))
      val x1031_inr_UnitPipe = x1031_inr_UnitPipe_kernel.run()
      val x1332_rd_x1014 = x1014_reg.value
      val x1333_rd_x1015 = x1015_reg.value
      val x1051_inr_Switch = x1051_inr_Switch_kernel.run(x1333_rd_x1015, x1332_rd_x1014)
      ()
    }
    else if (x1328_rd_x931) {
      x1054_reg.initMem(Bool(false,true))
      x1055_reg.initMem(Bool(false,true))
      x1056_reg.initMem(Bool(false,true))
      x1057_reg.initMem(Bool(false,true))
      val x1064_inr_UnitPipe = x1064_inr_UnitPipe_kernel.run(b73)
      val x1334_rd_x1054 = x1054_reg.value
      val x1335_rd_x1056 = x1056_reg.value
      val x1220_outr_Switch = x1220_outr_Switch_kernel.run(x1334_rd_x1054, b73, x1335_rd_x1056)
      ()
    }
    else ()
  } 
}
/** END Switch x1223_outr_Switch **/
