import emul._
import emul.implicits._

/** BEGIN Switch x1204_outr_Switch **/
object x1204_outr_Switch_kernel {
  def run(
    x1347_rd_x1138: Bool,
    b1136: FixedPoint,
    x1348_rd_x1140: Bool
  ): Unit = {
    if (x1347_rd_x1138) {
      val x1164_inr_UnitPipe = x1164_inr_UnitPipe_kernel.run(b1136)
      val x1349_rd_x712 = x712_ncol.value
      val x1166_ctr = Counter(FixedPoint(BigDecimal("0"),FixFormat(true,32,0)), x1349_rd_x712, FixedPoint(BigDecimal("1"),FixFormat(true,32,0)), FixedPoint(BigDecimal("1"),FixFormat(true,32,0)))
      val x1167_ctrchain = Array[Counterlike](x1166_ctr)
      val x1350_rd_x829 = x829_reg.value
      val x1351_rd_x931 = x931_reg.value
      val x1352_rd_x1057 = x1057_reg.value
      val x1353_rd_x1111 = x1111_reg.value
      val x1354_rd_x1138 = x1138_reg.value
      val x1200_inr_Foreach = x1200_inr_Foreach_kernel.run(x1354_rd_x1138, b1136, x1351_rd_x931, x1350_rd_x829, x1167_ctrchain, x1353_rd_x1111, x1352_rd_x1057)
      ()
    }
    else if (x1348_rd_x1140) {
      ()
    }
    else ()
  } 
}
/** END Switch x1204_outr_Switch **/
