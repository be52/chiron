import spatial.dsl._

@spatial object Simplex extends SpatialApp {
  def main(args: Array[String]): Void = {
    // println("------------------------- Host -------------------------")
    // Parameters
    val max_iters = 10

    // Step 1. Load Simplex tableau
    val file = args(0)
    val tab_data = loadCSV2D[Float](file)
    // printMatrix(tab_data, "Starting Tableau: ")

    // Give the accelerator these things, maybe unecesarry
    val nrow = ArgIn[Int]
    val ncol = ArgIn[Int]
    val ncons = ArgIn[Int]
    val ncoefs = ArgIn[Int]

    setArg(nrow, tab_data.rows.to[Int])
    setArg(ncol, tab_data.cols.to[Int])
    setArg(ncons, tab_data.rows.to[Int] - 1)
    setArg(ncoefs, tab_data.cols.to[Int] - tab_data.rows.to[Int])

    // println(r"Number of coefficients: ${ncoefs}")
    // println(r"Number of constraints: ${ncons}")

    // DRAM
    // Create DRAM for Simplex Tableau
    val tab_dram = DRAM[Float](nrow, ncol)
    
    // Load data into DRAM
    setMem(tab_dram, tab_data)

    // Create output
    val tab_dram_out = DRAM[Float](nrow, ncol)

    // println("")
    
    Accel {
      // println("------------------------- Accel -------------------------")
      // Create SRAM
      // Just hard coding to "large enough" for now
      // val tab_sram = SRAM[Float](100, 100)
      val tab_sram = SRAM[Float](10, 10)
      
      // Transfer from DRAM into SRAM
      // Using nrow and ncol here causes no such issues, so OK?
      tab_sram load tab_dram(0::nrow, 0::ncol)

      // FSM states
      val state_step_2 = 0
      val state_step_3 = 1
      val state_step_4 = 2
      val state_step_5 = 3
      val state_done = 4

      // Iteration counter
      val iter = Reg[Int]
      iter := 1

      // Triggers for end
      val no_pivot_col = Reg[Bit]
      val no_pivot_row = Reg[Bit]
      val no_pivot_row_count = Reg[Int]
      val pivot_element = Reg[Float]
      val pivot_row_min_ratio = Reg[Float]
      no_pivot_col := false
      no_pivot_row := false

      // Keep track of pivot cols and rows
      val pivot_col = Reg[Int]
      val pivot_row = Reg[Int]

      FSM(0) {state => 
        // While condition
        state != state_done
      } { state =>
        // Main loop
        if (state == state_step_2) {
          // println("")
          // println(r"------------------------- Iter ${iter} -------------------------")
          // println(r"Step 2. Choose pivot column")
          
          // Set pivot col to 0 by default then search for better pivots
          pivot_col := 0
          Foreach(ncol by 1){(i) => 
            if (tab_sram(nrow - 1, i) < tab_sram(nrow - 1, pivot_col)) {
              pivot_col := i
            }
          }

          // If min is positive then there is no pivot column
          if (tab_sram(nrow - 1, pivot_col) >= 0) {
            // println("No pivot column found!")
            no_pivot_col := true
          } else {
            // println(r"Pivot column: ${pivot_col}; constraint value: ${tab_sram(nrow - 1, pivot_col)}")
          }

        } else if (state == state_step_3) {
          // println(r"Step 3. Choose pivot row")
          // TODO: Something better than this
          no_pivot_row_count := 0
          pivot_row_min_ratio := 99999
          Foreach(ncons by 1){(i) =>
            pivot_element := tab_sram(i, ncol - 1)/tab_sram(i, pivot_col)
            if (tab_sram(i, pivot_col) > 0.0) {
              if (pivot_element < pivot_row_min_ratio) {
                pivot_row := i
                pivot_row_min_ratio := pivot_element
              }
            } else {
              no_pivot_row_count := no_pivot_row_count + 1 
              // println(r"Hello row ${i}; no_pivot_row_count: ${no_pivot_row_count}")
            }
            // println(r"row: ${i}; ratio: ${pivot_element}")
          }
          // println(r"no_pivot_row_count: ${no_pivot_row_count}; ncons: ${ncons}")
          if (no_pivot_row_count === ncons.value) {
            // println("No pivot row found!")
            no_pivot_row := true
          } else {
            // println(r"Pivot row: ${pivot_row}; pivot_row_min_ratio: ${pivot_row_min_ratio}")
          }

        } else if (state == state_step_4) {
          // println(r"Step 4. Pivot the pivot row")
          pivot_element := tab_sram(pivot_row, pivot_col)
          Foreach(ncol by 1){(i) =>
            tab_sram(pivot_row, i) = tab_sram(pivot_row, i)/pivot_element
          }
          // // printSRAM2(tab_sram(0::nrow, 0::ncol), "Tableau after Step 4: ") 

        } else if (state == state_step_5) {
          // println(r"Step 5. Pivot everything else (elementar)")
          Foreach(nrow by 1){(i) =>
            // Don't pivot the pivot row though
            if (i != pivot_row.value) {
              pivot_element := tab_sram(i, pivot_col)
              Foreach(ncol by 1){(j) =>
                tab_sram(i, j) = tab_sram(i, j) - pivot_element*tab_sram(pivot_row, j)
              }
            }
          }
          // // printSRAM2(tab_sram(0::nrow, 0::ncol), "Tableau after Step 5: ")

          iter := iter + 1
        }
      } {state =>
        // Calculate next state
        mux(iter >= max_iters || no_pivot_col == true || no_pivot_row == true, state_done, (state + 1) % 4)
      }

      // Store output
      tab_dram_out(0::nrow, 0::ncol) store tab_sram(0::nrow, 0::ncol) 
    }
    // println("")
    // println("------------------------- Host -------------------------")
    // printMatrix(getMatrix(tab_dram_out), "Ending Tableau: ")
  }
}